﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Collaborative.C2.Toolkit.JobScheduler.Configuration
{
    public class RecurringJobGlobalConfiguration
    {
        private static readonly RecurringJobGlobalConfiguration configuration = new RecurringJobGlobalConfiguration();

        public static RecurringJobGlobalConfiguration Configuration
        {
            get { return configuration; }
        }

        public static IGlobalConfiguration<T> Use<T>(
            T entry,
            Action<T> entryAction)
        {
            entryAction(entry);

            return new ConfigurationEntry<T>(entry);
        }

        public static IGlobalConfiguration<RecurringJobOptions> UseServerConfiguration(RecurringJobOptions options = null)
        {
            return Use<RecurringJobOptions>(options, 
                (_options => 
                    RecurringJobConfiguration.SetJobSchedulerServerConfiguration(_options)));
        }
    }

    public interface IGlobalConfiguration<out T>
    {
        T Entry { get; }
    }

    class ConfigurationEntry<T> : IGlobalConfiguration<T>
    {
        private readonly T _entry;

        public ConfigurationEntry(T entry)
        {
            _entry = entry;
        }

        public T Entry
        {
            get { return _entry; }
        }
    }
}