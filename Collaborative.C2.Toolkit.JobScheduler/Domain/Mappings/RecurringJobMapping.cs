﻿using Collaborative.C2.Toolkit.JobScheduler.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.C2.Toolkit.JobScheduler.Domain.Mappings
{
    public class RecurringJobMapping : EntityTypeConfiguration<RecurringJob>
    {
        public RecurringJobMapping()
        {
            this.ToTable("C2_JOB_SCHEDULE");

            this.HasKey(job => job.Id)
                .Property(job => job.Id)
                .HasColumnName("JOB_ID")
                .IsRequired();

            this.Property(job => job.CronSchedule)
                .HasColumnName("CRON_SCHEDULE_FORMAT")
                .HasMaxLength(20)
                .IsRequired();

            this.Property(job => job.Key)
                .HasColumnName("SCHEDULE_KEY")
                .HasMaxLength(100)
                .IsRequired();

            this.Property(job => job.NextRunTime)
                .HasColumnName("NEXT_RUN_TIME")
                .IsRequired();

            this.Property(job => job.LastSuccessfulRun)
                .HasColumnName("LAST_SUCCESSFUL_RUN")
                .IsOptional();

            this.Property(job => job.LastFailureTime)
                .HasColumnName("LAST_FAILURE_TIME")
                .IsOptional();

            this.Property(job => job.SerializedJob)
                .HasColumnName("JSON_SERIALIZED")
                .HasMaxLength(800);

            this.Property(job => job.Status)
                .HasColumnName("STATUS")
                .HasMaxLength(25);

            this.Property(job => job.ModifiedBy)
                .HasColumnName("MODIFIED_BY")
                .HasMaxLength(38)
                .IsRequired();

            this.Property(job => job.ModifiedDate)
                .HasColumnName("MODIFIED_DATE")
                .IsRequired();

            this.Property(job => job.CreatedBy)
                .HasColumnName("CREATED_BY")
                .HasMaxLength(38)
                .IsRequired();

            this.Property(job => job.CreatedDate)
                .HasColumnName("CREATED_DATE")
                .IsRequired();
        }
    }
}