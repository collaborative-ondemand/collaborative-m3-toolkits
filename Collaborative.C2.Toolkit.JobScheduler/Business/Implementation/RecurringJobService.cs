﻿using Collaborative.C2.Toolkit.JobScheduler.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Collaborative.C2.Toolkit.JobScheduler.Core;
using Collaborative.C2.Toolkit.JobScheduler.Configuration;
using Collaborative.M3.ServerSdk.Core.Logging;
using System.Threading;

namespace Collaborative.C2.Toolkit.JobScheduler.Business.Implementation
{
    public class RecurringJobService
    {
        private static readonly Lazy<RecurringJobManagerService> Instance = new Lazy<RecurringJobManagerService>(
            () => new RecurringJobManagerService());

        private static DateTime StartDateTime = System.DateTime.Now;
        private static int NextPollingInSeconds = 0;

        private static M3Logger logger = M3Logger.getLogger("M3RecurringJobService");

        public static void AddOrUpdate(
            Expression<Action> methodCall,
            Func<string> cronExpression,
            TimeZoneInfo timeZone = null)
        {
            AddOrUpdate(methodCall, cronExpression(), timeZone);
        }

        public static void AddOrUpdate<T>(
            Expression<Action<T>> methodCall,
            Func<string> cronExpression,
            TimeZoneInfo timeZone = null)
        {
            AddOrUpdate(methodCall, cronExpression(), timeZone);
        }

        public static void AddOrUpdate(
            Expression<Action> methodCall,
            string cronExpression,
            TimeZoneInfo timeZone = null)
        {
            var job = Job.FromExpression(methodCall);
            var id = GetRecurringJobId(job);

            Instance.Value.AddOrUpdate(id, job, cronExpression, timeZone ?? TimeZoneInfo.Utc);
        }

        public static void AddOrUpdate<T>(
            Expression<Action<T>> methodCall,
            string cronExpression,
            TimeZoneInfo timeZone = null)
        {
            var job = Job.FromExpression(methodCall);
            var id = GetRecurringJobId(job);

            Instance.Value.AddOrUpdate(id, job, cronExpression, timeZone ?? TimeZoneInfo.Utc);
        }

        public static void AddOrUpdate(
            string recurringJobId,
            Expression<Action> methodCall,
            Func<string> cronExpression,
            TimeZoneInfo timeZone = null)
        {
            AddOrUpdate(recurringJobId, methodCall, cronExpression(), timeZone);
        }

        public static void AddOrUpdate<T>(
            string recurringJobId,
            Expression<Action<T>> methodCall,
            Func<string> cronExpression,
            TimeZoneInfo timeZone = null)
        {
            AddOrUpdate(recurringJobId, methodCall, cronExpression(), timeZone);
        }

        public static void AddOrUpdate(
            string recurringJobId,
            Expression<Action> methodCall,
            string cronExpression,
            TimeZoneInfo timeZone = null)
        {
            var job = Job.FromExpression(methodCall);
            Instance.Value.AddOrUpdate(recurringJobId, job, cronExpression, timeZone ?? TimeZoneInfo.Utc);
        }

        public static void AddOrUpdate<T>(
            string recurringJobId,
            Expression<Action<T>> methodCall,
            string cronExpression,
            TimeZoneInfo timeZone = null)
        {
            var job = Job.FromExpression(methodCall);
            Instance.Value.AddOrUpdate(recurringJobId, job, cronExpression, timeZone ?? TimeZoneInfo.Utc);
        }

        public static void AddOrUpdate(
            Type type,
            string cronExpression,
            TimeZoneInfo timeZone = null)
        {
            string typeName = type.Name;
            string uri = $"/api/{typeName}/execute";

            //Use HttpClient to make a request
        }

        public static async void AddOrUpdateNoStoragePersist(
            Func<DateTimeOffset, CancellationToken, Task> methodCall,
            TimeSpan time,
            TimeZoneInfo timeZone = null)
        {
            await Instance.Value.AddJobToNeverEndingTask(methodCall, time);
        }

        public static void Trigger(string recurringJobId)
        {
            Instance.Value.Trigger(recurringJobId);
        }

        public static void Execute()
        {
            Instance.Value.ExecuteRecurringJobs();
        }

        public static async Task WatchDog()
        {
            Console.WriteLine("Entered watchdog: " + System.DateTime.Now.ToLongTimeString());
            //Adjust seconds
            AdjustStartDateTimeSeconds();

            //Adjust next polling only the first time, the following times the if condition will adjust it.
            if (NextPollingInSeconds == 0)
            {
                AdjustNextPollingSeconds();
            }

            //This method will be watching every 30 seconds if we have to fetch the DataSource or not, this is to avoid a long Delay
            TimeSpan elapsedTime = DateTime.Now - StartDateTime;

            //Verify if the elapsed time is equal to the one configured
            if (Math.Truncate(elapsedTime.TotalSeconds) >= NextPollingInSeconds)
            {
                logger.info("Watchdog", M3LoggerTags.M3BUS.ToString(), "about to enter ExecuteRecurringJobs");

                //Reset the clock, adjust StartDateTime seconds and adjust NextPollingInSeconds
                StartDateTime = DateTime.Now;
                AdjustStartDateTimeSeconds();
                AdjustNextPollingSeconds();
                await Instance.Value.ExecuteRecurringJobs();
            }
        }

        private static string GetRecurringJobId(Job job)
        {
            return String.Format("{0}.{1}", job.Type.ToGenericTypeString(), job.Method.Name);
        }

        /// <summary>
        /// Method that verifies if StartDateTime's seconds is more than zero seconds, if yes then adjust the seconds to zero.
        /// </summary>
        private static void AdjustStartDateTimeSeconds()
        {
            if(StartDateTime.Second > 0)
            {
                StartDateTime = DateTime.Parse(StartDateTime.ToString("f"));
            }
        }

        private static void AdjustNextPollingSeconds()
        {
            if (
                    (RecurringJobConfiguration.GetJobSchedulerServerConfiguration().DataSourcePolling == DataSourcePollingType.Hourly ||
                     RecurringJobConfiguration.GetJobSchedulerServerConfiguration().DataSourcePolling == DataSourcePollingType.TwoHours ||
                     RecurringJobConfiguration.GetJobSchedulerServerConfiguration().DataSourcePolling == DataSourcePollingType.ThreeHours) &&
                     RecurringJobConfiguration.GetJobSchedulerServerConfiguration().AdjustDataSourcePolling)
            {
                //If the setting is to adjust the next polling time then adjust it
                NextPollingInSeconds = SecondsLeftForNextHourlyPolling(DateTime.Now, RecurringJobConfiguration.GetJobSchedulerServerConfiguration().DataSourcePolling);
            }
            else
            {
                NextPollingInSeconds = NextPolling(RecurringJobConfiguration.GetJobSchedulerServerConfiguration().DataSourcePolling);
            }
        }

        private static int SecondsLeftForNextHourlyPolling(DateTime dateTime, DataSourcePollingType pollingType)
        {
            switch (pollingType)
            {
                case DataSourcePollingType.None:
                    return 0;
                case DataSourcePollingType.Minutely:
                    return 60 * 1;
                case DataSourcePollingType.FiveMinutes:
                    return 60 * 5;
                case DataSourcePollingType.TenMinutes:
                    return 60 * 10;
                case DataSourcePollingType.ThirtyMinutes:
                    return 60 * 30;
                case DataSourcePollingType.Hourly:
                    return 60 * (60 - dateTime.Minute);
                case DataSourcePollingType.TwoHours:
                    return 60 * (120 - dateTime.Minute);
                case DataSourcePollingType.ThreeHours:
                    return 60 * (180 - dateTime.Minute);
                default:
                    return 0;
            }
        }

        private static int NextPolling(DataSourcePollingType pollingType)
        {
            switch (pollingType)
            {
                case DataSourcePollingType.None:
                    return 0;
                case DataSourcePollingType.Minutely:
                    return 60 * 1;
                case DataSourcePollingType.FiveMinutes:
                    return 60 * 5;
                case DataSourcePollingType.TenMinutes:
                    return 60 * 10;
                case DataSourcePollingType.ThirtyMinutes:
                    return 60 * 30;
                case DataSourcePollingType.Hourly:
                    return 60 * 60;
                case DataSourcePollingType.TwoHours:
                    return 60 * 120;
                case DataSourcePollingType.ThreeHours:
                    return 60 * 180;
                default:
                    return 0;
            }
        }
    }
}
