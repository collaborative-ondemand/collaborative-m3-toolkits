﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Collaborative.M3.ServerSdk.Core.Common;
using NCrontab;
using System.Threading;
using System.Threading.Tasks.Dataflow;
using Collaborative.C2.Toolkit.JobScheduler.Domain.Model;
using Collaborative.C2.Toolkit.JobScheduler.Core;
using Collaborative.C2.Toolkit.JobScheduler.Integration.Interface;
using Collaborative.M3.ServerSdk.Core.Logging;
using Collaborative.M3.ServerSdk.Core.Common.Business.Implementation;

namespace Collaborative.C2.Toolkit.JobScheduler.Business.Implementation
{
    public class RecurringJobManagerService
    {
        #region Fields
        private IRecurringJobDataIntegrationService m3RecurringJobDataIntegrationService;
        private M3Logger logger = M3Logger.getLogger("RecurringJobManagerService");
        private NeverEndingTask _neverEndingTask = new NeverEndingTask();
        #endregion

        #region Properties
        public IRecurringJobDataIntegrationService RecurringJobDataIntegrationService
        {
            get
            {
                //If _m3RecurringJobDataIntegrationService has been set (usually for mock unit tests) then return the value set
                //otherwise set the default service and assign it to the global variable
                if (m3RecurringJobDataIntegrationService != null)
                {
                    return m3RecurringJobDataIntegrationService;
                }
                else
                {
                    return M3Resolver.Current.GetService<IRecurringJobDataIntegrationService>();
                }
            }
            set
            {
                m3RecurringJobDataIntegrationService = value;
            }
        }
        #endregion

        public async Task AddOrUpdate(
            [NotNull] string recurringJobId,
            [NotNull] Job job,
            [NotNull] string cronExpression)
        {
            await AddOrUpdate(recurringJobId, job, cronExpression, TimeZoneInfo.Utc);
        }

        public async Task AddOrUpdate(
            [NotNull] string recurringJobId,
            [NotNull] Job job,
            [NotNull] string cronExpression,
            [NotNull] TimeZoneInfo timeZone)
        {
            try
            {
                if (recurringJobId == null) throw new ArgumentNullException("Invalid recurringJobId");
                if (job == null) throw new ArgumentNullException("Invalid job");
                if (cronExpression == null) throw new ArgumentNullException("Invalid cronExpression");
                if (timeZone == null) throw new ArgumentNullException("Invalid timeZone");

                ValidateCronExpression(cronExpression);

                var schedule = CrontabSchedule.Parse(cronExpression);
                var nextOcurrence = schedule.GetNextOccurrence(DateTime.Now);

                job.NextRun = DateTime.Parse(nextOcurrence.ToString("f"));
                job.CronSchedule = cronExpression;
                job.Key = recurringJobId;

                SerializedJob serializedJob = SerializedJob.Serialize(job);

                RecurringJobResponse recurringJob = new RecurringJobResponse()
                {
                    CronSchedule = cronExpression,
                    Key = recurringJobId,
                    SerializedJob = JsonConvert.SerializeObject(SerializedJob.Serialize(job)),
                    NextRunTime = DateTime.Parse(schedule.GetNextOccurrence(DateTime.Now).ToString("f")),
                    Status = "SCHEDULED"
                };

                logger.info("AddOrUpdate", M3LoggerTags.M3BUS.ToString(), "Saving new Job");

                if (RecurringJobDataIntegrationService.GetByRecurringJobId(recurringJobId) == null)
                {
                    await RecurringJobDataIntegrationService.Save(recurringJob);
                }
            }
            catch
            {
                throw;
            }
        }

        public async Task AddOrUpdate(
            [NotNull] string recurringJobId,
            [NotNull] Job job,
            [NotNull] string cronExpression,
            [NotNull] string uri,
            [NotNull] TimeZoneInfo timeZone)
        {
            try
            {
                if (recurringJobId == null) throw new ArgumentNullException("Invalid recurringJobId");
                if (job == null) throw new ArgumentNullException("Invalid job");
                if (cronExpression == null) throw new ArgumentNullException("Invalid cronExpression");
                if (timeZone == null) throw new ArgumentNullException("Invalid timeZone");

                ValidateCronExpression(cronExpression);

                var schedule = CrontabSchedule.Parse(cronExpression);
                var nextOcurrence = schedule.GetNextOccurrence(DateTime.Now);

                job.NextRun = DateTime.Parse(nextOcurrence.ToString("f"));
                job.CronSchedule = cronExpression;
                job.Key = recurringJobId;

                SerializedJob serializedJob = SerializedJob.Serialize(job);

                RecurringJobResponse recurringJob = new RecurringJobResponse()
                {
                    CronSchedule = cronExpression,
                    Key = recurringJobId,
                    SerializedJob = JsonConvert.SerializeObject(SerializedJob.Serialize(job)),
                    NextRunTime = DateTime.Parse(schedule.GetNextOccurrence(DateTime.Now).ToString("f")),
                    //Uri = uri,
                    Status = "SCHEDULED"
                };

                logger.info("AddOrUpdate", M3LoggerTags.M3BUS.ToString(), "Saving new Job");
                if (RecurringJobDataIntegrationService.GetByRecurringJobId(recurringJobId) == null)
                {
                    await RecurringJobDataIntegrationService.Save(recurringJob);
                }
            }
            catch
            {
                throw;
            }
        }

        public async Task ExecuteRecurringJobs()
        {
            logger.info("ExecuteRecurringJobs", M3LoggerTags.M3BUS.ToString(), "Verifying if there are jobs to execute");

            IList<RecurringJobResponse> allJobs = new List<RecurringJobResponse>();

            //Get all the jobs from the DB - Since this property is referenced once no need to create an additional variable
            allJobs = await RecurringJobDataIntegrationService.GetAllJobs(DateTime.Parse(DateTime.Now.ToString("f")));

            logger.info("ExecuteRecurringJobs", M3LoggerTags.M3BUS.ToString(), "Executed GetAllJobs");
            //Create a list of all the jobs that are about to be executed based on the current time.
            //All the jobs that are supposed to run now or before needs to be executed, since the poll is every 15 seconds and because the min schedule time is 1 minute
            //we won't have an issue on missing a Job.

            IEnumerable<Task> asyncJobExecutions = (from job in allJobs
                                                    select JobExecutionAsync(JsonConvert.DeserializeObject<SerializedJob>(job.SerializedJob).Deserialize()));

            await AsyncExecuteAllJobs(asyncJobExecutions);
        }

        private async Task JobExecutionAsync(Job job)
        {
            logger.info("AsyncJobExecution", M3LoggerTags.M3BUS.ToString(), "Executing a Job");
            await Task.Run(async () => 
                {
                    IRecurringJobDataIntegrationService recurringJobDataIntegrationService = null;

                    try
                    {
                        //Getting the reference only once so we don't resolve it every time
                        recurringJobDataIntegrationService = RecurringJobDataIntegrationService;

                        //Update status to Processing 

                        //Get RecurringJobDataIntegrationService only once since the GetService method might create a transaction scope also
                        //as a best practice if we call the property multiple times it will create the instance by using reflection which impacts 
                        //the performance

                        await recurringJobDataIntegrationService.SetJobStatus(job.Key, "PROCESSING");

                        //Static class verification, .NET CLR does not know about static classes instead you will check for abstract and sealed
                        if (job.Type.IsAbstract && job.Type.IsSealed)
                        {
                            dynamic responseFromStatic = job.Method.Invoke(null, job.Args.ToArray());

                            //Update Job information
                            string nextRun = CrontabSchedule.Parse(job.CronSchedule).GetNextOccurrence(job.NextRun).ToString("f");
                            job.NextRun = DateTime.Parse(nextRun);

                            //After Job is executed update it with the next run time and set the status to scheduled

                            await
                            recurringJobDataIntegrationService.Update(new RecurringJobResponse()
                            {
                                CronSchedule = job.CronSchedule,
                                Key = job.Key,
                                SerializedJob = JsonConvert.SerializeObject(SerializedJob.Serialize(job)),
                                NextRunTime = job.NextRun,
                                Status = "SCHEDULED"
                            });
                        }
                        else
                        {
                            dynamic instanceOfAClass = Activator.CreateInstance(job.Type);
                            dynamic responseFromNonStatic = job.Method.Invoke(instanceOfAClass, job.Args.ToArray());

                            //Update Job information
                            string nextRun = CrontabSchedule.Parse(job.CronSchedule).GetNextOccurrence(job.NextRun).ToString("f");
                            job.NextRun = DateTime.Parse(nextRun);

                            //After Job is executed update it with the next run time and set the status to scheduled

                            await
                            recurringJobDataIntegrationService.Update(new RecurringJobResponse()
                            {
                                CronSchedule = job.CronSchedule,
                                Key = job.Key,
                                SerializedJob = JsonConvert.SerializeObject(SerializedJob.Serialize(job)),
                                NextRunTime = job.NextRun,
                                Status = "SCHEDULED"
                            });
                        }
                    }
                    catch
                    {
                        //Add to the log table 
                    }
                });
        }

        private async Task AsyncExecuteAllJobs(IEnumerable<Task> allAsyncJobs)
        {
            //Materialize all the jobs and execute them in parallel
            foreach (var job in allAsyncJobs)
            {
                await job;
            }
        }
        private static CancellationTokenSource _wtoken = new CancellationTokenSource();

        public async Task AddJobToNeverEndingTask(Func<DateTimeOffset, CancellationToken, Task> methodCall, TimeSpan time)
        {
            ITargetBlock<DateTimeOffset> task = _neverEndingTask
                .CreateNeverEndingTask(methodCall, _wtoken.Token, Convert.ToInt32(time.TotalMilliseconds));

            //Start the task and post the time
            task.Post(DateTimeOffset.Now);
        }

        public async Task Trigger(string recurringJobId)
        {
            if (string.IsNullOrEmpty(recurringJobId))
            {
                throw new ArgumentNullException("Invalid recurringJobId");
            }

            RecurringJobResponse recurringJob = null;

            recurringJob = await RecurringJobDataIntegrationService.GetByRecurringJobId(recurringJobId);

            Job job = JsonConvert
                .DeserializeObject<SerializedJob>(recurringJob.SerializedJob)
                .Deserialize();
        }

        private static void ValidateCronExpression(string cronExpression)
        {
            try
            {
                var schedule = CrontabSchedule.Parse(cronExpression);
                schedule.GetNextOccurrence(DateTime.UtcNow);
            }
            catch (Exception ex)
            {
                throw new ArgumentException("CRON expression is invalid. Please see the inner exception for details.", "cronExpression", ex);
            }
        }
    }
}
