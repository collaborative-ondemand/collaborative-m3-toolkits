﻿using Collaborative.C2.Toolkit.JobScheduler.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.C2.Toolkit.JobScheduler.Integration.Interface
{
    public interface IRecurringJobDataIntegrationService
    {
        Task Save(RecurringJobResponse recurringJob);
        Task Update(RecurringJobResponse recurringJob);
        Task SetJobStatus(string recurringJobId, string jobStatus);
        Task<RecurringJobResponse> GetByRecurringJobId(string recurringJobId);
        Task<IList<RecurringJobResponse>> GetAllJobs(DateTime executionTime);
    }
}
