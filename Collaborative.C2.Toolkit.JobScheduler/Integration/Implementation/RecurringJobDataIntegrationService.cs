﻿using Collaborative.C2.Toolkit.JobScheduler.Integration.Interface;
using Collaborative.C2.Toolkit.JobScheduler.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.C2.Toolkit.JobScheduler.Integration.Implementation
{
    public class RecurringJobDataIntegrationService : IRecurringJobDataIntegrationService
    {
        public async Task Save(RecurringJobResponse recurringJob)
        {
            throw new NotImplementedException();
        }

        public async Task Update(RecurringJobResponse recurringJob)
        {
            throw new NotImplementedException();
        }

        public async Task SetJobStatus(string recurringJobId, string jobStatus)
        {
            throw new NotImplementedException();
        }

        public async Task<RecurringJobResponse> GetByRecurringJobId(string recurringJobId)
        {
            throw new NotImplementedException();
        }

        public async Task<IList<RecurringJobResponse>> GetAllJobs(DateTime executionTime)
        {
            throw new NotImplementedException();
        }
    }
}
