﻿using Collaborative.C2.Toolkit.JobScheduler.Business;
using Collaborative.C2.Toolkit.JobScheduler.Business.Implementation;
using Collaborative.C2.Toolkit.JobScheduler.Configuration;
using Collaborative.C2.Toolkit.JobScheduler.Core;
using Collaborative.M3.ServerSdk.Core.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks.Dataflow;
using System.Web;

namespace Collaborative.C2.Toolkit.JobScheduler.Server
{
    public class RecurringJobServer
    {
        private static CancellationTokenSource wtoken;
        private static ITargetBlock<DateTimeOffset> task;
        private static M3Logger logger = M3Logger.getLogger("RecurringJobServer");
        private const int WATCH_DOG_AWAKE_IN = 30 * 1000;

        public static void StartServer()
        {
            //Start the server using public static cancellation tokens, these tokens will be used to later stop the server if we need to
            logger.info("StartServer", "CTRL", "Starting Server");
            wtoken = new CancellationTokenSource();

            NeverEndingTask neverEndingTask = new NeverEndingTask();
            //Run watch every 30 seconds
            task = neverEndingTask
                .CreateNeverEndingTask((now, ct) => RecurringJobService.WatchDog(), wtoken.Token, WATCH_DOG_AWAKE_IN);

            // Start the task.  Post the time.
            task.Post(DateTimeOffset.Now);
        }

        public static void StopServer()
        {
            //Stop Worker
            // CancellationTokenSource implements IDisposable.
            using (wtoken)
            {
                // Cancel.  This will cancel the task.
                wtoken.Cancel();
            }

            // Set everything to null, since the references
            // are on the class level and keeping them around
            // is holding onto invalid state.
            wtoken = null;
            task = null;
        }
    }
}