﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.C2.Toolkit.JobScheduler.Core
{
    internal static class TypeExtensions
    {
        public static string ToGenericTypeString(this Type t)
        {
            if (!t.GetTypeInfo().IsGenericType)
            {
                return t.Name;
            }

            var genericTypeName = t.GetGenericTypeDefinition().Name;
            genericTypeName = genericTypeName.Substring(0, genericTypeName.IndexOf('`'));

            var genericArgs = string.Join(",", t.GetGenericArguments().Select(ToGenericTypeString).ToArray());

            return genericTypeName + "<" + genericArgs + ">";
        }
    }
}
