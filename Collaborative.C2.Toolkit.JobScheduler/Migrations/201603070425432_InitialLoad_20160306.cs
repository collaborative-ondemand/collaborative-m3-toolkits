namespace Collaborative.C2.Toolkit.JobScheduler.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialLoad_20160306 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "JOB.C2_JOB_SCHEDULE",
                c => new
                    {
                        JOB_ID = c.Int(nullable: false, identity: true),
                        SCHEDULE_KEY = c.String(nullable: false, maxLength: 100),
                        JSON_SERIALIZED = c.String(maxLength: 800),
                        CRON_SCHEDULE_FORMAT = c.String(nullable: false, maxLength: 20),
                        NEXT_RUN_TIME = c.DateTime(nullable: false),
                        LAST_SUCCESSFUL_RUN = c.DateTime(),
                        LAST_FAILURE_TIME = c.DateTime(),
                        STATUS = c.String(maxLength: 25),
                        MODIFIED_BY = c.String(nullable: false, maxLength: 38),
                        MODIFIED_DATE = c.DateTime(nullable: false),
                        CREATED_BY = c.String(nullable: false, maxLength: 38),
                        CREATED_DATE = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.JOB_ID);
            
        }
        
        public override void Down()
        {
            DropTable("JOB.C2_JOB_SCHEDULE");
        }
    }
}
