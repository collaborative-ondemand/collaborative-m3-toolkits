﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Collaborative.C2.Toolkit.Tokenizer.Helpers
{
    internal class Helper
    {
        // Dictionary to store cached properites
        private static IDictionary<string, PropertyInfo[]> _propertiesCache = new Dictionary<string, PropertyInfo[]>();
        // Help with locking
        private static ReaderWriterLockSlim _propertiesCacheLock = new ReaderWriterLockSlim();

        #region Reflection Helper

        /// <summary>
        /// Get an array of PropertyInfo for this type
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns>PropertyInfo[] for this type</returns>
        public static PropertyInfo[] GetCachedProperties<TEntity>(TEntity entity, out string entityName)
        {
            PropertyInfo[] props = new PropertyInfo[0];

            entityName = entity.GetType().FullName;

            if (_propertiesCacheLock.TryEnterUpgradeableReadLock(100))
            {
                try
                {
                    if (!_propertiesCache.TryGetValue(entityName, out props))
                    {
                        props = entity.GetType().GetProperties();

                        if (_propertiesCacheLock.TryEnterWriteLock(100))
                        {
                            try
                            {
                                _propertiesCache.Add(entity.GetType().FullName, props);
                            }
                            finally
                            {
                                _propertiesCacheLock.ExitWriteLock();
                            }
                        }
                    }
                }
                finally
                {
                    _propertiesCacheLock.ExitUpgradeableReadLock();
                }
                return props;
            }
            else
            {
                return entity.GetType().GetProperties();
            }
        }

        #endregion
    }
}
