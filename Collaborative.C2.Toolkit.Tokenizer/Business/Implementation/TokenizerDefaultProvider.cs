﻿using Collaborative.C2.Toolkit.Tokenizer.Business.Interface;
using Collaborative.C2.Toolkit.Tokenizer.Domain.CustomAttributes;
using Collaborative.C2.Toolkit.Tokenizer.Domain.Model.Tokens;
using Collaborative.C2.Toolkit.Tokenizer.Helpers;
using Collaborative.M3.ServerSdk.Core.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Collaborative.C2.Toolkit.Tokenizer.Business.Implementation
{
    public class TokenizerDefaultProvider : ITokenizerProvider
    {
        /// <summary>
        /// Replace all of the token key occurences inside the specified template text with corresponded token values
        /// </summary>
        /// <param name="template">The template with token keys inside</param>
        /// <param name="tokens">The sequence of tokens to use</param>
        /// <param name="htmlEncode">The value indicating whether tokens should be HTML encoded</param>
        /// <returns>Text with all token keys replaces by token value</returns>
        public string Replace<TToken>(string template, IEnumerable<TToken> tokens, bool htmlEncode, bool caseInvariantReplacement = true)
            where TToken : BaseToken, new()
        {
            if (string.IsNullOrWhiteSpace(template))
                throw new ArgumentNullException("template");

            if (tokens == null)
                throw new ArgumentNullException("tokens");

            foreach (var token in tokens)
            {
                string tokenValue = token.Value;
                //do not encode URLs
                if (htmlEncode && !token.NeverHtmlEncoded)
                    tokenValue = HttpUtility.HtmlEncode(tokenValue);
                template = Replace(template, String.Format(@"%{0}%", token.Key), tokenValue, caseInvariantReplacement);
            }
            return template;
        }

        private string Replace(string original, string pattern, string replacement, bool caseInvariantReplacement)
        {
            StringComparison stringComparison = StringComparison.OrdinalIgnoreCase;

            if (!caseInvariantReplacement)
                stringComparison = StringComparison.Ordinal;

            if (stringComparison == StringComparison.Ordinal)
            {
                return original.Replace(pattern, replacement);
            }
            else
            {
                int count, position0, position1;
                count = position0 = position1 = 0;
                int inc = (original.Length / pattern.Length) * (replacement.Length - pattern.Length);
                char[] chars = new char[original.Length + Math.Max(0, inc)];
                while ((position1 = original.IndexOf(pattern, position0, stringComparison)) != -1)
                {
                    for (int i = position0; i < position1; ++i)
                        chars[count++] = original[i];
                    for (int i = 0; i < replacement.Length; ++i)
                        chars[count++] = replacement[i];
                    position0 = position1 + pattern.Length;
                }
                if (position0 == 0) return original;
                for (int i = position0; i < original.Length; ++i)
                    chars[count++] = original[i];
                return new string(chars, 0, count);
            }
        }

        public IList<TToken> GetTokenList<TToken, TEntity>(IList<TEntity> dataSource)
            where TToken : BaseToken, new()
        {
            IList<string> tokensAdded = new List<string>();
            IList<TToken> tokens = new List<TToken>();
            TToken token = null;
            string entityName = string.Empty;

            foreach (TEntity entity in dataSource)
            {
                IEnumerable<PropertyInfo> pdfProperties = Helper.GetCachedProperties<TEntity>(entity, out entityName)
                                                                .Where(property => property.GetCustomAttributes<TokenAttribute>().Count() > 0);

                //Making sure that only one Entity Type is used to retrieve tokens.
                //Since TEntity is type of BaseEntity it means you could send a list of Entities (all inherits from BaseEntity) Person, Address, Wages
                //In the case that you send two Organizations in the same IList<TEntity> only the first one will be used to retrieve its tokens and values the next ones
                //will be omitted.

                if (!tokensAdded.Contains(entityName))
                {
                    tokensAdded.Add(entityName);
                    TokenAttribute tokenAttribute = null;

                    //Check invariant, there should be tokens if not, do not let if fail just move to the next item
                    if (pdfProperties.Count() == 0)
                        continue;

                    foreach (PropertyInfo item in pdfProperties)
                    {
                        //Get the first because there should be only one PdfRender attribute per property, otherwise the rest will be ignored
                        tokenAttribute = item.GetCustomAttributes<TokenAttribute>().FirstOrDefault();
                        token = new TToken();
                        token.Key = tokenAttribute.TokenKey;
                        token.Value = item.GetValue(entity).ToStringEx();
                        tokens.Add(token);
                    }
                }
            }
            return tokens;
        }
    }
}
