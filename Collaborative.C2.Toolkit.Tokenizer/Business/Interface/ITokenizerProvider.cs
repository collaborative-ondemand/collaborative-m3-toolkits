﻿using Collaborative.C2.Toolkit.Tokenizer.Domain.Model.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.C2.Toolkit.Tokenizer.Business.Interface
{
    public interface ITokenizerProvider
    {
        string Replace<TToken>(string template, IEnumerable<TToken> tokens, bool htmlEncode, bool caseInvariantReplacement = true)
            where TToken : BaseToken, new();
        IList<TToken> GetTokenList<TToken, TEntity>(IList<TEntity> dataSource)
            where TToken : BaseToken, new();
    }
}
