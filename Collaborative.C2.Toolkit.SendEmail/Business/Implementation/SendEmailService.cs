﻿using Collaborative.C2.Toolkit.JobScheduler.Configuration;
using Collaborative.C2.Toolkit.SendEmail.Business.Helper;
using Collaborative.C2.Toolkit.SendEmail.Configuration;
using Collaborative.C2.Toolkit.SendEmail.Domain.Model.Email;
using Collaborative.C2.Toolkit.SendEmail.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.C2.Toolkit.SendEmail.Business.Implementation
{
    internal class SendEmailService
    {
        public static async Task Send()
        {
            try
            {
                Console.WriteLine("I entered: " + DateTime.Now.ToLongTimeString());
                EmailMessage queue = null;
                //Read from the queue and Dequeue the number of emails configured (default is 5)
                for (int i = 0; i < SendEmailConfiguration.GetSendEmailConfiguration().NumberOfEmailsToDequeue; i++)
                {
                    if(!QueueEmailHelper.Instance.ExistsItemsToDequeue())
                    {
                        break;
                    }
                    queue = QueueEmailHelper.Instance.Dequeue();

                    //Send email
                    SendEmailHelper.SendEmail(queue);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
