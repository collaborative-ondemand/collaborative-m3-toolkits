﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.C2.Toolkit.SendEmail.Business.Interface
{
    public interface IEncryptDecryptProvider
    {
        string EncryptText(string plainText, string encryptionPrivateKey = "");
        string DecryptText(string cipherText, string encryptionPrivateKey = "");
        string CreateSaltKey(int size);
        string CreateHash(string clearText, string saltkey, string hashFormat);
    }
}
