﻿using Collaborative.C2.Toolkit.SendEmail.Business.Interface;
using Collaborative.C2.Toolkit.Tokenizer.Business.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Collaborative.C2.Toolkit.SendEmail.Configuration
{
    public class SendEmailConfiguration
    {
        #region Fields
        private static SendEmailOptions _jobSchedulerServerOptions;
        #endregion

        public static void SetSendEmailConfiguration(SendEmailOptions options)
        {
            _jobSchedulerServerOptions = options;
        }

        public static SendEmailOptions GetSendEmailConfiguration()
        {
            return _jobSchedulerServerOptions;
        }
    }

    public class SendEmailOptions
    {
        public DataSourcePollingType DataSourcePolling { get; set; }
        /// <summary>
        /// This property will only work if we set DataSourcePollingType >= 1 hour
        /// </summary>
        public bool AdjustDataSourcePolling { get; set; }
        public int NumberOfEmailsToDequeue { get; set; }
        public TimeSpan DequeueEmailsAfter { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public bool SslEnabled { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public IEncryptDecryptProvider EncryptDecryptProvider { get; set; }
        public ITokenizerProvider TokenizerProvider { get; set; }
    }

    public enum DataSourcePollingType
    {
        None = 0,
        Minutely = 1,
        FiveMinutes = 2,
        TenMinutes = 3,
        ThirtyMinutes = 4,
        Hourly = 5,
        TwoHours = 6,
        ThreeHours = 7
    }
}