﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.C2.Toolkit.SendEmail.Domain.Model.Email
{
    public class EmailAttachment
    {
        public byte[] Bytes { get; set; }
        public string Name { get; set; }
        public AttachmentTypeEnum AttachmentType { get; set; }
    }

    public enum AttachmentTypeEnum
    {
        Zip = 0,
        Excel = 1,
        Pdf = 2
    }
}
