﻿using Collaborative.C2.Toolkit.SendEmail.Business.Interface;
using Collaborative.C2.Toolkit.SendEmail.Configuration;
using Collaborative.C2.Toolkit.SendEmail.Domain.Model.Email;
using Collaborative.C2.Toolkit.Tokenizer.Business.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.C2.Toolkit.SendEmail.Helper
{
    public static class SendEmailHelper
    {
        public static void SendEmail(EmailMessage emailMessage)
        {
            try
            {
                //Getting providers
                IEncryptDecryptProvider encryptDecryptProvider = 
                    SendEmailConfiguration.GetSendEmailConfiguration().EncryptDecryptProvider;

                ITokenizerProvider tokenizerProvider = 
                    SendEmailConfiguration.GetSendEmailConfiguration().TokenizerProvider;

                string username = SendEmailConfiguration.GetSendEmailConfiguration().Username;
                string host = SendEmailConfiguration.GetSendEmailConfiguration().Host;
                int port = SendEmailConfiguration.GetSendEmailConfiguration().Port;
                bool sslEnabled = SendEmailConfiguration.GetSendEmailConfiguration().SslEnabled;

                //Decrypting password
                string clearTextPassword = string.Empty;

                if (encryptDecryptProvider != null)
                {
                    clearTextPassword = encryptDecryptProvider
                                        .DecryptText(SendEmailConfiguration.GetSendEmailConfiguration().Password);
                }
                else
                {
                    clearTextPassword = SendEmailConfiguration.GetSendEmailConfiguration().Password;
                }


                //Validate invariants
                if (String.IsNullOrWhiteSpace(emailMessage.From))
                {
                    throw new ArgumentException("Invalid From email");
                }

                if (emailMessage.To == null || emailMessage.To.Count() == 0)
                {
                    throw new ArgumentException("Invalid To email");
                }

                if(emailMessage.TokenList != null && emailMessage.TokenList.Count() > 0 && tokenizerProvider == null)
                {
                    throw new ArgumentException("A token list was passed but there is no token provider");
                }

                //Set email message
                var message = new MailMessage();

                message.From = new MailAddress(emailMessage.From);

                foreach (var toEmail in emailMessage.To.Where(toValue => !String.IsNullOrEmpty(toValue)))
                {
                    message.To.Add(toEmail.Trim());
                }

                if (emailMessage.BCC != null && emailMessage.BCC.Count() > 0)
                {
                    foreach (var address in emailMessage.BCC.Where(bccValue => !String.IsNullOrEmpty(bccValue)))
                    {
                        message.Bcc.Add(address.Trim());
                    }
                }

                if (emailMessage.CC != null && emailMessage.CC.Count() > 0)
                {
                    foreach (var address in emailMessage.CC.Where(ccValue => !String.IsNullOrEmpty(ccValue)))
                    {
                        message.CC.Add(address.Trim());
                    }
                }

                if (emailMessage.Attachments != null && emailMessage.Attachments.Count() > 0)
                {
                    foreach (EmailAttachment attachment in emailMessage.Attachments)
                    {
                        Stream stream = new MemoryStream(attachment.Bytes);
                        ContentType ct = new ContentType();

                        //Setup attachment type and name
                        if (attachment.AttachmentType == AttachmentTypeEnum.Excel)
                        {
                            ct.MediaType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                            //Format the file, add excel extension if it is not specified
                            ct.Name = attachment.Name.EndsWith(".xslx") || attachment.Name.EndsWith(".xsl") ? attachment.Name : attachment.Name + ".xlsx";
                        }
                        else if (attachment.AttachmentType == AttachmentTypeEnum.Zip)
                        {
                            ct.MediaType = "application/zip";
                            ct.Name = attachment.Name.EndsWith(".zip") ? attachment.Name : attachment.Name + ".zip";
                        }
                        else if (attachment.AttachmentType == AttachmentTypeEnum.Pdf)
                        {
                            ct.MediaType = "application/pdf";
                            ct.Name = attachment.Name.EndsWith(".pdf") ? attachment.Name : attachment.Name + ".pdf";
                        }

                        // Create the attachment.
                        Attachment data = new Attachment(stream, ct);
                        // Add the attachment to the message.
                        message.Attachments.Add(data);
                    }
                }
                
                string body = string.Empty;
                string subject = string.Empty;

                if (emailMessage.TokenList != null && emailMessage.TokenList.Count() > 0)
                {
                    body = tokenizerProvider.Replace(emailMessage.Body, emailMessage.TokenList, htmlEncode: false);
                    subject = tokenizerProvider.Replace(emailMessage.Subject, emailMessage.TokenList, htmlEncode: false);
                    message.Subject = subject;
                    message.Body = body;
                }
                else
                {
                    message.Subject = emailMessage.Subject;
                    message.Body = emailMessage.Body;
                }
                
                message.IsBodyHtml = true;

                var smtpClient = new SmtpClient(host, port);
                if(!String.IsNullOrWhiteSpace(username) && !String.IsNullOrWhiteSpace(clearTextPassword))
                {
                    smtpClient.Credentials = new NetworkCredential(username, clearTextPassword);
                }
                
                smtpClient.EnableSsl = sslEnabled;
                
                smtpClient.Send(message);

                //Clean up
                message.Dispose();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
