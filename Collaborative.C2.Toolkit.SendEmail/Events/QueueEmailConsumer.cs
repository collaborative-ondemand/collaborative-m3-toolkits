﻿using Collaborative.C2.Toolkit.SendEmail.Business.Helper;
using Collaborative.C2.Toolkit.SendEmail.Domain.Model.Email;
using Collaborative.C2.Toolkit.SendEmail.Domain.Model.Events;
using Collaborative.C2.Toolkit.EventPublisher.Business.Interface;

namespace Collaborative.C2.Toolkit.SendEmail.Events
{
    public class QueueEmailConsumer : 
        IConsumer<QueueEmailEvent>
    {
        public void HandleEvent(QueueEmailEvent eventMessage)
        {
            QueueEmailHelper.Instance.Enqueue(eventMessage.Entity);
        }
    }
}
