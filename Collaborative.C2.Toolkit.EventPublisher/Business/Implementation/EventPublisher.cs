﻿using System;
using System.Linq;
using Collaborative.C2.Toolkit.EventPublisher.Business.Interface;
using Collaborative.M3.ServerSdk.Core.Common.Business.Implementation;
using Collaborative.M3.ServerSdk.Core.Logging;

namespace Collaborative.C2.Toolkit.EventPublisher.Business.Implementation
{
    /// <summary>
    /// Evnt publisher
    /// </summary>
    public class EventPublisher : IEventPublisher
    {
        private readonly ISubscriptionService subscriptionService;
        private static readonly M3Logger logger = M3Logger.getLogger("EventPublisher");

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="subscriptionService"></param>
        public EventPublisher(ISubscriptionService subscriptionService)
        {
            this.subscriptionService = subscriptionService;
        }

        /// <summary>
        /// Publish to cunsumer
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="consumer">Event consumer</param>
        /// <param name="eventMessage">Event message</param>
        protected virtual void PublishToConsumer<T>(IConsumer<T> consumer, T eventMessage)
        {
            try
            {
                consumer.HandleEvent(eventMessage);
            }
            catch (Exception exc)
            {
                //we put in to nested try-catch to prevent possible cyclic (if some error occurs)
                try
                {
                    logger.error("PublishToConsumer<T>", M3LoggerTags.M3BUS.ToString(), exc.Message);
                }
                catch (Exception)
                {
                    //do nothing
                }
            }
        }

        /// <summary>
        /// Publish event
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <param name="eventMessage">Event message</param>
        public void Publish<T>(T eventMessage)
        {
            var subscriptions = subscriptionService.GetSubscriptions<T>();
            subscriptions.ToList().ForEach(consumer => PublishToConsumer(consumer, eventMessage));
        }

    }
}
