﻿using System.Collections.Generic;
using Collaborative.C2.Toolkit.EventPublisher.Business.Interface;
using Collaborative.M3.ServerSdk.Core.Common.Business.Implementation;

namespace Collaborative.C2.Toolkit.EventPublisher.Business.Implementation
{
    /// <summary>
    /// Event subscrption service
    /// </summary>
    public class SubscriptionService : ISubscriptionService
    {
        /// <summary>
        /// Get subscriptions
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <returns>Event consumers</returns>
        public IEnumerable<IConsumer<T>> GetSubscriptions<T>()
        {
            return M3Resolver.Current.GetServices<IConsumer<T>>();
        }
    }
}
