﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.C2.Toolkit.JobScheduler.Integration.EF
{
    public class JobDbConfiguration : DbConfiguration
    {
        public JobDbConfiguration()
        {
            //Uncomment for Oracle Provider
            //this.SetHistoryContext("Oracle.ManagedDataAccess.Client",
            //    (connection, defaultSchema) => new M3HistoryContext(connection, defaultSchema));

            this.SetHistoryContext("System.Data.SqlClient",
                (connection, defaultSchema) => new JobHistoryContext(connection, defaultSchema));
        }
    } 
}
