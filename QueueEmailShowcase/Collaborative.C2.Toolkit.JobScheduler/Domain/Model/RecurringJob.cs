﻿using Collaborative.M3.ServerSdk.Core.Common.Domain;
using Collaborative.M3.ServerSdk.Core.Identity.Domain.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.C2.Toolkit.JobScheduler.Domain.Model
{
    public class RecurringJob : BaseEntity
    {
        public string Key { get; set; }
        public string SerializedJob { get; set; }
        public string CronSchedule { get; set; }
        public DateTime NextRunTime { get; set; }
        public DateTime? LastSuccessfulRun { get; set; }
        public DateTime? LastFailureTime { get; set; }
        public string Status { get; set; }
    }
}
