﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.C2.Toolkit.JobScheduler.Domain.Model
{
    public class SerializedJob
    {
        public SerializedJob(
            string type, 
            string method, 
            string parameterTypes, 
            string arguments,
            string cronSchedule,
            string nextRun,
            string lastSuccessfulRun,
            string key,
            string lastFailureTime,
            string status)
        {
            Type = type;
            Method = method;
            ParameterTypes = parameterTypes;
            Arguments = arguments;
            CronSchedule = cronSchedule;
            NextRun = nextRun;
            LastSuccessfulRun = lastSuccessfulRun;
            Key = key;
            LastFailureTime = lastFailureTime;
            Status = status;
        }

        public string Type { get; private set; }
        public string Method { get; private set; }
        public string ParameterTypes { get; private set; }
        public string Arguments { get; set; }
        public string CronSchedule { get; set; }
        public string NextRun { get; set; }
        public string Key { get; set; }
        public string LastSuccessfulRun { get; set; }
        public string LastFailureTime { get; set; }
        public string Status { get; set; }

        public static SerializedJob Serialize(Job job)
        {
            return new SerializedJob(
                job.Type.AssemblyQualifiedName,
                job.Method.Name,
                JsonConvert.SerializeObject(job.Method.GetParameters().Select(x => x.ParameterType).ToArray()),
                JsonConvert.SerializeObject(SerializeArguments(job.Args)),
                job.CronSchedule,
                job.NextRun.ToString(),
                job.LastSuccessfulRun.ToString(),
                job.Key,
                job.LastFailureTime.HasValue ? job.LastFailureTime.ToString() : string.Empty,
                job.Status);
        }

        internal static string[] SerializeArguments(IReadOnlyCollection<object> arguments)
        {
            var serializedArguments = new List<string>(arguments.Count);
            foreach (var argument in arguments)
            {
                string value = null;

                if (argument != null)
                {
                    if (argument is DateTime)
                    {
                        value = ((DateTime)argument).ToString("o", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        value = JsonConvert.SerializeObject(argument);
                    }
                }

                // Logic, related to optional parameters and their default values, 
                // can be skipped, because it is impossible to omit them in 
                // lambda-expressions (leads to a compile-time error).

                serializedArguments.Add(value);
            }

            return serializedArguments.ToArray();
        }

        public Job Deserialize()
        {
            try
            {
                var type = System.Type.GetType(Type, throwOnError: true, ignoreCase: true);
                var parameterTypes = JsonConvert.DeserializeObject<Type[]>(ParameterTypes);
                var method = GetNonOpenMatchingMethod(type, Method, parameterTypes);

                if (method == null)
                {
                    throw new InvalidOperationException(String.Format(
                        "The type `{0}` does not contain a method with signature '{1}({2})'",
                        type.FullName,
                        Method,
                        String.Join(", ", parameterTypes.Select(x => x.Name))));
                }

                var serializedArguments = JsonConvert.DeserializeObject<string[]>(Arguments);

                //Updated code only required by DCS FLC, it can be removed from the Core.

                //Replacing NextRun as part of the arguments, will get replaced always as last argument
                serializedArguments[serializedArguments.Length - 1] = NextRun;

                var arguments = DeserializeArguments(method, serializedArguments);

                DateTime nextRun = DateTime.MinValue;
                DateTime lastSuccessfulExecution = DateTime.MinValue;
                DateTime? lastFailureTime = null;

                if(!string.IsNullOrEmpty(NextRun))
                {
                    nextRun = DateTime.Parse(NextRun);
                }

                if(!string.IsNullOrEmpty(LastSuccessfulRun))
                {
                    lastSuccessfulExecution = DateTime.Parse(LastSuccessfulRun);
                }

                if(!string.IsNullOrEmpty(LastFailureTime))
                {
                    lastFailureTime = DateTime.Parse(LastFailureTime);
                }

                return new Job(type, method, CronSchedule, nextRun, lastSuccessfulExecution, Key, lastFailureTime, Status, arguments);
            }
            catch (Exception ex)
            {
                throw new Exception("Could not load the job. See inner exception for the details.", ex);
            }
        }

        internal static object[] DeserializeArguments(MethodInfo methodInfo, string[] arguments)
        {
            var parameters = methodInfo.GetParameters();
            var result = new List<object>(arguments.Length);

            for (var i = 0; i < parameters.Length; i++)
            {
                var parameter = parameters[i];
                var argument = arguments[i];

                object value;

                value = DeserializeArgument(argument, parameter.ParameterType);

                result.Add(value);
            }

            return result.ToArray();
        }

        private static object DeserializeArgument(string argument, Type type)
        {
            object value;
            try
            {
                if (type == null)
                {
                    throw new ArgumentNullException("Null Type not allowed.");
                }

                value = argument != null
                    ? JsonConvert.DeserializeObject(argument, type)
                    : null;
            }
            catch (Exception jsonException)
            {
                if (type == typeof(object))
                {
                    // Special case for handling object types, because string can not
                    // be converted to object type.
                    value = argument;
                }
                else
                {
                    try
                    {
                        var converter = TypeDescriptor.GetConverter(type);
                        value = converter.ConvertFromInvariantString(argument);
                    }
                    catch (Exception)
                    {
                        throw jsonException;
                    }
                }
            }
            return value;
        }

        private static IEnumerable<MethodInfo> GetAllMethods(Type type)
        {
            var methods = new List<MethodInfo>(type.GetMethods());

            if (type.IsInterface)
            {
                methods.AddRange(type.GetInterfaces().SelectMany(x => x.GetMethods()));
            }

            return methods;
        }

        private static MethodInfo GetNonOpenMatchingMethod(Type type, string name, Type[] parameterTypes)
        {
            var methodCandidates = GetAllMethods(type);

            foreach (var methodCandidate in methodCandidates)
            {
                if (!methodCandidate.Name.Equals(name, StringComparison.Ordinal))
                {
                    continue;
                }

                var parameters = methodCandidate.GetParameters();
                if (parameters.Length != parameterTypes.Length)
                {
                    continue;
                }

                var parameterTypesMatched = true;
                var genericArguments = new List<Type>();

                // Determining whether we can use this method candidate with
                // current parameter types.
                for (var i = 0; i < parameters.Length; i++)
                {
                    var parameter = parameters[i];
                    var parameterType = parameter.ParameterType;
                    var actualType = parameterTypes[i];

                    // Skipping generic parameters as we can use actual type.
                    if (parameterType.IsGenericParameter)
                    {
                        genericArguments.Add(actualType);
                        continue;
                    }

                    // Skipping non-generic parameters of assignable types.
                    if (parameterType.IsAssignableFrom(actualType)) continue;

                    parameterTypesMatched = false;
                    break;
                }

                if (!parameterTypesMatched) continue;

                // Return first found method candidate with matching parameters.
                return methodCandidate.ContainsGenericParameters
                    ? methodCandidate.MakeGenericMethod(genericArguments.ToArray())
                    : methodCandidate;
            }

            return null;
        }
    }
}
