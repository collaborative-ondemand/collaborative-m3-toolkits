﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Collaborative.C2.Toolkit.JobScheduler.Configuration
{
    public class RecurringJobConfiguration
    {
        #region Fields
        private static RecurringJobOptions jobSchedulerServerOptions;
        #endregion

        public static void SetJobSchedulerServerConfiguration(RecurringJobOptions options = null)
        {
            if (options != null)
            {
                jobSchedulerServerOptions = options;
            }
            else
            {
                //Use defaults
                jobSchedulerServerOptions = new RecurringJobOptions()
                                                {
                                                    DataSourcePolling = DataSourcePollingType.TenMinutes, //Every 10 minutes by default
                                                    AdjustDataSourcePolling = false
                                                };
            }
        }

        public static RecurringJobOptions GetJobSchedulerServerConfiguration()
        {
            return jobSchedulerServerOptions;
        }
    }

    public class RecurringJobOptions
    {
        public DataSourcePollingType DataSourcePolling { get; set; }
        /// <summary>
        /// This property will only work if we set DataSourcePollingType >= 1 hour
        /// </summary>
        public bool AdjustDataSourcePolling { get; set; }
    }

    public enum DataSourcePollingType
    {
        None = 0,
        Minutely = 1,
        FiveMinutes = 2,
        TenMinutes = 3,
        ThirtyMinutes = 4,
        Hourly = 5,
        TwoHours = 6,
        ThreeHours = 7
    }
}