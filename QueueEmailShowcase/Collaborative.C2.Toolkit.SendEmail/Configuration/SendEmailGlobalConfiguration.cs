﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Collaborative.C2.Toolkit.SendEmail.Configuration
{
    public class SendEmailGlobalConfiguration
    {
        private static readonly SendEmailGlobalConfiguration _configuration = new SendEmailGlobalConfiguration();

        public static SendEmailGlobalConfiguration Configuration
        {
            get { return _configuration; }
        }

        public static IGlobalConfiguration<T> Use<T>(
            T entry,
            Action<T> entryAction)
        {
            entryAction(entry);

            return new ConfigurationEntry<T>(entry);
        }

        public static IGlobalConfiguration<SendEmailOptions> UseServerConfiguration(SendEmailOptions options = null)
        {
            return Use<SendEmailOptions>(options, 
                (_options => 
                    SendEmailConfiguration.SetSendEmailConfiguration(_options)));
        }
    }

    public interface IGlobalConfiguration<out T>
    {
        T Entry { get; }
    }

    class ConfigurationEntry<T> : IGlobalConfiguration<T>
    {
        private readonly T _entry;

        public ConfigurationEntry(T entry)
        {
            _entry = entry;
        }

        public T Entry
        {
            get { return _entry; }
        }
    }
}