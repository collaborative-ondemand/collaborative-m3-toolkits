﻿using Collaborative.C2.Toolkit.EventPublisher.Business.Interface;
using Collaborative.C2.Toolkit.SendEmail.Domain.Model.Email;
using Collaborative.C2.Toolkit.SendEmail.Domain.Model.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.C2.Toolkit.SendEmail.Events.Extensions
{
    public static class EventPublisherExtension
    {
        public static void QueueEmail(this IEventPublisher eventPublisher, EmailMessage message)
        {
            eventPublisher.Publish(new QueueEmailEvent(message));
        }
    }
}
