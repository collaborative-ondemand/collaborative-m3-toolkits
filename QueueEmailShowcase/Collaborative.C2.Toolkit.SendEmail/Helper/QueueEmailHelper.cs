﻿using Collaborative.C2.Toolkit.JobScheduler.Business.Implementation;
using Collaborative.C2.Toolkit.JobScheduler.Configuration;
using Collaborative.C2.Toolkit.JobScheduler.Server;
using Collaborative.C2.Toolkit.SendEmail.Business.Implementation;
using Collaborative.C2.Toolkit.SendEmail.Configuration;
using Collaborative.C2.Toolkit.SendEmail.Domain.Model.Email;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Collaborative.C2.Toolkit.SendEmail.Business.Helper
{
    internal sealed class QueueEmailHelper
    {
        #region Fields
        private Queue<EmailMessage> _allQueuedEmails = null;
        private static readonly QueueEmailHelper _singleton = new QueueEmailHelper();
        private ReaderWriterLockSlim _readWriteLock = new ReaderWriterLockSlim();
        #endregion

        #region Constructor
        private QueueEmailHelper()
        {
            _allQueuedEmails = new Queue<EmailMessage>();
            StartQueueJob();
        }
        #endregion

        public static QueueEmailHelper Instance
        {
            get
            {
                return _singleton;
            }
        }

        public void Enqueue(EmailMessage emailToQueue)
        {
            _readWriteLock.EnterWriteLock();
            try
            {
                _allQueuedEmails.Enqueue(emailToQueue);
            }
            finally
            {
                _readWriteLock.ExitWriteLock();
            }
        }

        public EmailMessage Dequeue()
        {
            _readWriteLock.EnterReadLock();
            try
            {
                return _allQueuedEmails.Dequeue();
            }
            finally
            {
                _readWriteLock.ExitReadLock();
            }
        }

        public bool ExistsItemsToDequeue()
        {
            return _allQueuedEmails.Count > 0;
        }

        private void StartQueueJob()
        {
            try
            {
                //Setup Recurring Job Configuration only if it was not configured before
                if (RecurringJobConfiguration.GetJobSchedulerServerConfiguration() == null)
                {
                    RecurringJobGlobalConfiguration
                    .UseServerConfiguration(new RecurringJobOptions()
                    {
                        DataSourcePolling = JobScheduler.Configuration.DataSourcePollingType.Minutely,
                        AdjustDataSourcePolling = true
                    });

                    RecurringJobServer.StartServer();
                }

                //Setup Email Configuration only if was not configured before
                if (SendEmailConfiguration.GetSendEmailConfiguration() == null)
                {
                    SendEmailGlobalConfiguration
                        .UseServerConfiguration();
                }

                RecurringJobService
                    .AddOrUpdateNoStoragePersist((now,ct) => SendEmailService.Send(), SendEmailConfiguration.GetSendEmailConfiguration().DequeueEmailsAfter);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
