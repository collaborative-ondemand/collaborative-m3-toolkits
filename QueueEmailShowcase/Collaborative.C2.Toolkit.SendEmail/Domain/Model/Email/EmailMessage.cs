﻿using Collaborative.C2.Toolkit.Tokenizer.Domain.Model.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.C2.Toolkit.SendEmail.Domain.Model.Email
{
    public class EmailMessage
    {
        public string Subject { get; set; }
        public string Body { get; set; }
        public IEnumerable<string> To { get; set; }
        public string From { get; set; }
        public IEnumerable<string> BCC { get; set; }
        public IEnumerable<string> CC { get; set; }
        public IEnumerable<EmailAttachment> Attachments { get; set; }
        public IEnumerable<BaseToken> TokenList { get; set; }
        public bool IsHtml { get; set; }
    }
}
