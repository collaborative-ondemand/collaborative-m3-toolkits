﻿using Collaborative.C2.Toolkit.SendEmail.Domain.Model.Email;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.C2.Toolkit.SendEmail.Domain.Model.Events
{
    public class QueueEmailEvent
    {
        public QueueEmailEvent(EmailMessage entity)
        {
            this.Entity = entity;
        }

        public EmailMessage Entity { get; private set; }
    }
}
