﻿using Collaborative.C2.Toolkit.SendEmail.Events.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using Collaborative.C2.Toolkit.SendEmail.Domain.Model.Email;
using Collaborative.M3.ServerSdk.Core.Common.IoC;
using System.Reflection;
using Collaborative.C2.Toolkit.SendEmail.Configuration;
using Collaborative.C2.Toolkit.Tokenizer.Business.Implementation;
using Collaborative.C2.Toolkit.EventPublisher.Business.Interface;
using Collaborative.C2.Toolkit.Tokenizer.Domain.Model.Tokens;
using Collaborative.M3.ServerSdk.Core.Common.Business.Implementation;

namespace Collaborative.C2.Toolkit.ConApp.Showcase
{
    class Program
    {
        static void Main(string[] args)
        {
            RegisterDependenciesUsingDefaultSimpleInjector();
            ConfigureSendEmail();

            //This instance will get resolved by using Simple Injector IoC and the reason is because M3Resolver under the covers
            //uses Simple Injector, and if you inspect M3Resolver (if no Custom Resolver is injected) then you will notice
            //that uses M3ContainerManager.Container to return instances, which means that by registering the Interface - Concrete class
            //in RegisterDependenciesUsingDefaulSimpleInjector method, then M3Resolver will be able to resolve the instance.

            IEventPublisher _eventPublisher = M3Resolver.Current.GetService<IEventPublisher>();
            List<BaseToken> tokenList = new List<BaseToken>();

            tokenList.Add(new BaseToken()
            {
                Key = "YourName",
                Value = "Jorge",
                NeverHtmlEncoded = false
            });

            _eventPublisher.QueueEmail(new EmailMessage()
            {
                Subject = "Some Subject that goes to",
                Body = "Some message for",
                To = new string[] { "jorge.cotillo@gmail.com" },
                From = "testemaildcs@gmail.com",
                TokenList = tokenList
            });

            _eventPublisher.QueueEmail(new EmailMessage()
            {
                Subject = "Some Subject that goes to %YourName%",
                Body = "Some message for %YourName%",
                To = new string[] { "jcotillo@collaborative.com" },
                From = "testemaildcs@gmail.com",
                TokenList = tokenList
            });

            Console.ReadKey();
        }

        /// <summary>
        /// This method uses M3ContainerManager which is the default IoC Container used in M3DefaultDependencyServiceRegistration.
        /// If your project has a different IoC, then you will have to create your own M3Resolver and instruct how to resolver the Service
        /// or Services and register EventPublisher, SubscriptionService and IConsumer<>
        /// </summary>
        static void RegisterDependenciesUsingDefaultSimpleInjector()
        {
            //If your project is not planning on using any IoC then copy this method as is in your Global.asax
            //If your project is planning on using an IoC then the registration from below will reside in its own Registration class

            M3ContainerManager.Container.Register<IEventPublisher, Collaborative.C2.Toolkit.EventPublisher.Business.Implementation.EventPublisher>();
            M3ContainerManager.Container.Register<ISubscriptionService, Collaborative.C2.Toolkit.EventPublisher.Business.Implementation.SubscriptionService>();

            //Let's get the Assembly that contains implementations of IConsumer<T>
            //IConsumer<T> is the interface implemented by all the Consumers (Batch, RealTime and Queue)
            //by getting all the interfaces and registering them, SendEmail project will be able to call
            //the proper implementation
            Assembly sendEmailAssembly = AppDomain
                                        .CurrentDomain
                                        .GetAssemblies()
                                        .Where(i => i.FullName.Contains("Collaborative.C2.Toolkit.SendEmail"))
                                        .FirstOrDefault();

            Type openGenericType = typeof(IConsumer<>);

            List<Type> allConsumers = (from type in sendEmailAssembly.GetTypes()
                                       from interfaces in type.GetInterfaces()
                                       let baseType = type.BaseType
                                       where
                                       (baseType != null && baseType.IsGenericType &&
                                       openGenericType.IsAssignableFrom(baseType.GetGenericTypeDefinition())) ||
                                       (interfaces.IsGenericType &&
                                       openGenericType.IsAssignableFrom(interfaces.GetGenericTypeDefinition()))
                                       select type).ToList();

            M3ContainerManager.Container.RegisterCollection(openGenericType, allConsumers);
        }

        static void ConfigureSendEmail()
        {
            SendEmailGlobalConfiguration
                .UseServerConfiguration(new SendEmailOptions()
                {
                    NumberOfEmailsToDequeue = 10,
                    DequeueEmailsAfter = TimeSpan.FromSeconds(45),
                    EncryptDecryptProvider = null,//new M3EncryptDecryptDefaultProvider(),
                    Host = "smtp.gmail.com",
                    Username = "testemaildcs@gmail.com",
                    Password = "Toyota2013",
                    Port = 587,
                    SslEnabled = true,
                    TokenizerProvider = new TokenizerDefaultProvider()
                });
        }
    }
}
