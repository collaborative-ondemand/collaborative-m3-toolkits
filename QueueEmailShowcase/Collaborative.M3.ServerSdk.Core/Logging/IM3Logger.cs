﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Logging
{
    public interface IM3Logger
    {
        void Error(string errorMessage, Exception ex);
    }
}
