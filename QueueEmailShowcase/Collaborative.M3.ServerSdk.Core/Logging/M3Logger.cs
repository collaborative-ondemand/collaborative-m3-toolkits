﻿using Collaborative.M3.ServerSdk.Core.Config;
using Collaborative.M3.ServerSdk.Identity.Common.User;
using log4net;
using log4net.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Logging
{
    /// <summary>
    /// The M3Logger provides a logging wrapper for standardizing loggging formats.
    /// </summary>
    public class M3Logger
    {

        // Log Format - e.g. : context | message type (APPL OR PERF) | user | role | .... | message
        private static String logFormat = "{0}|{1}|{2}|{3}|{4}|{5}";
        
        // Message Types
        private static String APPLICATION_MSG_TYPE = "APPL";
        private static String PERFORMANCE_MSG_TYPE = "PERF";

        // Formatting Constants
        private static String ROLE_DELIMETER = ",";
        private static String STOPWATCH_NULL = "-";

        private ILog log;

        private void init(String classname)
        {
            this.log = LogManager.GetLogger(classname);
        }

        public static M3Logger getLogger(String classname)
        {
            M3Logger logger = new M3Logger();
            logger.init(classname);
            return logger;
        }

        public void debug(String context, String message) 
        {
            if (log.IsDebugEnabled) 
            {
                String formatted = format(context, 
                    APPLICATION_MSG_TYPE, 
                    M3User.GetM3UserFromCurrentContext().UserName,
                    string.Join(ROLE_DELIMETER, M3User.GetM3UserFromCurrentContext().RoleList.ToArray()),
                    STOPWATCH_NULL, 
                    message);
            
                log.Debug(formatted);
            }
        }

        public void debug(String context, Stopwatch stopwatch, String message) 
        {
            if (log.IsDebugEnabled)
            {
                String formatted = format(context,
                    PERFORMANCE_MSG_TYPE,
                    M3User.GetM3UserFromCurrentContext().UserName,
                    string.Join(ROLE_DELIMETER, M3User.GetM3UserFromCurrentContext().RoleList.ToArray()),
                    stopwatch.ElapsedMilliseconds.ToString(),
                    message);

                log.Debug(formatted);
            }
        }

        public void info(String context, String message) 
        {
            if (log.IsInfoEnabled) 
            {
                String formatted = format(context, 
                    APPLICATION_MSG_TYPE, 
                    "",//M3User.GetM3UserFromCurrentContext().UserName,
                    "",//string.Join(ROLE_DELIMETER, M3User.GetM3UserFromCurrentContext().RoleList.ToArray()),
                    STOPWATCH_NULL, 
                    message);
            
                log.Info(formatted);
            }
        }

        public void info(String context, Stopwatch stopwatch, String message) 
        {
            if (log.IsInfoEnabled)
            {
                String formatted = format(context,
                    PERFORMANCE_MSG_TYPE,
                    M3User.GetM3UserFromCurrentContext().UserName,
                    string.Join(ROLE_DELIMETER, M3User.GetM3UserFromCurrentContext().RoleList.ToArray()),
                    stopwatch.ElapsedMilliseconds.ToString(),
                    message);

                log.Info(formatted);
            }
        }

        public void warn(String context, String message)
        {
            if (log.IsWarnEnabled)
            {
                String formatted = format(context,
                    APPLICATION_MSG_TYPE,
                    M3User.GetM3UserFromCurrentContext().UserName,
                    string.Join(ROLE_DELIMETER, M3User.GetM3UserFromCurrentContext().RoleList.ToArray()),
                    STOPWATCH_NULL,
                    message);

                log.Warn(formatted);
            }
        }

        public void warn(String context, Stopwatch stopwatch, String message) 
        {
            if (log.IsWarnEnabled)
            {
                String formatted = format(context,
                    PERFORMANCE_MSG_TYPE,
                    M3User.GetM3UserFromCurrentContext().UserName,
                    string.Join(ROLE_DELIMETER, M3User.GetM3UserFromCurrentContext().RoleList.ToArray()),
                    stopwatch.ElapsedMilliseconds.ToString(),
                    message);

                log.Warn(formatted);
            }
        }

        public void error(String context, String message)
        {
            if (log.IsErrorEnabled)
            {
                String formatted = format(context,
                    APPLICATION_MSG_TYPE,
                    M3User.GetM3UserFromCurrentContext().UserName,
                    string.Join(ROLE_DELIMETER, M3User.GetM3UserFromCurrentContext().RoleList.ToArray()),
                    STOPWATCH_NULL,
                    message);

                log.Error(formatted);
            }
        }

        public void error(String context, Stopwatch stopwatch, String message) 
        {
            if (log.IsErrorEnabled)
            {
                String formatted = format(context,
                    PERFORMANCE_MSG_TYPE,
                    M3User.GetM3UserFromCurrentContext().UserName,
                    string.Join(ROLE_DELIMETER, M3User.GetM3UserFromCurrentContext().RoleList.ToArray()),
                    stopwatch.ElapsedMilliseconds.ToString(),
                    message);

                log.Error(formatted);
            }
        }

        public void fatal(String context, String message)
        {
            if (log.IsFatalEnabled)
            {
                String formatted = format(context,
                    APPLICATION_MSG_TYPE,
                    M3User.GetM3UserFromCurrentContext().UserName,
                    string.Join(ROLE_DELIMETER, M3User.GetM3UserFromCurrentContext().RoleList.ToArray()),
                    STOPWATCH_NULL,
                    message);

                log.Fatal(formatted);
            }
        }

        public void fatal(String context, Stopwatch stopwatch, String message)
        {
            if (log.IsFatalEnabled)
            {
                String formatted = format(context,
                    PERFORMANCE_MSG_TYPE,
                    M3User.GetM3UserFromCurrentContext().UserName,
                    string.Join(ROLE_DELIMETER, M3User.GetM3UserFromCurrentContext().RoleList.ToArray()),
                    stopwatch.ElapsedMilliseconds.ToString(),
                    message);

                log.Fatal(formatted);
            }
        }

        private static String format(String context, String messageType, String username, String roles, String millis, String message) {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat(logFormat, getApplicationName() + context, messageType, username, roles, millis, message);
            return sb.ToString();
        }

        private static String getApplicationName()
        {
            if (M3Config.ActiveConfig.M3LoggingConfig.applicationName != null)
            {
                return M3Config.ActiveConfig.M3LoggingConfig.applicationName + ".";
            }
            return "";
        }
    }

}
