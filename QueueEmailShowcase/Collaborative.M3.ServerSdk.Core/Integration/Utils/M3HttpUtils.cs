﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Collaborative.M3.ServerSdk.Core.Integration.Util
{
    public static class M3HttpUtils
    {
        public static T DeserializeJson<T>(string data)
        {
            try
            {
                var responseDeserialized = JsonConvert.DeserializeObject<T>(data);
                return responseDeserialized;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static T DeserializeXML<T>(string data) where T : new()
        {
            try
            {
                XDocument xdoc = XDocument.Parse(data);
                var serializer = new XmlSerializer(typeof(T));
                T responseEntity = new T();
                using (XmlReader reader = XmlReader.Create(xdoc.CreateReader(), null))
                {
                    responseEntity = (T)serializer.Deserialize(reader);
                }
                if (responseEntity != null)
                    return responseEntity;
                else
                    return default(T);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static XmlDocument DeserializeJsonToXML(string data)
        {
            try
            {
                return JsonConvert.DeserializeXmlNode(data);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static string SerializeJson(Object data)
        {
            try
            {
                var responseSerialized = JsonConvert
                    .SerializeObject(data,
                    new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore, MaxDepth = 1 });
                return responseSerialized;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static string SerializeXml(Object data)
        {
            try
            {
                var stringwriter = new System.IO.StringWriter();
                var serializer = new XmlSerializer(data.GetType());
                serializer.Serialize(stringwriter, data);
                return stringwriter.ToString();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static double GetCurrentMilli()
        {
            DateTime Jan1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan javaSpan = DateTime.UtcNow - Jan1970;
            return Math.Truncate(javaSpan.TotalMilliseconds);
        }

    }
}
