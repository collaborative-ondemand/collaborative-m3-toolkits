﻿using Collaborative.M3.ServerSdk.Core.Business.Common.Messaging;
using Collaborative.M3.ServerSdk.Core.Integration.Util;
using Collaborative.M3.ServerSdk.Core.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Integration
{
    /// <summary>
    /// HTTP Web Service Integration.  Facilitates external HTTP web service calls.
    /// This class has been developed but is not in use in the reference implementation.
    /// When integrated it should be thoroughly tested.
    /// </summary>
    public class M3HttpIntegrationService
    {
        private static readonly M3Logger logger = M3Logger.getLogger(MethodBase.GetCurrentMethod().DeclaringType.Name);

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="httpMethod"></param>
        /// <param name="url"></param>
        /// <param name="contentType"></param>
        /// <param name="requestData"></param>
        /// <param name="streamBodyMessage"></param>
        /// <param name="headerParameters"></param>
        /// <param name="queryStringParamaters"></param>
        /// <param name="deserializeJsonStringResponse"></param>
        /// <param name="deserializeXMLStringResponse"></param>
        /// <returns></returns>
        public static KeyValuePair<HttpStatusCode, T> CallService<T>(
            string httpMethod,
            string url,
            string contentType,
            string requestData,
            object streamBodyMessage,
            Dictionary<string, object> headerParameters,
            object[] queryStringParamaters = null,
            bool deserializeJsonStringResponse = false,
            bool deserializeXMLStringResponse = false) where T : new()
        {
            try
            {
                KeyValuePair<HttpStatusCode, string> responseDataKeyPair = HttpCall(httpMethod, url, contentType, streamBodyMessage, headerParameters, queryStringParamaters, requestData);
                return ReturnHttpStatusCodeAndDeserializedResponse<T>(responseDataKeyPair, deserializeJsonStringResponse, deserializeXMLStringResponse, url);
            }
            catch (Exception ex)
            {
                logger.fatal("M3WebIntegrationService.CallService", "An unhandled error has ocurred that is preventing the service to continue: " + url);
                logger.error("M3WebIntegrationService.CallService", ex.Message);

                // Handle error
                return ReturnHttpStatusCodeAndDeserializedResponse<T>(new KeyValuePair<HttpStatusCode, string>(HttpStatusCode.InternalServerError, "An unknown error has occurred."),
                                                                      deserializeJsonStringResponse, deserializeXMLStringResponse, url);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="httpMethod"></param>
        /// <param name="url"></param>
        /// <param name="contentType"></param>
        /// <param name="requestData"></param>
        /// <param name="streamBodyMessage"></param>
        /// <param name="headerParameters"></param>
        /// <param name="queryStringParamaters"></param>
        /// <param name="deserializeJsonStringResponse"></param>
        /// <param name="deserializeXMLStringResponse"></param>
        /// <returns></returns>
        public static async Task<KeyValuePair<HttpStatusCode, T>> CallServiceAsync<T>(
            string httpMethod,
            string url,
            string contentType,
            string requestData,
            object streamBodyMessage,
            Dictionary<string, object> headerParameters,
            object[] queryStringParamaters = null,
            bool deserializeJsonStringResponse = false,
            bool deserializeXMLStringResponse = false) where T : new()
        {
            try
            {
                KeyValuePair<HttpStatusCode, string> responseDataKeyPair = await HttpCallAsync(httpMethod, url, contentType, streamBodyMessage, headerParameters, queryStringParamaters, requestData);
                return ReturnHttpStatusCodeAndDeserializedResponse<T>(responseDataKeyPair, deserializeJsonStringResponse, deserializeXMLStringResponse, url);
            }
            catch (Exception ex)
            {
                logger.fatal("M3WebIntegrationService.CallServiceAsync", "An error has ocurred while calling the REST service: " + url);
                logger.error("M3WebIntegrationService.CallServiceAsync", ex.Message);

                // Handle error
                return ReturnHttpStatusCodeAndDeserializedResponse<T>(new KeyValuePair<HttpStatusCode, string>(HttpStatusCode.InternalServerError, "An unknown error has occurred."),
                                                          deserializeJsonStringResponse, deserializeXMLStringResponse, url);
            }
        }

        /// <summary>
        /// REST execution
        /// </summary>
        /// <param name="httpMethod">HTTP Method such as POST, GET, DELETE, PUT</param>
        /// <param name="url">URL of the service to be called</param>
        /// <param name="contentType">Content Type of the body message, this is used when bodyMessage is not null</param>
        /// <param name="streamBodyMessage">Optional: Any stream content that will be passed</param>
        /// <param name="headerParameters">Optional: In case a bodyMessage is specified and there is a need to send extra parameters, use headerParameters for that purpose</param>
        /// <param name="queryStringParamaters">Optional: Specify any query string that will be concatenated to the URL</param>
        /// <param name="requestData">Optional: Specify any request data, this is useful when an XML request data needs to be sent</param>
        /// <returns>Returns the HTTP Status code</returns>
        private static KeyValuePair<HttpStatusCode, string> HttpCall(
            string httpMethod,
            string url,
            string contentType,
            object streamBodyMessage = null,
            Dictionary<string, object> headerParameters = null,
            object[] queryStringParamaters = null,
            string requestData = "")
        {
            try
            {
                url = SetQueryStringParameters(url, queryStringParamaters);
                HttpWebRequest httpWebRequest = SetHttpWebRequest(url, httpMethod, contentType);
                SetHeaderParameters(httpWebRequest, headerParameters);
                SetRequestContent(httpWebRequest, contentType, requestData, streamBodyMessage);

                HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                string responseData = new StreamReader(httpWebResponse.GetResponseStream()).ReadToEnd();
                return new KeyValuePair<HttpStatusCode, string>(httpWebResponse.StatusCode, responseData);
            }
            catch (WebException webEx)
            {
                using (Stream stream = webEx.Response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    return new KeyValuePair<HttpStatusCode, string>(((HttpWebResponse)webEx.Response).StatusCode, reader.ReadToEnd());
                }
            }
            catch (Exception ex)
            {
                logger.fatal("M3WebIntegrationService.HttpCall", "Connection Failure: " + url);
                logger.error("M3WebIntegrationService.HttpCall", ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Async REST execution
        /// </summary>
        /// <param name="httpMethod">HTTP Method such as POST, GET, DELETE, PUT</param>
        /// <param name="url">URL of the service to be called</param>
        /// <param name="contentType">Content Type of the body message, this is used when bodyMessage is not null</param>
        /// <param name="streamBodyMessage">Optional: Any stream content that will be passed</param>
        /// <param name="headerParameters">Optional: In case a bodyMessage is specified and there is a need to send extra parameters, use headerParameters for that purpose</param>
        /// <param name="queryStringParamaters">Optional: Specify any query string that will be concatenated to the URL</param>
        /// <param name="requestData">Optional: Specify any request data, this is useful when an XML request data needs to be sent</param>
        /// <returns>Returns a Dictionary object that contains an HTTP Status code along with a description</returns>
        private static async Task<KeyValuePair<HttpStatusCode, string>> HttpCallAsync(
            string httpMethod,
            string url,
            string contentType,
            object streamBodyMessage = null,
            Dictionary<string, object> headerParameters = null,
            object[] queryStringParamaters = null,
            string requestData = "")
        {
            try
            {
                url = SetQueryStringParameters(url, queryStringParamaters);
                HttpWebRequest httpWebRequest = SetHttpWebRequest(url, httpMethod, contentType);
                SetHeaderParameters(httpWebRequest, headerParameters);
                SetRequestContent(httpWebRequest, contentType, requestData, streamBodyMessage);

                using (HttpWebResponse webResponse = (HttpWebResponse)await httpWebRequest.GetResponseAsync())
                {
                    HttpWebResponse httpWebResponse = (HttpWebResponse)webResponse as HttpWebResponse;
                    string responseData = new StreamReader(webResponse.GetResponseStream()).ReadToEnd();
                    return new KeyValuePair<HttpStatusCode, string>(httpWebResponse.StatusCode, responseData);
                }
            }
            catch (WebException webEx)
            {
                using (Stream stream = webEx.Response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    return new KeyValuePair<HttpStatusCode, string>(((HttpWebResponse)webEx.Response).StatusCode, reader.ReadToEnd());
                }
            }
            catch (Exception ex)
            {
                logger.fatal("M3WebIntegrationService.HttpCallAsync", "Connection Failure: " + url);
                logger.error("M3WebIntegrationService.HttpCallAsync", ex.Message);
                throw;
            }
        }

        private static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }


        private static void SetRequestContent(HttpWebRequest httpWebRequest, string contentType, string requestData, object streamBodyMessage)
        {
            if (streamBodyMessage != null && streamBodyMessage is Stream)
            {
                byte[] reqBodyBytes = ReadFully((Stream)streamBodyMessage);
                httpWebRequest.ContentLength = reqBodyBytes.Length;
                Stream reqStream = httpWebRequest.GetRequestStream();
                reqStream.Write(reqBodyBytes, 0, reqBodyBytes.Length);
                reqStream.Close();
                return;
            }

            if (!string.IsNullOrWhiteSpace(requestData))
            {
                //"application/xml; charset=utf-8";                    
                byte[] reqBodyBytes = null;
                if (contentType.Contains("xml"))
                    reqBodyBytes = Encoding.UTF8.GetBytes(requestData);
                else
                    reqBodyBytes = System.Text.Encoding.ASCII.GetBytes(requestData);
                httpWebRequest.ContentLength = reqBodyBytes.Length;
                Stream reqStream = httpWebRequest.GetRequestStream();
                reqStream.Write(reqBodyBytes, 0, reqBodyBytes.Length);
                reqStream.Close();
            }
            else
                httpWebRequest.ContentLength = 0;
        }


        private static void SetHeaderParameters(HttpWebRequest httpWebRequest, Dictionary<string, object> headerParameters)
        {
            if (headerParameters != null)
            {
                foreach (KeyValuePair<string, object> item in headerParameters)
                {
                    if (!string.IsNullOrWhiteSpace(item.Key))
                        httpWebRequest.Headers.Add(item.Key, item.Value.ToString());
                }
            }
        }


        private static HttpWebRequest SetHttpWebRequest(string url, string httpMethod, string contentType)
        {
            HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create(url);
            httpWebRequest.Method = httpMethod;
            httpWebRequest.ContentType = contentType;
            return httpWebRequest;
        }


        private static string SetQueryStringParameters(string url, object[] queryStringParamaters)
        {
            //if query string parameters is different than null then the string format received should be: http://server/api/controller?par1={0}&par2={1} 
            //where {0} will be the first parameter and {1} the second parameter and so on.
            if (queryStringParamaters != null && queryStringParamaters.Length > 0)
                url = string.Format(url, queryStringParamaters);
            return url;
        }


        private static KeyValuePair<HttpStatusCode, T> ReturnHttpStatusCodeAndDeserializedResponse<T>(
            KeyValuePair<HttpStatusCode, string> responseDataKeyPair,
            bool deserializeJsonStringResponse,
            bool deserializeXMLStringResponse,
            string url) where T : new()
        {
            if (responseDataKeyPair.Key == HttpStatusCode.OK)
            {
                if (deserializeJsonStringResponse)
                {
                    var deserializedJson = M3HttpUtils.DeserializeJson<T>(responseDataKeyPair.Value);
                    return new KeyValuePair<HttpStatusCode, T>(HttpStatusCode.OK, deserializedJson);
                }
                else if (deserializeXMLStringResponse)
                {
                    //This code is used whent the REST call returns an XML string and needs to be deserialized into a type T
                    var deserializedXML = M3HttpUtils.DeserializeXML<T>(responseDataKeyPair.Value);
                    return new KeyValuePair<HttpStatusCode, T>(HttpStatusCode.OK, deserializedXML);
                }
                else
                {
                    //This code is used when the REST call return a string that can be changed to a specified type
                    return new KeyValuePair<HttpStatusCode, T>(HttpStatusCode.OK, (T)Convert.ChangeType(responseDataKeyPair.Value, typeof(T)));
                }
            }
            else
            {
                logger.fatal("M3WebIntegrationService.ReturnHttpStatusCodeAndDeserializedResponse", "An error has ocurred while calling the REST service: " + url);
                return new KeyValuePair<HttpStatusCode, T>(responseDataKeyPair.Key, default(T));
            }
        }
    }
}
