﻿using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Migrations.History; 

namespace Collaborative.M3.ServerSdk.Core.Integration.EF
{
    public class M3HistoryContext : HistoryContext
    {
        private readonly string _schemaName = string.Empty;

        public M3HistoryContext(DbConnection dbConnection, string defaultSchema)
            : base(dbConnection, defaultSchema)
        {
            _schemaName = defaultSchema;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<HistoryRow>().ToTable(tableName: "M3_MIGRATION_HISTORY", schemaName: _schemaName);
            modelBuilder.Entity<HistoryRow>().Property(p => p.MigrationId).HasColumnName("MIGRATION_ID");
            modelBuilder.Entity<HistoryRow>().Property(p => p.ContextKey).HasColumnName("CONTEXT_KEY");
            modelBuilder.Entity<HistoryRow>().Property(p => p.Model).HasColumnName("MODEL");
            modelBuilder.Entity<HistoryRow>().Property(p => p.ProductVersion).HasColumnName("PRODUCT_VERSION");
        }
    }
}
