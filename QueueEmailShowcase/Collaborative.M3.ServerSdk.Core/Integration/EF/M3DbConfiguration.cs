﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Integration.EF
{
    public class M3DbConfiguration : DbConfiguration
    {
        public M3DbConfiguration()
        {
            //Uncomment for Oracle Provider
            //this.SetHistoryContext("Oracle.ManagedDataAccess.Client",
            //    (connection, defaultSchema) => new M3HistoryContext(connection, defaultSchema));

            this.SetHistoryContext("System.Data.SqlClient",
                (connection, defaultSchema) => new M3HistoryContext(connection, defaultSchema));
        }
    } 
}
