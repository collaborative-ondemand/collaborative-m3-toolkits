﻿using Collaborative.M3.ServerSdk.Core.Common.Domain;
using Collaborative.M3.ServerSdk.Core.Identity.Domain.EF;
using Collaborative.M3.ServerSdk.Core.Integration.EF.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Integration.EF.Implementation
{
    /// <summary>
    /// Entity Framework repository
    /// </summary>
    public partial class EFRepository<TEntity> : IRepository<TEntity>
        where TEntity : BaseEntity
    {
        private readonly IDbContext _context;
        protected IDbSet<TEntity> _entities;

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="context">Object context</param>
        public EFRepository(IDbContext context)
        {
            this._context = context;
        }

        public async Task<IList<TEntity>> GetAll(int page = 0, int pageSize = Int32.MaxValue, bool active = true)
        {
            try
            {
                var query = this.Table;

                query = query
                    .OrderBy(entity => entity.Id)
                    .Skip(page)
                    .Take(pageSize);

                return await query.ToListAsync();
            }
            catch (AggregateException ag)
            {
                throw;
            }
        }

        public async Task<TEntity> GetById(int id, bool active = true)
        {
            try
            {
                return await this.Table
                            .Where(entity => entity.Id == id) /*&& entity.Active == 1)*/
                            .FirstOrDefaultAsync();
            }
            catch (AggregateException ag)
            {
                throw;
            }
            
        }

        public async Task Insert(TEntity entity, bool autoCommit = false)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");
                
                this.Entities.Add(entity);

                if (autoCommit)
                    await Commit();
            }
            catch(AggregateException ag)
            {

            }
        }

        public async Task Update(TEntity entity, bool autoCommit = false)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");

                if (autoCommit)
                    await Commit();
            }
            catch(AggregateException ag)
            {

            }
        }

        public async Task Delete(TEntity entity, bool autoCommit = false)
        {
            try
            {
                if (entity == null)
                    throw new ArgumentNullException("entity");

                this.Entities.Remove(entity);

                if (autoCommit)
                {
                    await Commit();
                }
            }
            catch(AggregateException ag)
            {

            }
        }

        public IQueryable<TEntity> Table
        {
            get
            {
                return this.Entities;
            }
        }

        public async Task Commit()
        {
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbEntityValidationException dbEx)
            {
                var msg = string.Empty;

                foreach (var validationErrors in dbEx.EntityValidationErrors)
                    foreach (var validationError in validationErrors.ValidationErrors)
                        msg += Environment.NewLine + string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);

                var fail = new Exception(msg, dbEx);
                //Debug.WriteLine(fail.Message, fail);
                throw fail;
            }
            catch (AggregateException ag)
            {
                throw;
            }
        }

        private IDbSet<TEntity> Entities
        {
            get
            {
                if (_entities == null)
                    _entities = _context.Set<TEntity>();
                return _entities;
            }
        }
    }
}