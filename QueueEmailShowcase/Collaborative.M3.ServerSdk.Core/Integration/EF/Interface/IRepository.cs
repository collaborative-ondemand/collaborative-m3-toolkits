﻿using Collaborative.M3.ServerSdk.Core.Common.Domain;
using Collaborative.M3.ServerSdk.Core.Identity.Domain.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Integration.EF.Interface
{
    public interface IRepository<TEntity>
        where TEntity : BaseEntity
    {
        Task<IList<TEntity>> GetAll(int page = 0, int pageSize = Int32.MaxValue, bool active = true);
        Task<TEntity> GetById(int id, bool active = true);
        Task Insert(TEntity entity, bool autoCommit = false);
        Task Update(TEntity entity, bool autoCommit = false);
        Task Delete(TEntity entity, bool autoCommit = false);
        IQueryable<TEntity> Table { get; }
        Task Commit();
    }
}
