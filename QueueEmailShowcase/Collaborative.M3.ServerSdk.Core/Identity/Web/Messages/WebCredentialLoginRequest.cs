﻿using Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage.Base;
using Collaborative.M3.ServerSdk.Identity.Domain;

namespace Collaborative.M3.ServerSdk.Identity.Web.Messages
{
    public class WebCredentialLoginRequest : M3RequestMessageBase<UserCredentialLogin>
    {
        public override UserCredentialLogin body { get; set; }
    }

}