﻿using Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage.Base;
using Collaborative.M3.ServerSdk.Identity.Domain;

namespace Collaborative.M3.ServerSdk.Identity.Web.Messages
{
    public class WebRegistrationInfoRequest : M3RequestMessageBase<UserRegistrationInfo>
    {
        public override UserRegistrationInfo body { get; set; }
    }
}