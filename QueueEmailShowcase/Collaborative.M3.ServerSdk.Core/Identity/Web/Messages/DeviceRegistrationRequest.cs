﻿using Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage.Base;
using Collaborative.M3.ServerSdk.Identity.Domain;

namespace Collaborative.M3.ServerSdk.Identity.Web.Messages
{
    /// <summary>
    /// Contains the device specific parameters required to register the device with the app on behalf of the authenticated user.
    /// </summary>
    public class DeviceRegistrationRequest : M3RequestMessageBase<DeviceRegistrationInfo>
    {
        public override DeviceRegistrationInfo body { get; set; }
    }
}