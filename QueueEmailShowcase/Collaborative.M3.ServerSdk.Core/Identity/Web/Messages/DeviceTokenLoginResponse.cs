﻿using Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage.Base;
using Collaborative.M3.ServerSdk.Identity.Domain;

namespace Collaborative.M3.ServerSdk.Identity.Web.Messages
{
    public class DeviceTokenLoginResponse : M3ResponseMessageBase<DeviceTokenInfo>
    {
        public override DeviceTokenInfo body { get; set; }
    }
}