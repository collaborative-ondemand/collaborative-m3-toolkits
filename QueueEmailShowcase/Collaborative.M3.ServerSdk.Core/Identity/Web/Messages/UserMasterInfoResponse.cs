﻿using Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage.Base;
using Collaborative.M3.ServerSdk.Identity.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Identity.Web.Messages
{
    public class UserMasterInfoResponse : M3ResponseMessageBase<UserMasterInfo>
    {
        public override UserMasterInfo body { get; set; }
    }
}
