﻿using Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage.Base;
using Collaborative.M3.ServerSdk.Identity.Domain;

namespace Collaborative.M3.ServerSdk.Identity.Web.Messages
{
    public class DeviceTokenLoginRequest : M3RequestMessageBase<DeviceTokenInfo>
    {
        public override DeviceTokenInfo body { get; set; }
    }
}