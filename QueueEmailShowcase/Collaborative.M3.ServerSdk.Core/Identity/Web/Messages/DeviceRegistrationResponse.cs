﻿using Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage.Base;
using Collaborative.M3.ServerSdk.Identity.Domain;

namespace Collaborative.M3.ServerSdk.Identity.Web.Messages
{
    /// <summary>
    /// Contains the unique token representing the registered device to the application.
    /// </summary>
    public class DeviceRegistrationResponse : M3ResponseMessageBase<DeviceTokenInfo>
    {
        public override DeviceTokenInfo body { get; set; }

    }
}