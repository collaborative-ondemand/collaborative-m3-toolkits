﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Collaborative.M3.ServerSdk.Identity.Common.Utils
{
    /// <summary>
    /// Provides a representation of a boolean value that can be toggled in a thread-safe, atomic manner.
    /// </summary>
    public class M3ThreadSafeBoolean
    {
        /// <summary>
        /// Internal representation of the boolean value true.
        /// </summary>
        private const int BOOL_TRUE = 1;

        /// <summary>
        /// Internal representation of the boolean value false.
        /// </summary>
        private const int BOOL_FALSE = 0;

        /// <summary>
        /// Internal representation of the current value as either BOOL_TRUE or BOOL_FALSE.
        /// </summary>
        private int _currentBoolState = 0;

        /// <summary>
        /// Returns the boolean value representing the current state of this instance.
        /// </summary>
        public bool Value{
            get {
                // get the current value, converts to a bool and returns
                bool currentBoolValue = _currentBoolState == BOOL_TRUE;

                return currentBoolValue;
            }
        }

        /// <summary>
        /// Attempts to set the value of the thread safe boolean to True while returning the original value.
        /// </summary>
        /// <returns></returns>
        public bool ToggleTrue()
        {
            // perform the Interlocked update
            int originalState = Interlocked.Exchange(ref _currentBoolState, BOOL_TRUE);

            // convert original value to a bool
            // grab the orig value
            bool originalValue = originalState == BOOL_TRUE;

            return originalValue;
        }

        /// <summary>
        /// Attempts to set the value of the thread safe boolean to False while returning the original value.
        /// </summary>
        /// <returns></returns>
        public bool ToggleFalse()
        {
            // perform the Interlocked update
            int originalState = Interlocked.Exchange(ref _currentBoolState, BOOL_FALSE);

            // convert original value to a bool
            // grab the orig value
            bool originalValue = originalState == BOOL_TRUE;

            return originalValue;
        }

    }
}
