﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Identity.Common.Util
{
    /// <summary>
    /// M3 Implementation of the password hasher.
    /// </summary>
    public class M3PasswordHasher
    {
        private static PasswordHasher hasher = new PasswordHasher();

        public static string HashPassword(string password)
        {
            return hasher.HashPassword(password);

        }

        public static bool VerifyHashedPassword(string hashedPassword, string providedPassword)
        {

            PasswordVerificationResult verificationResult = hasher.VerifyHashedPassword(hashedPassword, providedPassword);

            bool verified = false;

            if ((verificationResult == PasswordVerificationResult.Success) || (verificationResult == PasswordVerificationResult.SuccessRehashNeeded))
            {
                verified = true;
            } else {
                verified = false;
            }

            return verified;

        }
    }
}
