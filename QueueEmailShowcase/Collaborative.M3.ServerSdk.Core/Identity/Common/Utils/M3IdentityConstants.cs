﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Identity.Common.Utils
{
    public class M3IdentityConstants
    {
        /// <summary>
        /// Constant for password match failure code.
        /// </summary>
        public const string FAIL_CODE_PASSWORD_MATCH = "M3FailPasswordMissmatch";


        /// <summary>
        /// Constant for user id already exists.
        /// </summary>
        public const string FAIL_CODE_USERNAME_EXISTS = "M3FailUserNameExists";


        /// <summary>
        /// Constant for failed credential match.
        /// </summary>
        public const string FAIL_CODE_INVALID_CREDENTIAL = "M3FailInvalidCredentials";


        /// <summary>
        /// Constant for username not found.
        /// </summary>
        public const string FAIL_CODE_USERNAME_NOT_FOUND = "M3FailUserNameNotFound";


        /// <summary>
        /// Claim type name for the unique system identifier in the M3 Identity space
        /// </summary>
        public const string CLAIM_TYPE_M3ID = "http://m3.collaborative.com/m3/identity/claims/m3Id";

        /// <summary>
        /// Claim type name for the unique system identifier in the Org space
        /// </summary>        
        public const string CLAIM_TYPE_CONTEXTID = "http://m3.collaborative.com/m3/identity/claims/contextId";

        public const string CLAIM_TYPE_DYNAMIC = "http://m3.collaborative.com/m3/identity/claims/dynamic/";

        public const string CLAIM_DELIMITER = "|";
    }
}
