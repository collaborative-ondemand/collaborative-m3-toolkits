﻿using Collaborative.M3.ServerSdk.Identity.Common.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Collaborative.M3.ServerSdk.Core.Identity.Common.Authorization
{
    public class M3CustomAuthorizeRolesAttribute : AuthorizeAttribute
    {
        public M3CustomAuthorizeRolesAttribute(params string[] roles)
            : base()
        {
            Roles = string.Join(",", roles);
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            // All is setup now each role the user has
            M3User m3User = (M3User)Thread.CurrentPrincipal;

            foreach (string role in Roles.Split(','))
            {
                if (m3User.RoleList.Contains(role))
                {
                    return true;
                }
            }

            //No role found, not authorized
            return false;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(new
            RouteValueDictionary(new { controller = "Error", action = "NotAuthorized" }));
        }
    }
}
