﻿using Collaborative.M3.ServerSdk.Identity.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Collaborative.M3.ServerSdk.Identity.Common.Utils;
using Collaborative.M3.ServerSdk.Identity.Common.User;
using Collaborative.M3.ServerSdk.Identity.Integration;
using Collaborative.M3.ServerSdk.Core.Business.Common.Messaging;

namespace Collaborative.M3.ServerSdk.Identity.Common.Authorization
{
    public class M3AuthorizationManager
    {


        /// <summary>
        /// Holds the collection of roles and their associated entitlements.
        /// </summary>
        private static Dictionary<string, Role> _roleMap = new Dictionary<string, Role>();

        /// <summary>
        /// Flag indicating if the manager is initialized. 
        /// </summary>
        private static M3ThreadSafeBoolean _isInitialized = new M3ThreadSafeBoolean();

        /// <summary>
        /// Flag indicating if a refresh is occuring.
        /// </summary>
        private static M3ThreadSafeBoolean _refreshInProgress = new M3ThreadSafeBoolean();

        /// <summary>
        /// Used to manage thread safety while a refresh is in progress
        /// </summary>
        private static ReaderWriterLockSlim _refreshInProgressLock = new ReaderWriterLockSlim();

        /// <summary>
        /// Semaphore used to manage the initialization of the Authorization manager.
        /// </summary>
        private static readonly object _initLocker = new object();

        /// <summary>
        /// Holds the time that the role map was last refreshed.
        /// </summary>
        private static DateTime _lastRefresh = DateTime.Now;

        /// <summary>
        /// Time to live of cache data before a refresh is triggered.
        /// </summary>
        private static long _cacheTimeToLive = 0;

        /// <summary>
        /// Default TTL
        /// </summary>
        private const long DEFAULT_TIME_TO_LIVE = 60000; // 1 minute



        /// <summary>
        /// Checks the entitlements associated with all of the roles assigned to the current authenticated M3User.
        /// </summary>
        /// <param name="targetEntitlement"></param>
        /// <returns></returns>
        public static bool IsUserEntitled(string targetEntitlement)
        {
            // call init
            M3AuthorizationManager.initAuthorizationManager();

            // check for refresh required
            M3AuthorizationManager.triggerRefreshIfRequired() ;

            // All is setup now check the entitlement for each role the user has
            M3User m3User = (M3User)Thread.CurrentPrincipal;

            // declare a role to hold the interim value
            Role currentRole = null;

            // loop through each role the user is a member and interogate the associated entitlements collection until the target 
            // entitlement is found or we run out of roles.
            foreach (string roleName in m3User.RoleList)
            {
                // check the entitlement list for the requested entitlement for each role
                // wrap this call in a read lock
                M3AuthorizationManager._refreshInProgressLock.EnterReadLock();

                // access the current role from the _roleMap
                currentRole = _roleMap[roleName];

                // exit the read lock
                M3AuthorizationManager._refreshInProgressLock.ExitReadLock();

                if (currentRole != null)
                {
                    // check for the entitlement
                    if (currentRole.Entitlements.ContainsKey(targetEntitlement))
                    {
                        // found the targetEntitlement in this role
                        return true;
                    }
                }

            }

            // if we got here then there was no match
            return false;
        }

        /// <summary>
        /// Administrative API to force a refresh of the role/entitlement matrix.  
        /// </summary>
        public static void ForceRefresh()
        {
            M3AuthorizationManager.refreshAuthorizationData();
        }

        /// <summary>
        /// Initialization method gets called with every invocation but only executes fully once.  It performs the initialization of 
        /// this manager in a thread-safe manner and handles the stampede effect for the initial execution. Additional requestors will block until the
        /// first initialization completes.
        /// </summary>
        private static void initAuthorizationManager() 
        {
            // perform a non-thread safe check first.  if it is true then someone else got there first
            // if it is true then attempt a thread safe check
            if (M3AuthorizationManager._isInitialized.Value == false)
            {
                // only one thread can execute an initiation so this needs to be thread safe with a lock
                // this will cause all other threads to wait to get in and once the first thread completes, the initialization flag will be true
                // causing them to move on without trying to re-initialize
                lock (M3AuthorizationManager._initLocker)
                {

                    // perform a thread-safe check attempting to set the init flag to true and conduct init if it was previously false
                    if (M3AuthorizationManager._isInitialized.Value == false)
                    {
                        // force a refresh
                        M3AuthorizationManager.refreshAuthorizationData();

                        // establish runtime configuration
                        // TODO:  make this dynamic.  for now use the default
                        M3AuthorizationManager._cacheTimeToLive = M3AuthorizationManager.DEFAULT_TIME_TO_LIVE;

                        // once all config is complete, flip the init flag so no other inits occur
                        M3AuthorizationManager._isInitialized.ToggleTrue();

                    }
                    else
                    {
                        // todo: log debug that initialization must already be in progress
                    }
                } // end of lock block
            }
        }

        /// <summary>
        /// Checks to see if a refresh is required.  If a refresh is required then the method will trigger it.
        /// </summary>
        /// <returns></returns>
        private static void triggerRefreshIfRequired()
        {
            // determine interval since last refresh
            TimeSpan spanSinceRefresh = DateTime.Now.Subtract(M3AuthorizationManager._lastRefresh);

            // check it
            if (spanSinceRefresh.Milliseconds > M3AuthorizationManager._cacheTimeToLive)
            {
                // time exceeded so check to see if there is a refresh in progress
                if (M3AuthorizationManager._refreshInProgress.Value == false)
                {
                    // refresh required and no one is currently refreshing so do a refresh
                    M3AuthorizationManager.refreshAuthorizationData();
                }
            }
        }


        /// <summary>
        /// Loads the role entitlement matrix and parses into the appropriate structure replacing the existing role information in this component in 
        /// a thread-safe manner.  This refresh handles the stampede effect so that only one execution occurs per refresh cycle.  Additional requestors will 
        /// access stale data until refresh completes.
        /// </summary>
        private static void refreshAuthorizationData()
        {
            // try to set the in progress flag in a thread safe manner
            if (M3AuthorizationManager._refreshInProgress.ToggleTrue() == false)
            {
                // declare an array of these to hold the result
                RoleEntitlementItem[] roleEntitlementItems = M3AuthorizationDataIntegrationService.GetAllEntitlementsAllRoles();


                if (roleEntitlementItems == null)
                {
                    // TODO: log the issus

                    // fail safe by revoking all entitlements
                    roleEntitlementItems = new RoleEntitlementItem[0];

                }
                else
                {
                    
                }

                // process the roles
                Dictionary<string,Role> newRoleDict = M3AuthorizationManager.convertRoleEntitlemntItemsToRoleDictionary(roleEntitlementItems);

                // save the new dictionary
                // grab a WriteLock
                M3AuthorizationManager._refreshInProgressLock.EnterWriteLock();

                // update the role map
                M3AuthorizationManager._roleMap = newRoleDict;

                // exit the lock
                M3AuthorizationManager._refreshInProgressLock.ExitWriteLock();

                // update the timestamp
                M3AuthorizationManager._lastRefresh = DateTime.Now;

                // update the in progress flag
                M3AuthorizationManager._refreshInProgress.ToggleFalse();

            }

            

        }

        private static Dictionary<string,Role> convertRoleEntitlemntItemsToRoleDictionary(RoleEntitlementItem[] roleEntitlementItems)
        {
            Dictionary<string,Role> roleList = new Dictionary<string,Role>();
            Role currentRole = null;
            EntitlementInfo entitlementInfo = null;
            string currentRoleKey = "";
            
            // ensure there is at least 1 item
            if ((roleEntitlementItems == null) || (roleEntitlementItems.Length == 0)) 
            {
                // no items so return an empty dictionary
                return roleList;
            }

            // loop thorugh the input array and create a Role object then add to the dictionary
            // TODO:  

            foreach (RoleEntitlementItem roleEntitlement in roleEntitlementItems)
            {
                // check for a change in the role key
                if (roleEntitlement.RoleKey.Equals(currentRoleKey,StringComparison.OrdinalIgnoreCase) == false)
                {
                    // this is a new role
                
                    // init a new role
                    currentRole = new Role();
                    currentRole.RoleId = roleEntitlement.RoleId;
                    currentRole.RoleKey = roleEntitlement.RoleKey;
                    currentRole.RoleDisplayName = roleEntitlement.RoleDisplayName;
                    currentRole.RoleDescription = roleEntitlement.RoleDescription;
                    
                    // set the new current role key
                    currentRoleKey = roleEntitlement.RoleKey;
                    // add the role to the role list
                    roleList.Add(currentRole.RoleKey, currentRole);
                }
 
                // build the entitlement object 
                entitlementInfo = new EntitlementInfo();

                // load it
                entitlementInfo.EntitlementId = roleEntitlement.EntitlementId;
                entitlementInfo.EntitlementKey = roleEntitlement.EntitlementKey;
                entitlementInfo.EntitlementDisplayName = roleEntitlement.EntitlementDisplayName;
                entitlementInfo.EntiltementDescription = roleEntitlement.EntiltementDescription;

                // add it
                currentRole.Entitlements.Add(entitlementInfo.EntitlementKey,entitlementInfo);

            }


            return roleList ;
        }

    }
}
