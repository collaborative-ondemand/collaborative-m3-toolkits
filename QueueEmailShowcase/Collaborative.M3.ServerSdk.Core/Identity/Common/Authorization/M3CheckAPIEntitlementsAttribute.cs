﻿using Collaborative.M3.ServerSdk.Identity.Common.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Routing;

namespace Collaborative.M3.ServerSdk.Identity.Common.Authorization
{
    /// <summary>
    /// Verifies the user has the requested entitlement to access an Web API, service style controller.
    /// </summary>
    public class M3CheckAPIEntitlementsAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// Property indicating the specific entitlement being requested.
        /// </summary>
        public string Entitlement { get; set; }

        /// <summary>
        /// Default constructor used when no entitlements are being checked.  The result is an authentication check only.
        /// </summary>
        public M3CheckAPIEntitlementsAttribute()
        {
            Entitlement = "";
        }

        /// <summary>
        /// Constructor that takes a single string entitlement to check.
        /// </summary>
        /// <param name="entitlementToCheck"></param>
        public M3CheckAPIEntitlementsAttribute(string entitlementToCheck)
        {
            Entitlement = entitlementToCheck;
        }

        
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            M3User currentUser = M3User.GetM3UserFromCurrentContext();

            // check login and authorization
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                
                // set the response to be Http Unauthorized
                //actionContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);

                // nothing else to do so return from here
                return false;
            }

            // that worked so now see if we have any entitlements to check
            if (this.Entitlement.Length == 0)
            {
                // no entitlements to check and already passed base authz so return true
                return true;
            }
            else
            {
                // got here so must need to check entitlements...do so
                bool hasEntitlement = M3AuthorizationManager.IsUserEntitled(Entitlement);

                // return the result of the check
                if (hasEntitlement)
                {
                    // all is well so just return
                    return true;
                }
                else
                {
                    // failed entitlement check so....
                    // set the response to be Http Forbidden
                    //actionContext.Response = new HttpResponseMessage(HttpStatusCode.Forbidden);
                    return false;

                }

            }

        }

         
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            M3User currentUser = M3User.GetM3UserFromCurrentContext();

            // check login and authorization
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                
                // set the response to be Http Unauthorized
                actionContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);

                // nothing else to do so return from here
                return;
            }

            // that worked so now see if we have any entitlements to check
            if (this.Entitlement.Length == 0)
            {
                // no entitlements to check and already passed base authz so return true
                return;
            }
            else
            {
                // got here so must need to check entitlements...do so
                bool hasEntitlement = M3AuthorizationManager.IsUserEntitled(Entitlement);

                // return the result of the check
                if (hasEntitlement)
                {
                    // all is well so just return
                    return;
                }
                else
                {
                    // failed entitlement check so....
                    // set the response to be Http Forbidden
                    actionContext.Response = new HttpResponseMessage(HttpStatusCode.Forbidden);

                }

            }
        }
        
        /// <summary>
        /// Override of base behavior to redirect to /Error/AccessDenied controller and action for AuthZ failures.  AuthN failures still go to the login page.
        /// </summary>
        /// <param name="actionContext"></param>
        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {

            // return a 403
            HttpContext.Current.Response.Status = "403 Forbidden";
            //the next line is untested - thanks to strider for this line
            HttpContext.Current.Response.StatusCode = 403;
            HttpContext.Current.Response.End();


            // check to see if the user is authenticated.
            // If the user is not authenticated then use standard behavior to send to the login page
            // if not, then send to the custom access denied page
            
            /*
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                // delegate to base
                //base.HandleUnauthorizedRequest(filterContext);
                HttpContext.Current.Response.Status = "403 Forbidden";
                //the next line is untested - thanks to strider for this line
                HttpContext.Current.Response.StatusCode = 403;
                HttpContext.Current.Response.End();
            }
            else
            {
                // send to custom error page.
                //filterContext.Result = new RedirectToRouteResult(
                //    new RouteValueDictionary(new { controller = "Error", action = "AccessDenied" }));
                HttpContext.Current.Response.Status = "403 Forbidden";
                //the next line is untested - thanks to strider for this line
                HttpContext.Current.Response.StatusCode = 403;
                HttpContext.Current.Response.End();

            }
            */
        }
    }
}
