﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Collaborative.M3.ServerSdk.Identity.Common.Authorization
{
    /// <summary>
    /// Verifies the user has the requested entitlement to access an MVC or Web style controller.
    /// </summary>
    public class M3CheckEntitlementsAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// Property indicating the specific entitlement being requested.
        /// </summary>
        public string Entitlement { get; set; }

        /// <summary>
        /// Default constructor used when no entitlements are being checked.  The result is an authentication check only.
        /// </summary>
        public M3CheckEntitlementsAttribute()
        {
            Entitlement = "";
        }

        /// <summary>
        /// Constructor that takes a single string entitlement to check.
        /// </summary>
        /// <param name="entitlementToCheck"></param>
        public M3CheckEntitlementsAttribute(string entitlementToCheck)
        {
            Entitlement = entitlementToCheck;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            /*
             * Not sure if I need this call to base?????
            var isAuthorized = base.AuthorizeCore(httpContext);
            if (!isAuthorized)
            {
                return false;
            }
            */
            
            // call the base to allow normal authorization processing
            bool isAuthorized = base.AuthorizeCore(httpContext);

            // if this fails then return false
            if (!isAuthorized)
            {
                return false;
            }

            // that worked so now see if we have any entitlements to check
            if (this.Entitlement.Length == 0)
            {
                // no entitlements to check and already passed base authz so return true
                return true;
            }

            // got here so must need to check entitlements...do so
            bool hasEntitlement = M3AuthorizationManager.IsUserEntitled(Entitlement);

            // return the check result
            return hasEntitlement;
        }

        /// <summary>
        /// Override of base behavior to redirect to /Error/NotAuthorized controller and action for AuthZ failures.  AuthN failures still go to the login page.
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            // check to see if the user is authenticated.
            // If the user is not authenticated then use standard behavior to send to the login page
            // if not, then send to the custom access denied page
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                // delegate to base
                base.HandleUnauthorizedRequest(filterContext);
            }
            else
            {
                // send to custom error page.
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary(new { controller = "Error", action = "NotAuthorized" }));
            }
            
        }
    }
}
