﻿using Collaborative.M3.ServerSdk.Core.Business.Common.Messaging;
using Collaborative.M3.ServerSdk.Identity.Domain;
using Collaborative.M3.ServerSdk.Identity.Business;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Collaborative.M3.ServerSdk.Core.Business.Common.Exception;

namespace Collaborative.M3.ServerSdk.Identity.Common.OAuth
{
    public class M3OAuthLocalAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        /// <summary>
        /// This method is responsible for validating the client is a valid client.  Essentially this method
        /// ensures the device tag is valid.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            string clientId;
            string clientSecret;

            //  This method is responsible for validating the client is a valid client.  
            if (context.TryGetBasicCredentials(out clientId, out clientSecret) || context.TryGetFormCredentials(out clientId, out clientSecret))
            {
                try{

                    // Verify client
                    DeviceTokenInfo deviceTokenInfo = new DeviceTokenInfo();
                    deviceTokenInfo.DeviceAppRegistrationTag = clientId;
                    deviceTokenInfo.RefreshToken = clientSecret;
                    M3IdentityService.AuthenticateOAuthRefreshToken(deviceTokenInfo);
                    
                    // need to make the client_id available for later security checks
                    context.OwinContext.Set<string>("as:client_id", clientId);
                    context.Validated();
                } 
                catch (M3Exception e)
                {
                    context.Rejected();
                }
            }
        }

        /// <summary>
        /// validates resource owner credentials and issues the refresh and access token
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            // create a web credential login
            UserCredentialLogin webCredentialLogin = new UserCredentialLogin();

            // populate it
            webCredentialLogin.UserName = context.UserName;
            webCredentialLogin.Password = context.Password;

            // use the identity manager to get a claims identity
            ClaimsIdentity claimsIdentityResult = M3IdentityService.GetAuthenticatedLocalUserClaimsIdentity(webCredentialLogin);

            // if failed then set error and return
            if (claimsIdentityResult == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");
                return;
            }


            // all is well so extract the claims identity
            ClaimsIdentity claimsIdentity = claimsIdentityResult;
            
            // add in the client verification
            AuthenticationProperties authNProps = new AuthenticationProperties(new Dictionary<string, string>
            {
                { "as:client_id", context.ClientId }
            });

            // create an authN ticket
            AuthenticationTicket authNTicket = new AuthenticationTicket(claimsIdentity, authNProps);

            // call Validated
            context.Validated(authNTicket);

        }

        public override async Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            {
                string originalClient = context.Ticket.Properties.Dictionary["as:client_id"];
                string currentClient = context.OwinContext.Get<string>("as:client_id");
 
                // enforce client binding of refresh token
                if (originalClient != currentClient)
                {
                    context.Rejected();
                    return;
                }
 
                // chance to change authentication ticket for refresh token requests
                var newId = new ClaimsIdentity(context.Ticket.Identity);
                newId.AddClaim(new Claim("newClaim", "refreshToken"));
 
                var newTicket = new AuthenticationTicket(newId, context.Ticket.Properties);
                context.Validated(newTicket);
            }
        }
    }
}