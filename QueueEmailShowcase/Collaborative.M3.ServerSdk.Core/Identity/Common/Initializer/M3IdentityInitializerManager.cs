﻿using Collaborative.M3.ServerSdk.Core.Business.Common.Container;
using Collaborative.M3.ServerSdk.Core.Config;
using Collaborative.M3.ServerSdk.Core.Config.Section;
using Collaborative.M3.ServerSdk.Core.Identity.Integration.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Identity.Common.Initializer
{
    public static class M3IdentityInitializerManager
    {
        public static IM3AuthorizationDataIntegrationService GetAuthorizationDataIntegrationService()
        {
            M3IdentityConfig identityConfig = M3Config.ActiveConfig.M3IdentityConfig;
            switch (identityConfig.DataAccessType.ToLower().Trim())
            {
                case "ef" :
                        return RetrieveInstance<Collaborative.M3.ServerSdk.Core.Identity.Integration.Implementation.EF.M3AuthorizationDataIntegrationService>("Authorization");
                case "ado" :
                        return RetrieveInstance<Collaborative.M3.ServerSdk.Core.Identity.Integration.Implementation.ADO.M3AuthorizationDataIntegrationService>("Authorization");
                default:
                    throw new Exception("Data Access Type not found");
            }
        }

        public static IM3UserDataIntegrationService GetUserDataIntegrationService()
        {
            M3IdentityConfig identityConfig = M3Config.ActiveConfig.M3IdentityConfig;
            switch (identityConfig.DataAccessType.ToLower().Trim())
            {
                case "ef":
                    return RetrieveInstance<Collaborative.M3.ServerSdk.Core.Identity.Integration.Implementation.EF.M3UserDataIntegrationService>("Authentication");
                case "ado":
                    return RetrieveInstance<Collaborative.M3.ServerSdk.Core.Identity.Integration.Implementation.ADO.M3UserDataIntegrationService>("Authentication");
                default:
                    throw new Exception("Data Access Type not found");
            }
        }

        private static TClass RetrieveInstance<TClass>(string initializerType)
            where TClass : class
        {
            if (!M3Container.HasRegisteredType<TClass>())
                Initialize(initializerType);

            return M3Container.GetInstance<TClass>();
        }

        private static void Initialize(string type)
        {
            if (type == "Authorization")
                M3Container.RegisterAllConcreteImplementations<IM3AuthorizationDataIntegrationService>();

            if (type == "Authentication")
                M3Container.RegisterAllConcreteImplementations<IM3UserDataIntegrationService>();
        }
    }
}
