﻿using Collaborative.M3.ServerSdk.Core.Utils;
using Collaborative.M3.ServerSdk.Identity.Business;
using Collaborative.M3.ServerSdk.Identity.Common;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Collaborative.M3.ServerSdk.Identity.Common.Utils;
using Collaborative.M3.ServerSdk.Identity.Common.Authorization;
using System.Threading;
using System.Dynamic;

namespace Collaborative.M3.ServerSdk.Identity.Common.User
{
    public class M3User : IPrincipal
    {

        /// <summary>
        /// list of roles.
        /// </summary>
        private string[] Roles = new string[0];

        /// <summary>
        /// Represents the unique local system identifier for the user.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Represents the unique local system identifier for the user's context (e.g. - Org, Entity, Legal Authority, etc...).
        /// </summary>
        public string ContextId { get; set; }

        /// <summary>
        /// UserName property delegates to the underlying Identity object.
        /// </summary>
        public string UserName {
            get
            {
                return this.Identity.Name;
            }
          
        }

        /// <summary>
        /// Provides a list of roles assigned to the user.
        /// </summary>
        public IEnumerable<string> RoleList 
        { 
            get 
            {   
                return this.Roles.AsEnumerable<string>();
            } 
        }

        /// <summary>
        /// Provides a list of claims for the user.
        /// </summary>
        public IEnumerable<Claim> Claims
        {
            get
            {
                ClaimsIdentity claimsIdentity = (ClaimsIdentity)HttpContext.Current.User.Identity;
                return claimsIdentity.Claims;
            }
        }

        /// <summary>
        /// Dynamic object following ViewBag concept, it adds properties in runtime
        /// </summary>
        public dynamic AdditionalProperties { get; set; }

        public IIdentity Identity
        {
            get;
            private set;
        }




        /// <summary>
        /// Private constructor initializes an empty version of the M3User.
        /// </summary>
        private M3User()
        {
            this.Identity = new GenericIdentity("");
            this.Id = "";
            this.ContextId = "";
            this.Roles = new string[] { };
        }
        
        /// <summary>
        /// Private constructor that initializes a version of M3User and the underlying identity implementation.
        /// </summary>
        /// <param name="identityName"></param>
        private M3User(string identityName)
        {
            this.Identity = new GenericIdentity(identityName);
            this.Id = "";
            this.ContextId = "";
            this.Roles = new string[] { };
        }       
        /// <summary>
        /// Checks to see if the current authenticated user is a member of the requested role.
        /// </summary>
        /// <param name="requestedRole"></param>
        /// <returns></returns>
        public bool IsInRole(string requestedRole)
        {
            bool hasRole = Roles.Contains<string>(requestedRole);

            return hasRole;
        }


        /// <summary>
        /// Checks to see if the current authenticated user has the entitlement across any of their assigned roles.  This method is generally preferred to the IsInRole() method 
        /// since it provides greater flexibility and more fine grained control.
        /// </summary>
        /// <param name="requestedEntitlement"></param>
        /// <returns></returns>
        public bool IsEntitled(string requestedEntitlement)
        {
            return M3AuthorizationManager.IsUserEntitled(requestedEntitlement);
        }

        public object GetClaim(string key)
        {
            foreach (Claim claim in Claims)
            {
                if (claim.Type.Equals(key))
                    return claim.Value;
            }
            return null;
        }

        public bool HasClaim(string key, string value)
        {
            foreach (Claim claim in Claims)
            {
                if (claim.Type.Equals(key) && (claim.Value.Equals(value)))
                    return true;
            }
            return false;
        }


        /// <summary>
        /// Factory method for instantiating a new m3User instance based on the current logged in user.
        /// </summary>
        /// <returns></returns>
        public static M3User GetM3UserFromCurrentContext()
        {
            M3User m3User = new M3User();

            // set the core identity object to the current identified user
            m3User.Identity = HttpContext.Current.User.Identity;
            
            // check to see if the current user is authenticated
            if (m3User.Identity.IsAuthenticated)
            {
                try
                {
                    // the user is authenticated so get the claims based attributes
                    // first grab the ClaimsIdentity
                    ClaimsIdentity claimsIdentity = (ClaimsIdentity) HttpContext.Current.User.Identity;

                    // get the M3 unique internal system identifier for the user
                    // using FindFirstValue -> Returns null if the value doesn't exist
                    m3User.Id = claimsIdentity.FindFirstValue(M3IdentityConstants.CLAIM_TYPE_M3ID);

                    m3User.ContextId = claimsIdentity.FindFirstValue(M3IdentityConstants.CLAIM_TYPE_CONTEXTID);
                    
                    // parse the roles
                    string roleString = claimsIdentity.FindFirstValue(ClaimTypes.Role);
                    
                    if (roleString.IsNullOrEmpty())
                    {
                        // no roles assigned so create the emtpy array
                        m3User.Roles = new string[] {};

                    }
                    else
                    {
                        // parse the role string
                        m3User.Roles = roleString.Split(Convert.ToChar(M3IdentityConstants.CLAIM_DELIMITER)).ToArray<string>();
                    }

                    //Create runtime properties
                    if (claimsIdentity.Claims != null && claimsIdentity.Claims.Count() > 0)
                    {
                        //Get all the dynamic claims
                        var dynamicClaims = claimsIdentity.Claims
                            .Where(claim => claim.Type.Contains(M3IdentityConstants.CLAIM_TYPE_DYNAMIC))
                            .ToList();

                        if (dynamicClaims != null && dynamicClaims.Count > 0)
                        {
                            //Cast ExpandoObject into IDictionary so we can add a property name and value, even though the claim value is a string
                            //ExpandoObject expects an object type as value, by not using object the cast will fail.
                            var dynamicProperties = new ExpandoObject() as IDictionary<string, object>;
                            foreach (var dynamicClaim in dynamicClaims)
                            {
                                //Since the dynamic claims always contains the prefix represented by CLAIM_TYPE_DYNAMIC, by replacing the constant with
                                //an empty string, we'll have the Property name.
                                dynamicProperties.Add(dynamicClaim.Type.Replace(M3IdentityConstants.CLAIM_TYPE_DYNAMIC, string.Empty), dynamicClaim.Value);
                            }
                            m3User.AdditionalProperties = dynamicProperties;
                        }
                    }
                }
                catch (Exception ex)
                {
                    // Missing required claims or the cookie was malformed so reset identity to blank
                    m3User = new M3User();

                    // force a logout
                    M3IdentityService.InvalidateWebSession();

                    //TODO:  log the issue
                    throw new Exception("M3User detected a malformed cookie or missing claim.  The user was logged out and session destroyed.  Details:  " + ex.Message);
                }

            }
            else
            {
                // perform any additional non-authenticated clean up...
                // the fields are initialized to be empty....
            }

            return m3User;
            
        }
       
    }
}
