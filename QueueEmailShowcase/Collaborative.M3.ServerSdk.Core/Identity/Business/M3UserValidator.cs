﻿using Collaborative.M3.ServerSdk.Core.Identity.Domain;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Identity.Business
{
    public class M3UserValidator<TKey> : UserValidator<User<TKey>, TKey>
        where TKey : IEquatable<TKey>
    {
        //
        // Summary:
        //     Constructor
        //
        // Parameters:
        //   manager:
        public M3UserValidator(UserManager<User<TKey>, TKey> manager)
            : base(manager)
        {

        }
    }
}
