﻿using Collaborative.M3.ServerSdk.Core.Identity.Domain;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Identity.Business
{
    public class M3DataProtectorTokenProvider<TKey> : DataProtectorTokenProvider<User<TKey>, TKey> 
        where TKey : IEquatable<TKey>
    {
        public M3DataProtectorTokenProvider(IDataProtector protector)
            : base(protector)
        {

        }
    }
}
