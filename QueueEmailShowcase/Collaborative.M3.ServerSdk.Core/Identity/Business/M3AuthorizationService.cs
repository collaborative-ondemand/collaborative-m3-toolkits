﻿using Collaborative.M3.ServerSdk.Core.Business;
using Collaborative.M3.ServerSdk.Core.Business.Common.Messaging;
using Collaborative.M3.ServerSdk.Core.Common;
using Collaborative.M3.ServerSdk.Core.Common.Implementation;
using Collaborative.M3.ServerSdk.Core.Identity.Integration.Interface;
using Collaborative.M3.ServerSdk.Core.Logging;
using Collaborative.M3.ServerSdk.Identity.Domain;
using Collaborative.M3.ServerSdk.Identity.Integration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Identity.Business
{
    public class M3AuthorizationService : IM3BusinessService
    {
        private static readonly M3Logger logger = M3Logger.getLogger(MethodBase.GetCurrentMethod().DeclaringType.Name);
        private static IM3AuthorizationDataIntegrationService _userAuthorizationStore;

        /// <summary>
        /// Expose a way to inject the dependency, DI Property injection, otherwise use the instance by looking up in the config file
        /// </summary>
        public static IM3AuthorizationDataIntegrationService M3AuthorizationDataIntegrationService
        {
            get
            {
                _userAuthorizationStore = M3Resolver.Current.GetService<IM3AuthorizationDataIntegrationService>();
                return _userAuthorizationStore;
            }
            set
            {
                _userAuthorizationStore = value;
            }
        }

        /// <summary>
        /// Loads a flat list of all roles and entitlements
        /// </summary>
        /// <returns></returns>
        public static RoleEntitlementItem[] LoadRoleEntitlementMatrix()
        {
            RoleEntitlementItem[] roleEntitlementMatrixResult = M3AuthorizationDataIntegrationService.GetAllEntitlementsAllRoles();
            return roleEntitlementMatrixResult;
        }

        /// <summary>
        /// Looks up the roles for a given user id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string[] FindRolesForUserId(string id)
        {
            RoleInfoBasic[] findRolesResult = M3AuthorizationDataIntegrationService.GetRolesForUserById(id);

            string[] roleList = new string[0];

            if (findRolesResult != null)
            {
                // thunk into string array
                roleList = findRolesResult.Select(roleInfo => roleInfo.RoleKey).ToArray<string>();

            }
            else
            {
                // if something went wrong then set the array to empty
                roleList = new string[] { };
            }

            // return
            return roleList;

        }

    }
}
