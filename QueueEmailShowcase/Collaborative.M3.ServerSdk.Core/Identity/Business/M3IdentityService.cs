﻿using Collaborative.M3.ServerSdk.Core.Business.Common.Messaging;
using Collaborative.M3.ServerSdk.Core.Utils;
using Collaborative.M3.ServerSdk.Identity.Domain;
using Collaborative.M3.ServerSdk.Identity.Integration;
using Collaborative.M3.ServerSdk.Identity.Common.User;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Collaborative.M3.ServerSdk.Identity.Common.Utils;
using Collaborative.M3.ServerSdk.Identity.Common.Util;
using Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage;
using Collaborative.M3.ServerSdk.Core.Cryptography;
using Microsoft.Owin.Infrastructure;
using Collaborative.M3.ServerSdk.Core.Logging;
using System.Reflection;
using Collaborative.M3.ServerSdk.Core.Business.Common.Exception;
using Collaborative.M3.ServerSdk.Core.Business;
using Collaborative.M3.ServerSdk.Core.Identity.Integration.Interface;
using Collaborative.M3.ServerSdk.Core.Identity.Domain.EF;
using Collaborative.M3.ServerSdk.Core.Identity.Integration.Implementation.EF;
using Collaborative.M3.ServerSdk.Core.Common;
using Collaborative.M3.ServerSdk.Core.Common.Implementation;

namespace Collaborative.M3.ServerSdk.Identity.Business
{
    /// <summary>
    /// The M3IdentityService provides core services for managing a user identity and relationships to authorization roles.
    /// </summary>
    public class M3IdentityService : IM3BusinessService
    {
        private static readonly M3Logger logger = M3Logger.getLogger(MethodBase.GetCurrentMethod().DeclaringType.Name);
        private static IM3UserDataIntegrationService _userAuthenticationStore;
        private static IM3AuthorizationDataIntegrationService _userAuthorizationStore;

        /// <summary>
        /// Expose a way to inject the dependency, DI Property injection, otherwise use the instance by looking up in the config file
        /// </summary>
        public static IM3AuthorizationDataIntegrationService M3AuthorizationDataIntegrationService
        {
            get
            {
                _userAuthorizationStore = M3Resolver.Current.GetService<IM3AuthorizationDataIntegrationService>();
                return _userAuthorizationStore;
            }
            set
            {
                _userAuthorizationStore = value;
            }
        }

        /// <summary>
        /// Expose a way to inject the dependency, DI Property injection, otherwise use the instance by looking up in the config file
        /// </summary>
        public static IM3UserDataIntegrationService M3UserDataIntegrationService
        {
            get
            {
                _userAuthenticationStore = M3Resolver.Current.GetService<IM3UserDataIntegrationService>();
                return _userAuthenticationStore;
            }
            set
            {
                _userAuthenticationStore = value;
            }
        }
        /// <summary>
        /// Initiate a web session based on a user's web credentials.
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>

        public enum PasswordStrategy
        {
            Hashed,
            Encrypted,
            ClearText
        }
        public static void AuthenticateWebCredential(
            UserCredentialLogin userCredentialLogin,
            PasswordStrategy passwordStrategy = PasswordStrategy.Hashed)
        {
            //TODO: This method is not generic it assumes that the password is hashed when there might be different encryption mechanisms
            //      Propose -> Send password as clear text to DataIntegrationService and let the service to determine if the comparison
            //                 needs to be hashed, encrypted or send as clear text.

            // STEP 1: lookup the credential
            UserCredential findCredentialUserResult = M3UserDataIntegrationService.FindUserCredentialByUserName(userCredentialLogin.UserName);

            // check to see if found
            if (findCredentialUserResult == null)
            {
                //log that the username was not found
                logger.error("M3IdentityService.AuthenticateWebCredential", "User not found.");
                throw new M3Exception("M3IdentityService:0001", "No user found.", M3ServiceResultFailCodes.M3FailRequestedDataNotFound);
            }

            //STEP 2: Check to see if active.  If inactive, throw an exception and don't verify the credentials nor create the web session. 
            if (findCredentialUserResult.Active == false)
            {
                //log that the user is inactive
                logger.error("M3IdentityService.AuthenticateWebCredential", "User inactive.");
                throw new M3Exception("M3IdentityService:0003", "User inactive.", M3ServiceResultFailCodes.M3FailRequestInactiveUser);
            }

            // STEP 3:  verify the password 
            // TODO:  enable encryption both directions  PasswordVerificationResult result = M3IdentityService.passwordHasher.VerifyHashedPassword(userName, password);

            bool verifyPasswordResult = M3PasswordHasher.VerifyHashedPassword(findCredentialUserResult.PasswordHash, userCredentialLogin.Password);

            // check result
            if (verifyPasswordResult)
            {
                // STEP 3:  If all is well then lookup the user by the internal ID and set the service result      
                // todo refactor into biz service for "FindLocalUserClaimsINfoById()
                LocalUserClaimsInfo localUserClaimsResult = GetLocalUserClaimsInfoById(findCredentialUserResult.Id);

                // should always be successful but good to check
                if (localUserClaimsResult != null)
                {
                    // create the web session
                    CreateWebSession(localUserClaimsResult, userCredentialLogin.RememberMe);

                }
            }
            else
            {
                // TODO:  audit failed password attempt
                logger.error("M3IdentityService.AuthenticateWebCredential", "Password failed.");
                throw new M3Exception("M3IdentityService:0002", "Password failed.", M3ServiceResultFailCodes.M3FailParameterInvalid);
            }
        }

        /// <summary>
        /// Initiates a session based on a device token.
        /// </summary>
        /// <param name="deviceTokenInfo"></param>
        /// <returns></returns>
        public static void AuthenticateOAuthRefreshToken(DeviceTokenInfo deviceTokenInfo)
        {
            string clearTextUserName = "";
            string encryptedToken = "";

            // STEP 1: split the composite token and find the token info
            try
            {

                // split it
                string[] tokenParts = SplitApiToken(deviceTokenInfo.RefreshToken);

                // check it exists
                clearTextUserName = tokenParts[0];
                encryptedToken = tokenParts[1];

                // confirm that the parameter is extracted
                if (encryptedToken.IsNullOrEmpty())
                {
                    // something is wrong with the token so return the fail
                    logger.error("M3IdentityService.AuthenticateOAuthRefreshToken", "Invalid Token.");
                    throw new M3Exception("M3IdentityService:0003", "Invalid Token.", M3ServiceResultFailCodes.M3FailParameterInvalid);
                }

            }
            catch (M3Exception e)
            {
                logger.error("M3IdentityService.AuthenticateOAuthRefreshToken", string.Format("An error occurred.  Message: {0}", e.GetBaseException().Message));
                throw;
            }
            catch (Exception ex)
            {
                logger.error("M3IdentityService.AuthenticateOAuthRefreshToken", string.Format("An error occurred splitting the token.  Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3IdentityService:0004", "An error occurred.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }

            try
            {
                // try to find the token info based on the token
                DeviceTokenInfo deviceTokenInfoResult = M3DeviceDataIntegrationService.FindDeviceTokenInfoByToken(deviceTokenInfo.RefreshToken);

                // check to see if found...if not, handle it, if so, return success
                if (deviceTokenInfoResult == null)
                {
                    logger.error("M3IdentityService.AuthenticateOAuthRefreshToken", "Invalid Device Token.");
                    throw new M3Exception("M3IdentityService:0005", "Invalid Device Token.", M3ServiceResultFailCodes.M3FailParameterInvalid);
                }
                else
                {
                    // extract the stored token
                    DeviceTokenInfo storedTokenInfo = deviceTokenInfoResult;

                    // found the token, confirm the inbound device tag and the token device tag match
                    if (deviceTokenInfo.DeviceAppRegistrationTag.Equals(storedTokenInfo.DeviceAppRegistrationTag, StringComparison.Ordinal) == false)
                    {
                        // the device app registration tags don't match so fail this
                        logger.error("M3IdentityService.AuthenticateOAuthRefreshToken", "Invalid deviceAppRegistrationTag.");
                        throw new M3Exception("M3IdentityService:0006", "Invalid deviceAppRegistrationTag.", M3ServiceResultFailCodes.M3FailParameterInvalid);
                    }
                }

                // get the user id from the username
                UserCredential userIdServiceResult = M3UserDataIntegrationService.FindUserCredentialByUserName(clearTextUserName);
                if (userIdServiceResult == null)
                {
                    logger.error("M3IdentityService.AuthenticateOAuthRefreshToken", "Invalid username.");
                    throw new M3Exception("M3IdentityService:0007", "Invalid username.", M3ServiceResultFailCodes.M3FailParameterInvalid);
                }

                // so far, so good.  The token was found and everything checks out so create the session
                LocalUserClaimsInfo localUserClaimsResult = GetLocalUserClaimsInfoById(userIdServiceResult.Id);

                // should always be successful but good to check
                if (localUserClaimsResult != null)
                {
                    // create the web session
                    CreateWebSession(localUserClaimsResult, false);
                }
                else
                {
                    // something went wrong....return the error
                    logger.error("M3IdentityService.AuthenticateOAuthRefreshToken", "Invalid user.");
                    throw new M3Exception("M3IdentityService:0008", "Invalid user.", M3ServiceResultFailCodes.M3FailParameterInvalid);
                }
            }
            catch (M3Exception e)
            {
                logger.error("M3IdentityService.AuthenticateOAuthRefreshToken", string.Format("An error occurred.  Message: {0}", e.GetBaseException().Message));
                throw;
            }
            catch (Exception ex)
            {
                logger.error("M3IdentityService.AuthenticateOAuthRefreshToken", string.Format("An error occurred.  Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3IdentityService:0010", "An error occurred.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }
        }

        /// <summary>
        /// Retrievs the claims associated with the user identified by the id parameter.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private static LocalUserClaimsInfo GetLocalUserClaimsInfoById(string id)
        {
            LocalUserClaimsInfo localClaimsInfoResult = new LocalUserClaimsInfo();

            // first find the basic info
            localClaimsInfoResult = M3UserDataIntegrationService.FindUserBasicInfoById(id);

            // lookup the roles
            string[] roleLookupResult = M3AuthorizationService.FindRolesForUserId(id);

            if (roleLookupResult != null)
            {
                // found roles so add them
                localClaimsInfoResult.Roles = roleLookupResult;
            }

            // return the result
            return localClaimsInfoResult;
        }

        /// <summary>
        /// Creates an OWIN ClaimsIdentity based on the credentials provided.
        /// </summary>
        /// <param name="userCredentialLogin"></param>
        /// <returns></returns>
        public static ClaimsIdentity GetAuthenticatedLocalUserClaimsIdentity(UserCredentialLogin userCredentialLogin)
        {
            // create a service result
            ClaimsIdentity claimsIdentity = new ClaimsIdentity();

            try
            {
                // verify authentication
                bool isAuthenticated = VerifyWebCredentials(userCredentialLogin.UserName, userCredentialLogin.Password);

                // check it
                if (isAuthenticated == false)
                {
                    // fail the request

                }

                // get the user id from the username
                UserCredential userIdServiceResult = M3UserDataIntegrationService.FindUserCredentialByUserName(userCredentialLogin.UserName);
                if (userIdServiceResult == null)
                {
                    // return the error
                }

                // get the local user claims info
                LocalUserClaimsInfo userClaimsInfoResult = GetLocalUserClaimsInfoById(userIdServiceResult.Id);

                // check the result
                if (userClaimsInfoResult == null)
                {
                    // error
                    logger.error("M3IdentityService.GetAuthenticatedLocalUserClaimsIdentity", "Local User Claim info is null.");
                    throw new M3Exception("M3IdentityService:0011", "Local User Claim info is null.", M3ServiceResultFailCodes.M3FailUnknown);

                }

                // authentication success so create the claims identity
                claimsIdentity = CreateClaimsIdentity(userClaimsInfoResult, DefaultAuthenticationTypes.ExternalBearer);

            }
            catch (M3Exception e)
            {
                logger.error("M3IdentityService.GetAuthenticatedLocalUserClaimsIdentity", string.Format("An error occurred. Message: {0}", e.Message));
                throw;
            }
            catch (Exception ex)
            {
                logger.error("M3IdentityService.GetAuthenticatedLocalUserClaimsIdentity", string.Format("An error occurred.  Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3IdentityService:0012", "An error occurred.", M3ServiceResultFailCodes.M3FailUnknown, ex);
            }

            // package and return
            return claimsIdentity;
        }

        /// <summary>
        /// Returns a non Authoritative representation of the user web credential
        /// </summary>
        /// <param name="userName"></param>
        /// <returns>WebCredentialuser</returns>
        private static UserCredential GetNonAuthoritativeWebCredential(string userName)
        {
            //Retrieve the specified WebCredential
            UserCredential webCredentialUserLookupResult = M3UserDataIntegrationService.FindUserCredentialByUserName(userName);
            return webCredentialUserLookupResult;
        }

        /// <summary>
        /// Returns information about the authenticated user.
        /// </summary>
        /// <returns></returns>
        public static UserMasterInfo GetUserMasterInfo()
        {
            // create a service result
            UserMasterInfo userMasterInfo = new UserMasterInfo();

            try
            {

                // get the logged in user
                M3User m3user = M3User.GetM3UserFromCurrentContext();

                // verify authentication
                if (m3user.Identity.IsAuthenticated == false)
                {
                    // set fail
                }

                // extract the info
                userMasterInfo.UserName = m3user.Identity.GetUserName();
            }
            catch (Exception ex)
            {
                logger.error("M3IdentityService.GetUserMasterInfo", string.Format("An error occurred.  Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3IdentityService:0013", "An error occurred.", M3ServiceResultFailCodes.M3FailUnknown, ex);
            }

            // return 
            return userMasterInfo;

        }

        /// <summary>
        /// Creates a web login account with web credentials.  After creation of the account, this service creates a web session for the user.
        /// </summary>
        /// <param name="userRegistrationInfo"></param>
        /// <returns></returns>
        public static bool RegisterUser(UserRegistrationInfo userRegistrationInfo)
        {

            logger.debug("M3IdentityService.RegisterUser", "Registering User");

            bool serviceResult = true;
            try
            {
                string passwordHash = M3PasswordHasher.HashPassword(userRegistrationInfo.Password);

                // create the user
                serviceResult = M3UserDataIntegrationService.CreateUserCredential(userRegistrationInfo, passwordHash);

                // lookup the new user then the claims info
                // TODO:  refactor to less round trips
                UserCredential webCredentialUserLookupResult = M3UserDataIntegrationService.FindUserCredentialByUserName(userRegistrationInfo.UserName);
                LocalUserClaimsInfo localUserClaimsInfoResult = GetLocalUserClaimsInfoById(webCredentialUserLookupResult.Id);

                // create the web session defaulting remember me to false
                bool sessionCreationResult = CreateWebSession(localUserClaimsInfoResult, false);

                if (sessionCreationResult == false)
                {
                    // error
                    logger.error("M3IdentityService.RegisterUser", "Session could not be created.");
                    throw new M3Exception("M3IdentityService:0015", "Session could not be created.", M3ServiceResultFailCodes.M3FailUnknown);
                }
            }
            catch (M3Exception e)
            {
                logger.error("M3IdentityService.RegisterUser", string.Format("An error occurred.  Message: {0}", e.GetBaseException().Message));
                throw;
            }
            catch (Exception ex)
            {
                logger.error("M3IdentityService.RegisterUser", string.Format("An error occurred.  Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3IdentityService:0014", "An error occurred.", M3ServiceResultFailCodes.M3FailUnknown, ex);
            }

            // if success then
            // return the result
            return serviceResult;
        }

        /// <summary>
        /// Creates a refresh token in the form of username:tokenhash
        /// </summary>
        /// <param name="deviceRegistrationParams"></param>
        /// <returns></returns>
        public static DeviceTokenInfo RegisterOAuthUser(DeviceRegistrationInfo deviceRegistrationParams)
        {
            // create the instance to hold the result
            DeviceTokenInfo deviceTokenInfo = new DeviceTokenInfo();

            try
            {

                // verify the credential
                bool authNResult = M3IdentityService.VerifyWebCredentials(deviceRegistrationParams.UserName, deviceRegistrationParams.Password);

                // check authN result
                if (authNResult == false)
                {
                    // AuthN failed so set fail and return
                    logger.error("M3IdentityService.RegisterOAuthUser", "Invalid credentials");
                    throw new M3Exception("M3IdentityService:0015", "Invalid Credentials.", M3ServiceResultFailCodes.M3FailUnknown);
                }

                // Get User Details
                // lookup the new user then the claims info
                UserCredential webCredentialUserLookupResult = M3UserDataIntegrationService.FindUserCredentialByUserName(deviceRegistrationParams.UserName);

                // copy in the device tag
                deviceTokenInfo.DeviceAppRegistrationTag = deviceRegistrationParams.DeviceAppRegistrationTag;

                // generate the token
                deviceTokenInfo.RefreshToken = M3IdentityService.GenerateApiToken(deviceRegistrationParams.UserName, deviceRegistrationParams.DeviceAppRegistrationTag);

                // store the new token
                M3DeviceDataIntegrationService.CreateDeviceRegistration(Convert.ToInt32(webCredentialUserLookupResult.Id), deviceTokenInfo);
            }
            catch (M3Exception e)
            {
                logger.error("M3IdentityService.RegisterOAuthUser", string.Format("An error occurred.  Message: {0}", e.GetBaseException().Message));
                throw;
            }
            catch (Exception ex)
            {
                logger.error("M3IdentityService.RegisterOAuthUser", string.Format("An error occurred.  Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3IdentityService:0016", "An error occurred.", M3ServiceResultFailCodes.M3FailUnknown, ex);
            }

            return deviceTokenInfo;
        }


        /// <summary>
        /// Updates the currently authenticated user's local login web credential.
        /// </summary>
        /// <param name="webCredentialChange"></param>
        /// <returns></returns>
        public static bool ChangeWebCredential(WebCredentialChange webCredentialChange)
        {
            // create a service result
            bool changeCredentialResult = true;

            try
            {
                // get the M3User
                M3User m3User = M3User.GetM3UserFromCurrentContext();

                // first verify the old credential
                if (VerifyWebCredentials(m3User.UserName, webCredentialChange.OldPassword))
                {
                    // hash the password
                    string hashedPassword = M3PasswordHasher.HashPassword(webCredentialChange.NewPassword);

                    // all is well so change the password
                    changeCredentialResult = M3UserDataIntegrationService.UpdateUserCredential(m3User.Id, hashedPassword);
                }
                else
                {
                    // something went wrong so return failed
                    logger.error("M3IdentityService.ChangeWebCredential", "Unable to verify credentials.");
                    throw new M3Exception("M3IdentityService:0017", "An error occurred verifying credentials.", M3ServiceResultFailCodes.M3FailUnknown);
                }
            }
            catch (M3Exception e)
            {
                logger.error("M3IdentityService.ChangeWebCredential", string.Format("An error occurred.  Message: {0}", e.GetBaseException().Message));
                throw;
            }
            catch (Exception ex)
            {
                logger.error("M3IdentityService.ChangeWebCredential", string.Format("An error occurred.  Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3IdentityService:0018", "An error occurred.", M3ServiceResultFailCodes.M3FailUnknown, ex);
            }

            return changeCredentialResult;

        }

        /// <summary>
        /// finds the web credential information and verifies it against the parameters returning true if it matches and false if it does not match.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        private static bool VerifyWebCredentials(string userName, string password)
        {
            // STEP 1: lookup the credential
            UserCredential findCredentialUserResult = M3UserDataIntegrationService.FindUserCredentialByUserName(userName);

            // check to see if found
            if (findCredentialUserResult == null)
            {
                // not found so return the fail
            }

            // STEP 2:  verify the password 
            bool isPasswordVerified = M3PasswordHasher.VerifyHashedPassword(findCredentialUserResult.PasswordHash, password);

            // return the results
            return isPasswordVerified;
        }


        /// <summary>
        /// Destroys the active the web session.
        /// </summary>
        public static void InvalidateWebSession()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }


        /// <summary>
        /// Creates a new web session.
        /// </summary>
        /// <param name="userClaimsInfo"></param>
        /// <param name="isPersistent"></param>
        /// <returns></returns>
        private static bool CreateWebSession(LocalUserClaimsInfo userClaimsInfo, bool isPersistent)
        {

            // clear the previous app cookie
            M3IdentityService.InvalidateWebSession();

            // load the claims
            ClaimsIdentity claimsIdentity = CreateClaimsIdentity(userClaimsInfo, DefaultAuthenticationTypes.ApplicationCookie);


            // set the new cookie
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, claimsIdentity);

            return true;
        }


        /// <summary>
        /// Helper function to create a claims identity for this login.
        /// </summary>
        /// <param name="userBasicInfo"></param>
        /// <param name="AuthenticationType"></param>
        /// <returns></returns>
        private static ClaimsIdentity CreateClaimsIdentity(LocalUserClaimsInfo userBasicInfo, string AuthenticationType)
        {
            IList<Claim> claimsCollection = new List<Claim>();

            // add claims
            claimsCollection.Add(new Claim(M3IdentityConstants.CLAIM_TYPE_M3ID, userBasicInfo.Id));
            claimsCollection.Add(new Claim(ClaimTypes.NameIdentifier, userBasicInfo.UserName));
            claimsCollection.Add(new Claim(ClaimTypes.Name, userBasicInfo.UserName));
            claimsCollection.Add(new Claim(ClaimTypes.Email, userBasicInfo.UserName));
            claimsCollection.Add(new Claim(ClaimTypes.Role, string.Join(M3IdentityConstants.CLAIM_DELIMITER, userBasicInfo.Roles)));
            claimsCollection.Add(new Claim(ClaimTypes.GivenName, userBasicInfo.GivenName == null ? "" : userBasicInfo.GivenName));
            claimsCollection.Add(new Claim(ClaimTypes.Surname, userBasicInfo.Surname == null ? "" : userBasicInfo.Surname));
            if(userBasicInfo.ContextIds != null)
            {
                claimsCollection.Add(new Claim(M3IdentityConstants.CLAIM_TYPE_CONTEXTID, string.Join(M3IdentityConstants.CLAIM_DELIMITER, userBasicInfo.ContextIds)));
            }

            if(userBasicInfo.AdditionalClaims != null && userBasicInfo.AdditionalClaims.Count > 0)
            {
                foreach (var claim in userBasicInfo.AdditionalClaims)
                {
                    claimsCollection.Add(new Claim(M3IdentityConstants.CLAIM_TYPE_DYNAMIC + claim.Key, claim.Value));
                }
            }

            ClaimsIdentity claimsIdentity = new ClaimsIdentity(claimsCollection, DefaultAuthenticationTypes.ApplicationCookie);

            return claimsIdentity;
        }




        /// <summary>
        /// Creates a token based on provided data plus a salt value.  The token is in the form of [username]:[hash]
        /// </summary>
        /// <param name="appTag"></param>
        /// <returns></returns>
        private static string GenerateApiToken(string userName, string appTag)
        {
            // build up the parts of the token
            //string userName = M3User.GetM3UserFromCurrentContext().UserName;
            string salt = Guid.NewGuid().ToString();
            string hash = HashUtil.HashSHA256(appTag + salt);

            // put it all together into the token
            string token = userName + ":" + hash;

            return token;

        }

        /// <summary>
        /// Takes a full token and splits into the clear text identifier (position 0) and the encrypted token (position 1).
        /// </summary>
        /// <param name="fullToken"></param>
        /// <returns></returns>
        private static string[] SplitApiToken(string fullToken)
        {
            char[] charSeparators = new char[] { ':' };
            string[] tokenParts = fullToken.Split(charSeparators, 2);

            return tokenParts;
        }


        /// <summary>
        /// Helper...see if this can be refactored
        /// </summary>
        private static IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().Authentication;
            }
        }


        public static IList<UserMasterInfo> GetUserMasterByName(string name)
        {
            try
            {
                return M3UserDataIntegrationService.GetUserMastersByName(name);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public static UserMasterInfo GetUserMasterInfoById(int id)
        {
            try
            {
                return M3UserDataIntegrationService.GetUserMasterInfoById(id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static int CreateUser(UserRegistrationInfo userRegistrationInfo)
        {
            bool isRegisterUserSuccess = RegisterUser(userRegistrationInfo);

            return FindUserIdByUserName(userRegistrationInfo.UserName);
        }

        public static bool SetUserRoles(int userId, int roleId, bool autoCommit = false)
        {
            try
            {
                return M3UserDataIntegrationService.SetUserRoles(userId, roleId, autoCommit);
            }
            catch (Exception ex)
            {
                logger.error("Setting User Role by User Id", string.Format("An error occurred.  Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3IdentityService:0010", "An error occurred.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }
        }

        public static bool SetUserRoles(string userName, int roleId, bool autoCommit = false)
        {
            try
            {
                return M3UserDataIntegrationService.SetUserRoles(userName, roleId, autoCommit);
            }
            catch (Exception ex)
            {
                logger.error("Setting User Role by Username", string.Format("An error occurred.  Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3IdentityService:0010", "An error occurred.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }
        }

        public static RoleInfo[] GetAllRoles()
        {
            try
            {
                return M3UserDataIntegrationService.GetAllRoles();
            }
            catch (Exception ex)
            {
                logger.error("Getting all roles from the database", string.Format("An error occurred.  Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3IdentityService:0010", "An error occurred.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }
        }

        public static int FindUserIdByUserName(string userName)
        {
            int userId = 0;
            try
            {
                UserCredential userCredential = M3UserDataIntegrationService.FindUserCredentialByUserName(userName);

                //It fails if we inject AOP, since AOP logging will stop the application when an exception is thrown
                if (userCredential != null)
                {
                    if (userCredential.Active == true && Int32.TryParse(userCredential.Id, out userId))
                    {
                        return userId;
                    }
                }
                else
                {
                    userId = 0;
                }
            }
            catch (M3Exception)
            {
                //this error happens because userName was not found.
                userId = 0;
            }
            return userId;
        }

        public static bool VerifyPassword(string currentHashedPassword, string providedPassword)
        {
            try
            {
                return M3PasswordHasher.VerifyHashedPassword(currentHashedPassword, providedPassword);
            }
            catch (Exception ex)
            {
                logger.error("UpdateUserCredentialsByName", string.Format("An error occurred.  Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3IdentityService:0010", "An error occurred.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }
        }

        public static bool UpdateUserCredentialsByName(string username, string password)
        {
            bool updated = false;

            try
            {
                M3UserDataIntegrationService.UpdateUserCredentialByName(username, M3PasswordHasher.HashPassword(password));
            }
            catch (Exception ex)
            {
                logger.error("Getting all roles from the database", string.Format("An error occurred.  Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3IdentityService:0010", "An error occurred.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }

            return updated;
        }
    }
}
