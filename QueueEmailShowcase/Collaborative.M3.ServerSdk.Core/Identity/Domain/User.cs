﻿using Collaborative.M3.ServerSdk.Core.Domain;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Identity.Domain
{
    public class User<TKey> : BaseEntity<TKey>, IUser<TKey>
        where TKey : IEquatable<TKey>
    {
        TKey _id;
        public User(TKey id)
        {
            _id = id;
        }
        public string UserName
        {
            get;set;
        }

        public new TKey Id
        {
            get
            {
                return _id;
            }
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User<TKey>, TKey> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }
}
