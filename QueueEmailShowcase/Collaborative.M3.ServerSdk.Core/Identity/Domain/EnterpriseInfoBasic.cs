﻿using Collaborative.M3.ServerSdk.Core.Business.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Identity.Domain
{
    public class EnterpriseInfoBasic : IM3Domain
    {
        public string EntitlementId { get; set; }
        public string EntitlementKey { get; set; }
    }
}
