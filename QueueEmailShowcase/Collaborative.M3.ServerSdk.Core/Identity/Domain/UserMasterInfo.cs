﻿using Collaborative.M3.ServerSdk.Core.Business.Domain;
using Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Identity.Domain
{
    public class UserMasterInfo : IM3Domain, IM3MessageBody
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
    }
}
