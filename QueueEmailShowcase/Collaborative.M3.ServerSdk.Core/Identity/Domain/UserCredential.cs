﻿using Collaborative.M3.ServerSdk.Core.Business.Domain;
using Collaborative.M3.ServerSdk.Identity.Common.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Identity.Domain
{
    public class UserCredential : LocalUserClaimsInfo
    {
        public bool Active { get; set;}
        public string PasswordHash { get; set; }
    }
}
