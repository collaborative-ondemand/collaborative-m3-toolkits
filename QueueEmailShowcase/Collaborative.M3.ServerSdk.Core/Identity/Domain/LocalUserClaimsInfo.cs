﻿using Collaborative.M3.ServerSdk.Core.Business.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Identity.Domain
{
    /// <summary>
    /// Holds the claims information for the user.  
    /// </summary>
    public class LocalUserClaimsInfo : IM3Domain
    {
        public string Id { set; get; }
        public string UserName { set; get; }
        public string[] Roles { get; set; }
        public string[] ContextIds { get; set; }
        public string GivenName { get; set; }
        public string Surname { get; set; }
        //Extend Claims so we can add extra info
        public Dictionary<string, string> AdditionalClaims { get; set; }
    }
}
