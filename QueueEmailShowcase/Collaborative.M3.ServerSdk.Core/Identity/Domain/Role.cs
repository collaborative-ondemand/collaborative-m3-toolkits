﻿using Collaborative.M3.ServerSdk.Core.Business.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Identity.Domain
{
    public class Role : IM3Domain
    {
        public string RoleId { get; set; }
        public string RoleKey { get; set; }
        public string RoleDisplayName { get; set; }
        public string RoleDescription { get; set; }

        /// <summary>
        /// Contains a Dictionary<> of all entitlements associated with this role.
        /// </summary>
        public Dictionary<string,EntitlementInfo> Entitlements {get; private set;}

        public Role()
        {
            this.Entitlements = new Dictionary<string, EntitlementInfo>();
        }
    }
}
