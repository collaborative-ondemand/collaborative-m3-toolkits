﻿using Collaborative.M3.ServerSdk.Core.Business.Domain;
using Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Identity.Domain
{
    /// <summary>
    /// Domain object to accept a typical web login
    /// </summary>
    public class UserCredentialLogin : IM3Domain, IM3MessageBody
    {
        public string UserName { get; set; }
        public string Password {get; set; }
        public bool RememberMe {get; set; }
    }
}
