﻿using Collaborative.M3.ServerSdk.Core.Business.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Identity.Domain
{
    public class RoleInfoBasic : IM3Domain
    {
        public string RoleId { get; set; }
        public string RoleKey { get; set; }
    }
}
