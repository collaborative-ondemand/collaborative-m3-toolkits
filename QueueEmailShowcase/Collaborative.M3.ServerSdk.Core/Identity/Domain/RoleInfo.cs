﻿using Collaborative.M3.ServerSdk.Core.Business.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Identity.Domain
{
    public class RoleInfo : IM3Domain
    {
        public string RoleId { get; set; }
        public string RoleKey { get; set; }
        public string RoleDisplayName { get; set; }
        public string RoleDescription { get; set; }
        public bool? RoleActive { get; set; }
        
        public int? RoleTypeId { get; set; }
        public string RoleTypeKey { get; set; }
        public string RoleTypeDescription { get; set; }
        public string RoleTypeDisplayName { get; set; }
        public int RoleTypeDisplayOrder { get; set; }
        public bool RoleTypeActive { get; set; }
    }
}
