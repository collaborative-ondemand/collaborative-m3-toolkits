﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Identity.Domain.EF.EFMappings
{
    public class IdnEntitlementDefinitionMapping : EntityTypeConfiguration<IdnEntitlementDefinition>
    {
        public IdnEntitlementDefinitionMapping()
        {
            this.ToTable("IDN_ENTITLEMENTS");

            this.HasKey(idnEntitlementDefinition => idnEntitlementDefinition.Id)
                .Property(idnEntitlementDefinition => idnEntitlementDefinition.Id)
                .HasColumnName("ENTITLEMENT_ID")
                .IsRequired();

            this.Property(idnEntitlementDefinition => idnEntitlementDefinition.EntitlementKey)
                .HasColumnName("KEY")
                .HasMaxLength(48)
                .IsRequired();

            this.Property(idnEntitlementDefinition => idnEntitlementDefinition.EntitlementDisplayName)
                .HasColumnName("NAME")
                .HasMaxLength(64)
                .IsOptional();

            this.Property(idnEntitlementDefinition => idnEntitlementDefinition.EntitlementDescription)
                .HasColumnName("DESCRIPTION")
                .HasMaxLength(128)
                .IsOptional();


            this.Property(idnEntitlementDefinition => idnEntitlementDefinition.ModifiedDate)
                .HasColumnName("MODIFIED_DT")
                .IsRequired();

            this.Property(idnEntitlementDefinition => idnEntitlementDefinition.ModifiedBy)
                .HasColumnName("MODIFIED_BY")
                .HasMaxLength(38)
                .IsRequired();

            this.Property(idnEntitlementDefinition => idnEntitlementDefinition.CreatedDate)
                .HasColumnName("CREATED_DT")
                .IsRequired();

            this.Property(idnEntitlementDefinition => idnEntitlementDefinition.CreatedBy)
                .HasColumnName("CREATED_BY")
                .HasMaxLength(38)
                .IsRequired();
        }
    }
}
