﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Identity.Domain.EF.EFMappings
{
    public class IdnUserMasterMapping : EntityTypeConfiguration<IdnUserMaster>
    {
        public IdnUserMasterMapping()
        {
            this.ToTable("IDN_USERS");

            this.HasKey(idnUserMaster => idnUserMaster.Id)
                .Property(idnUserMaster => idnUserMaster.Id)
                .HasColumnName("USER_ID")
                .IsRequired();

            this.Property(idnUserMaster => idnUserMaster.UserNameIdentifier)
                .HasColumnName("USER_NAME")
                .HasMaxLength(20)
                .IsRequired();

            this.Property(idnUserMaster => idnUserMaster.Active)
                .HasColumnName("ACTIVE")
                .IsRequired();


            this.Property(idnUserMaster => idnUserMaster.ModifiedDate)
                .HasColumnName("MODIFIED_DT")
                .IsRequired();

            this.Property(idnUserMaster => idnUserMaster.ModifiedBy)
                .HasColumnName("MODIFIED_BY")
                .HasMaxLength(38)
                .IsRequired();

            this.Property(idnUserMaster => idnUserMaster.CreatedDate)
                .HasColumnName("CREATED_DT")
                .IsRequired();

            this.Property(idnUserMaster => idnUserMaster.CreatedBy)
                .HasColumnName("CREATED_BY")
                .HasMaxLength(38)
                .IsRequired();
        }
    }
}
