﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Identity.Domain.EF.EFMappings
{
    public class IdnUserRoleMapping : EntityTypeConfiguration<IdnUserRole>
    {
        public IdnUserRoleMapping()
        {
            this.ToTable("IDN_USER_ROLES");

            this.HasKey(idnUserRole => idnUserRole.Id)
                .Property(idnUserRole => idnUserRole.Id)
                .HasColumnName("USER_ROLE_ID")
                .IsRequired();

            this.Property(idnUserRole => idnUserRole.UserId)
                .HasColumnName("USER_ID")
                .IsRequired();

            this.HasRequired(idnUserRole => idnUserRole.IdnRoleDefinition)
                .WithMany(idnRoleDefinition => idnRoleDefinition.IdnUserRoles)
                .HasForeignKey(idnUserRole => idnUserRole.RoleId)
                .WillCascadeOnDelete(false);


            this.Property(idnUserRole => idnUserRole.RoleId)
                .HasColumnName("ROLE_ID")
                .IsRequired();

            this.HasRequired(idnUserRole => idnUserRole.IdnUserMaster)
                .WithMany(idnUserMaster => idnUserMaster.IdnUserRoles)
                .HasForeignKey(idnUserRole => idnUserRole.UserId)
                .WillCascadeOnDelete(false);


            this.Property(idnUserRole => idnUserRole.ModifiedDate)
                .HasColumnName("MODIFIED_DT")
                .IsRequired();

            this.Property(idnUserRole => idnUserRole.ModifiedBy)
                .HasColumnName("MODIFIED_BY")
                .HasMaxLength(38)
                .IsRequired();

            this.Property(idnUserRole => idnUserRole.CreatedDate)
                .HasColumnName("CREATED_DT")
                .IsRequired();

            this.Property(idnUserRole => idnUserRole.CreatedBy)
                .HasColumnName("CREATED_BY")
                .HasMaxLength(38)
                .IsRequired();
        }
    }
}
