﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Identity.Domain.EF.EFMappings
{
    public class IdnRoleEntitlementMapping : EntityTypeConfiguration<IdnRoleEntitlement>
    {
        public IdnRoleEntitlementMapping()
        {
            this.ToTable("IDN_ROLE_ENTITLEMENTS");

            this.HasKey(idnRoleEntitlement => idnRoleEntitlement.Id)
                .Property(idnRoleEntitlement => idnRoleEntitlement.Id)
                .HasColumnName("ROLE_ENTITLEMENT_ID")
                .IsRequired();

            this.Property(idnRoleEntitlement => idnRoleEntitlement.RoleId)
                .HasColumnName("ROLE_ID")
                .IsRequired();

            this.HasRequired(idnRoleEntitlement => idnRoleEntitlement.IdnRoleDefinition)
                .WithMany(idnRoleDefinition => idnRoleDefinition.IdnRoleEntitlements)
                .HasForeignKey(idnRoleEntitlement => idnRoleEntitlement.RoleId)
                .WillCascadeOnDelete(false);


            this.Property(idnRoleEntitlement => idnRoleEntitlement.EntitlementId)
                .HasColumnName("ENTITLEMENT_ID")
                .IsRequired();

            this.HasRequired(idnRoleEntitlement => idnRoleEntitlement.IdnEntitlementDefinition)
                .WithMany(idnEntitlementDefinition => idnEntitlementDefinition.IdnRoleEntitlements)
                .HasForeignKey(idnRoleEntitlement => idnRoleEntitlement.EntitlementId)
                .WillCascadeOnDelete(false);


            this.Property(idnRoleEntitlement => idnRoleEntitlement.ModifiedDate)
                .HasColumnName("MODIFIED_DT")
                .IsRequired();

            this.Property(idnRoleEntitlement => idnRoleEntitlement.ModifiedBy)
                .HasColumnName("MODIFIED_BY")
                .HasMaxLength(38)
                .IsRequired();

            this.Property(idnRoleEntitlement => idnRoleEntitlement.CreatedDate)
                .HasColumnName("CREATED_DT")
                .IsRequired();

            this.Property(idnRoleEntitlement => idnRoleEntitlement.CreatedBy)
                .HasColumnName("CREATED_BY")
                .HasMaxLength(38)
                .IsRequired();
        }
    }
}
