﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Identity.Domain.EF.EFMappings
{
    public class IdnWebCredentialMapping : EntityTypeConfiguration<IdnWebCredential>
    {
        public IdnWebCredentialMapping()
        {
            this.ToTable("IDN_WEB_CREDENTIALS");

            this.HasKey(idnWebCredential => idnWebCredential.Id)
                .Property(idnWebCredential => idnWebCredential.Id)
                .HasColumnName("WEB_CREDENTIAL_ID")
                .IsRequired();

            this.Property(idnWebCredential => idnWebCredential.UserId)
                .HasColumnName("USER_ID")
                .IsRequired();

            this.HasRequired(idnWebCredential => idnWebCredential.IdnUserMaster)
                .WithMany()
                .HasForeignKey(idnUserMaster => idnUserMaster.UserId)
                .WillCascadeOnDelete(false);


            this.Property(idnWebCredential => idnWebCredential.WebCredentialHash)
                .HasColumnName("PASSWORD_HASH")
                .HasMaxLength(256)
                .IsRequired();

            this.Property(idnWebCredential => idnWebCredential.ModifiedDate)
                .HasColumnName("MODIFIED_DT")
                .IsRequired();

            this.Property(idnWebCredential => idnWebCredential.ModifiedBy)
                .HasColumnName("MODIFIED_BY")
                .HasMaxLength(38)
                .IsRequired();

            this.Property(idnWebCredential => idnWebCredential.CreatedDate)
                .HasColumnName("CREATED_DT")
                .IsRequired();

            this.Property(idnWebCredential => idnWebCredential.CreatedBy)
                .HasColumnName("CREATED_BY")
                .HasMaxLength(38)
                .IsRequired();
        }
    }
}
