﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Identity.Domain.EF.EFMappings
{
    public class IdnRoleDefinitionMapping : EntityTypeConfiguration<IdnRoleDefinition>
    {
        public IdnRoleDefinitionMapping()
        {
            this.ToTable("IDN_ROLES");

            this.HasKey(idnRoleDefinition => idnRoleDefinition.Id)
                .Property(idnRoleDefinition => idnRoleDefinition.Id)
                .HasColumnName("ROLE_ID")
                .IsRequired();

            this.Property(idnRoleDefinition => idnRoleDefinition.RoleKey)
                .HasColumnName("KEY")
                .HasMaxLength(32)
                .IsRequired();

            this.Property(idnRoleDefinition => idnRoleDefinition.RoleDisplayName)
                .HasColumnName("NAME")
                .HasMaxLength(48)
                .IsOptional();

            this.Property(idnRoleDefinition => idnRoleDefinition.RoleDescription)
                .HasColumnName("DESCRIPTION")
                .HasMaxLength(128)
                .IsOptional();


            this.Property(idnRoleDefinition => idnRoleDefinition.ModifiedDate)
                .HasColumnName("MODIFIED_DT")
                .IsRequired();

            this.Property(idnRoleDefinition => idnRoleDefinition.ModifiedBy)
                .HasColumnName("MODIFIED_BY")
                .HasMaxLength(38)
                .IsRequired();

            this.Property(idnRoleDefinition => idnRoleDefinition.CreatedDate)
                .HasColumnName("CREATED_DT")
                .IsRequired();

            this.Property(idnRoleDefinition => idnRoleDefinition.CreatedBy)
                .HasColumnName("CREATED_BY")
                .HasMaxLength(38)
                .IsRequired();
        }
    }
}
