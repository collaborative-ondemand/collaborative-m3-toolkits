﻿using Collaborative.M3.ServerSdk.Core.Common.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Identity.Domain.EF
{
    public class IdnUserMaster : BaseEntity
    {
        /// <summary>
        /// Required Parameter
        /// </summary>
        public string UserNameIdentifier { get; set; }
        /// <summary>
        /// Required Parameter
        /// </summary>
        public bool Active { get; set; }
        public virtual ICollection<IdnUserRole> IdnUserRoles { get; set; }
    }
}
