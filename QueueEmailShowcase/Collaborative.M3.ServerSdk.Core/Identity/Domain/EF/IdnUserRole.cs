﻿using Collaborative.M3.ServerSdk.Core.Common.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Identity.Domain.EF
{
    public class IdnUserRole : BaseEntity
    {
        public int UserId { get; set; }
        public virtual IdnUserMaster IdnUserMaster { get; set; }
        public int RoleId { get; set; }
        public virtual IdnRoleDefinition IdnRoleDefinition { get; set; }
    }
}
