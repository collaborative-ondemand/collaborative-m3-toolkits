﻿using Collaborative.M3.ServerSdk.Core.Common.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Identity.Domain.EF
{
    public class IdnRoleDefinition : BaseEntity
    {
        /// <summary>
        /// Required Parameter
        /// </summary>
        public string RoleKey { get; set; }
        /// <summary>
        /// Optional Parameter
        /// </summary>
        public string RoleDisplayName { get; set; }
        /// <summary>
        /// Optional Parameter
        /// </summary>
        public string RoleDescription { get; set; }
        public virtual ICollection<IdnUserRole> IdnUserRoles { get; set; }
        public virtual ICollection<IdnRoleEntitlement> IdnRoleEntitlements { get; set; }
    }
}
