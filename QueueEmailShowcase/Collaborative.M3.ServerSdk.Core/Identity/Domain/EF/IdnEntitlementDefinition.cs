﻿using Collaborative.M3.ServerSdk.Core.Common.Domain;
using Collaborative.M3.ServerSdk.Core.Identity.Domain.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Identity.Domain.EF
{
    public class IdnEntitlementDefinition : BaseEntity
    {
        /// <summary>
        /// Required Parameter
        /// </summary>
        public string EntitlementKey { get; set; }
        /// <summary>
        /// Optional Parameter
        /// </summary>
        public string EntitlementDisplayName { get; set; }
        /// <summary>
        /// Optional Parameter
        /// </summary>
        public string EntitlementDescription { get; set; }
        public virtual ICollection<IdnRoleEntitlement> IdnRoleEntitlements { get; set; }
    }
}
