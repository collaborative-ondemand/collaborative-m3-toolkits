﻿using Collaborative.M3.ServerSdk.Core.Common.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Identity.Domain.EF
{
    public class IdnWebCredential : BaseEntity
    {
        public int UserId { get; set; }
        public virtual IdnUserMaster IdnUserMaster { get; set; }
        /// <summary>
        /// Required Parameter
        /// </summary>
        public string WebCredentialHash { get; set; }
    }
}
