﻿using Collaborative.M3.ServerSdk.Core.Business.Common.Messaging;
using Collaborative.M3.ServerSdk.Identity.Domain;
using Collaborative.M3.ServerSdk.Identity.Integration.Util;
using Collaborative.M3.ServerSdk.Identity.Business;
using Collaborative.M3.ServerSdk.Identity.Common.User;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Collaborative.M3.ServerSdk.Identity.Common.Utils;
using Collaborative.M3.ServerSdk.Core.Business.Integration.Database.Utilities;
using Collaborative.M3.ServerSdk.Core.Logging;
using System.Reflection;
using System.Diagnostics;
using Collaborative.M3.ServerSdk.Core.Business.Common.Exception;
using Collaborative.M3.ServerSdk.Core.Integration;

namespace Collaborative.M3.ServerSdk.Identity.Integration
{
    public class M3UserDataIntegrationService : IM3IntegrationService
    {
        private static readonly M3Logger logger = M3Logger.getLogger(MethodBase.GetCurrentMethod().DeclaringType.Name);
 
        /// <summary>
        /// Inserts a new user registration into the physical data store.
        /// </summary>
        /// <param name="webRegistrationInfo"></param>
        /// <returns></returns>
        public static bool CreateWebCredentialUser(UserRegistrationInfo webRegistrationInfo, string hashedPassword) 
        {

            Stopwatch sw = new Stopwatch();
            sw.Start();
            logger.info("M3UserDataIntegrationService.CreateWebCredentialUser", "Creating Web Credential User");

            try
            {
                // declare the dataset
                DataSet dataSet = new DataSet();

                // get a connection
                using (IDbConnection conn = M3DBUtils.getDbConnection(M3IdentityDbConstants.DB_CONNECTION_NAME_M3IDENTITY))
                {
                    // create the command object
                    IDbCommand command = M3DBUtils.createCommand(conn, M3IdentityDbConstants.SP_CREATE_WEB_USER, CommandType.StoredProcedure);

                    // configure the parameters
                    M3DBUtils.addParameter(command, M3IdentityDbConstants.PARAM_USER_NAME_IDENTIFIER, webRegistrationInfo.UserName);
                    M3DBUtils.addParameter(command, M3IdentityDbConstants.PARAM_IS_ACTIVE, M3IdentityDbConstants.FLAG_ENABLED);
                    M3DBUtils.addParameter(command, M3IdentityDbConstants.PARAM_WEB_CREDENTIAL_HASH, hashedPassword);

                    // open the connection
                    conn.Open();

                    // execute the proc
                    int numberOfRows = command.ExecuteNonQuery();

                    // check the retval
                    if (numberOfRows == 0)
                    {
                        // something wrong
                        logger.error("M3UserDataIntegrationService.CreateWebCredentialUser", "No records were created.");
                        throw new M3Exception("M3UserDataIntegrationService:0010", "Unable to Create Web Credential User.", M3ServiceResultFailCodes.M3FailUnknown);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.error("M3UserDataIntegrationService.CreateWebCredentialUser", string.Format("An exception occurred trying to create the user. Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3UserDataIntegrationService:0001", "Unable to Create Web Credential User.", M3ServiceResultFailCodes.M3FailUnknown, ex);
            }

            sw.Stop();
            logger.info("M3UserDataIntegrationService.CreateWebCredentialUser", sw, "Created Web Credential User");

            return true;
        }

        /// <summary>
        /// Inserts a new user role into the physical data store.
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns></returns>
        public static bool AddRoleToUser(int userId, int roleId)
        {

            Stopwatch sw = new Stopwatch();
            sw.Start();
            logger.info("M3UserDataIntegrationService.AddRoleToUser", "Add Role To User");

            try
            {
                // declare the dataset
                DataSet dataSet = new DataSet();

                // get a connection
                using (IDbConnection conn = M3DBUtils.getDbConnection(M3IdentityDbConstants.DB_CONNECTION_NAME_M3IDENTITY))
                {
                    // create the command object
                    IDbCommand command = M3DBUtils.createCommand(conn, M3IdentityDbConstants.SP_ADD_ROLE_TO_USER, CommandType.StoredProcedure);

                    // configure the parameters
                    M3DBUtils.addParameter(command, M3IdentityDbConstants.PARAM_SYSTEM_USER_ID, userId);
                    M3DBUtils.addParameter(command, M3IdentityDbConstants.PARAM_ROLE_ID, roleId);

                    // open the connection
                    conn.Open();

                    // execute the proc
                    int numberOfRows = command.ExecuteNonQuery();

                    // check the retval
                    if (numberOfRows == 0)
                    {
                        // something wrong
                        logger.error("M3UserDataIntegrationService.AddRoleToUser", "No records were created.");
                        throw new M3Exception("M3UserDataIntegrationService:0013", "Unable to Add Role To User.", M3ServiceResultFailCodes.M3FailUnknown);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.error("M3UserDataIntegrationService.AddRoleToUser", string.Format("An exception occurred trying to add the role to the user. Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3UserDataIntegrationService:0014", "Unable to Add Role To User.", M3ServiceResultFailCodes.M3FailUnknown, ex);
            }

            sw.Stop();
            logger.info("M3UserDataIntegrationService.AddRoleToUser", sw, "Add Role To User");

            return true;
        }

        /// </summary>
        /// <param name="userId">The internal unique identifier for this user.</param>
        /// <returns></returns>
        public static LocalUserClaimsInfo FindUserBasicInfoById(string userId)
        {
            // declare the user object
            LocalUserClaimsInfo userBasicInfo = null;

            // declare the dataset
            DataSet dataSet = new DataSet();

            // convert the string to an int value
            int systemUserId = 0;

            try
            {
                // perform the conversion
                systemUserId = Convert.ToInt32(userId);
            }
            catch (FormatException ex)
            {
                logger.error("M3UserDataIntegrationService.FindUserBasicInfoById", "Invalid User Id Format.");
                throw new M3Exception("M3UserDataIntegrationService:0002", "Invalid User Id.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }
            catch (OverflowException ex)
            {
                logger.error("M3UserDataIntegrationService.FindUserBasicInfoById", "Invalid User Id.");
                throw new M3Exception("M3UserDataIntegrationService:0003", "Invalid User Id.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }

            try
            {
                // get a connection
                using (IDbConnection conn = M3DBUtils.getDbConnection(M3IdentityDbConstants.DB_CONNECTION_NAME_M3IDENTITY))
                {
                    // create the command object
                    IDbCommand command = M3DBUtils.createCommand(conn, M3IdentityDbConstants.SP_GET_M3_USER_BY_ID, CommandType.StoredProcedure);

                    // configure the parameters
                    M3DBUtils.addParameter(command, M3IdentityDbConstants.PARAM_SYSTEM_USER_ID, systemUserId);

                    // open the connection
                    conn.Open();

                    // Create the data adapter and execute the query
                    IDbDataAdapter dataAdapter = M3DBUtils.getDbDataAdapter(M3IdentityDbConstants.DB_CONNECTION_NAME_M3IDENTITY);
                    dataAdapter.SelectCommand = command;
                    dataAdapter.Fill(dataSet);

                }

                // Marshal the dataset to the domain object
                if (dataSet.Tables[0].Rows.Count > 0)
                {
                    userBasicInfo = (from dataRow in dataSet.Tables[0].AsEnumerable()
                                     select new LocalUserClaimsInfo
                                     {
                                         Id = Convert.ToString(dataRow.Field<int>(M3IdentityDbConstants.FIELD_MASTER_USER_ID)),
                                         UserName = dataRow.Field<string>(M3IdentityDbConstants.FIELD_USER_NAME_IDENTIFIER).Trim()
                                     }).FirstOrDefault<LocalUserClaimsInfo>();
                }
            }
            catch (Exception ex) 
            {
                logger.error("M3UserDataIntegrationService.FindUserBasicInfoById", string.Format("An exception occurred trying to find the user. Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3UserDataIntegrationService:0004", "Unable to retrieve User Id.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }

            // Default ContextIds
            // TODO : Determine default ContextId
            userBasicInfo.ContextIds = userBasicInfo.ContextIds != null ? userBasicInfo.ContextIds : new string[0];

            // return the result
            return userBasicInfo;

        }


        public static string FindUserRolesById(string userId)
        {
            // declare the string array
            string[] roleList = new string[] { } ;

            // declare the dataset
            DataSet dataSet = new DataSet();

            // convert the string to an int value
            int systemUserId = 0;

            try
            {
                // perform the conversion
                systemUserId = Convert.ToInt32(userId);
            }
            catch (FormatException ex)
            {
                logger.error("M3UserDataIntegrationService.FindUserRolesById", "Invalid User Id Format.");
                throw new M3Exception("M3UserDataIntegrationService:0005", "Invalid User Id.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }
            catch (OverflowException ex)
            {
                logger.error("M3UserDataIntegrationService.FindUserRolesById", "Invalid User Id.");
                throw new M3Exception("M3UserDataIntegrationService:0006", "Invalid User Id.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }

            try
            {
                //  TODO:  finish implementing the data service
                // get a connection
                using (IDbConnection conn = M3DBUtils.getDbConnection(M3IdentityDbConstants.DB_CONNECTION_NAME_M3IDENTITY))
                {
                    // create the command object
                    IDbCommand command = M3DBUtils.createCommand(conn, M3IdentityDbConstants.SP_GET_M3_USER_BY_ID, CommandType.StoredProcedure);

                    // configure the parameters
                    M3DBUtils.addParameter(command, M3IdentityDbConstants.PARAM_SYSTEM_USER_ID, systemUserId);

                    // open the connection
                    conn.Open();

                    // Create the data adapter and execute the query
                    IDbDataAdapter dataAdapter = M3DBUtils.getDbDataAdapter(M3IdentityDbConstants.DB_CONNECTION_NAME_M3IDENTITY);
                    dataAdapter.SelectCommand = command;
                    dataAdapter.Fill(dataSet);

                }
            }
            catch (Exception ex)
            {
                logger.error("M3UserDataIntegrationService.FindUserRolesById", string.Format("An exception occurred trying to find the roles. Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3UserDataIntegrationService:0007", "Unable to retrieve Roles.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }

            /** TODO
            // Marshal the dataset to the domain object
            **/
            
            // return the result
            return roleList.ToString();

        }


        /// <summary>
        /// Retrieves the user instance by the web credential user name.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public static UserCredential FindWebCredentialUserByUserName(string userName)
        {
            // declare the user object
            UserCredential webCredentialUser = null;

            try {
                // declare the dataset
                DataSet dataSet = new DataSet();

                // get a connection
                using (IDbConnection conn = M3DBUtils.getDbConnection(M3IdentityDbConstants.DB_CONNECTION_NAME_M3IDENTITY))
                {
                    // create the command object
                    IDbCommand command = M3DBUtils.createCommand(conn, M3IdentityDbConstants.SP_GET_WEB_CREDENTIAL_USER_BY_USERNAME, CommandType.StoredProcedure); 

                    // configure the parameters
                    // userId >
                    M3DBUtils.addParameter(command, M3IdentityDbConstants.PARAM_USER_NAME_IDENTIFIER, userName);

                    // open the connection
                    conn.Open();

                    // Create the data adapter and execute the query
                    IDbDataAdapter dataAdapter = M3DBUtils.getDbDataAdapter(M3IdentityDbConstants.DB_CONNECTION_NAME_M3IDENTITY);
                    dataAdapter.SelectCommand = command;
                    dataAdapter.Fill(dataSet);

                }

                // holds the IsActive flag
            
                // Marshal the dataset to the domain object
                if (dataSet.Tables[0].Rows.Count > 0)
                {
                    webCredentialUser = (from dataRow in dataSet.Tables[0].AsEnumerable()
                              select new UserCredential
                              {
                                Id = Convert.ToString(dataRow.Field<int>(M3IdentityDbConstants.FIELD_MASTER_USER_ID)),
                                UserName = dataRow.Field<string>(M3IdentityDbConstants.FIELD_USER_NAME_IDENTIFIER).Trim(),
                                Active = (dataRow.Field<string>(M3IdentityDbConstants.FIELD_IS_ACTIVE_FLAG).Equals(M3IdentityDbConstants.FLAG_ENABLED,StringComparison.OrdinalIgnoreCase)),
                                PasswordHash = dataRow.Field<string>(M3IdentityDbConstants.FIELD_WEB_CREDENTIAL_HASH).Trim()

                              }).FirstOrDefault<UserCredential>();

                }
                else
                {
                    // no result found so fail
                    logger.error("M3UserDataIntegrationService.FindWebCredentialUserByUserName", "An exception occurred trying to find the credential.");
                    throw new M3Exception("M3UserDataIntegrationService:0008", "Credential not found.", M3ServiceResultFailCodes.M3FailRequestedDataNotFound);
                }
            }
            catch (Exception ex)
            {
                logger.error("M3UserDataIntegrationService.FindWebCredentialUserByUserName", string.Format("An exception occurred trying to find the credential. Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3UserDataIntegrationService:0009", "Unable to retrieve Credentials.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }
            
            // return the result
            return webCredentialUser;

        }


        public static bool UpdateWebCredentials(string userId, string webCredentialHash)
        {
            try
            {
                // declare the dataset
                DataSet dataSet = new DataSet();

                // get a connection
                using (IDbConnection conn = M3DBUtils.getDbConnection(M3IdentityDbConstants.DB_CONNECTION_NAME_M3IDENTITY))
                {
                    // create the command object
                    IDbCommand command = M3DBUtils.createCommand(conn, M3IdentityDbConstants.SP_UPDATE_WEB_CREDENTIAL, CommandType.StoredProcedure);

                    // configure the parameters
                    M3DBUtils.addParameter(command, M3IdentityDbConstants.PARAM_SYSTEM_USER_ID, Convert.ToInt32(userId));
                    M3DBUtils.addParameter(command, M3IdentityDbConstants.PARAM_WEB_CREDENTIAL_HASH, webCredentialHash);

                    // open the connection
                    conn.Open();

                    // execute the proc
                    int numberOfRows = command.ExecuteNonQuery();

                    // check the retval
                    if (numberOfRows == 0)
                    {
                        // something wrong
                        logger.error("M3UserDataIntegrationService.UpdateWebCredentials", "No records were updated.");
                        throw new M3Exception("M3UserDataIntegrationService:0011", "Unable to update Web Credentials.", M3ServiceResultFailCodes.M3FailParameterInvalid);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.error("M3UserDataIntegrationService.UpdateWebCredentials", string.Format("An exception occurred trying to update the credential. Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3UserDataIntegrationService:0012", "Unable to update Web Credentials.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }

            return true;
        }

    }
}
