﻿using Collaborative.M3.ServerSdk.Core.Business.Common.Exception;
using Collaborative.M3.ServerSdk.Core.Business.Common.Messaging;
using Collaborative.M3.ServerSdk.Core.Business.Integration.Database.Utilities;
using Collaborative.M3.ServerSdk.Core.Integration;
using Collaborative.M3.ServerSdk.Core.Logging;
using Collaborative.M3.ServerSdk.Identity.Common.User;
using Collaborative.M3.ServerSdk.Identity.Domain;
using Collaborative.M3.ServerSdk.Identity.Integration.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Identity.Integration
{
    public class M3OAuthDataIntegrationService : IM3IntegrationService
    {

        private static readonly M3Logger logger = M3Logger.getLogger(MethodBase.GetCurrentMethod().DeclaringType.Name);

        // TODO: Overlaps with M3DeviceDataIntegrationService
        /// <summary>
        /// Inserts a new user into the physical data store as a registerd web user.
        /// </summary>
        /// <param name="webRegistrationInfo"></param>
        /// <returns></returns>
        public static void CreateOAuthRegistration(DeviceTokenInfo deviceTokenInfo)
        {

            // get the userid from the currently logged in user
            int userId = 0;

            try
            {
                userId = Convert.ToInt32( M3User.GetM3UserFromCurrentContext().Id);
            }
            catch (FormatException ex)
            {
                logger.error("M3OAuthDataIntegrationService.CreateOAuthRegistration", "Invalid User Id Format.");
                throw new M3Exception("M3OAuthDataIntegrationService:0001", "Invalid User Id.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }
            catch (OverflowException ex)
            {
                logger.error("M3OAuthDataIntegrationService.CreateOAuthRegistration", "Invalid User Id.");
                throw new M3Exception("M3OAuthDataIntegrationService:0002", "Invalid User Id.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }

            try
            {
                // declare the dataset
                DataSet dataSet = new DataSet();

                // get a connection
                using (IDbConnection conn = M3DBUtils.getDbConnection(M3IdentityDbConstants.DB_CONNECTION_NAME_M3IDENTITY))
                {
                    // create the command object
                    IDbCommand command = M3DBUtils.createCommand(conn, M3IdentityDbConstants.SP_ADD_DEVICE_APP_REGISTRATION, CommandType.StoredProcedure);

                    // configure the parameters
                    M3DBUtils.addParameter(command, M3IdentityDbConstants.PARAM_SYSTEM_USER_ID, userId);
                    M3DBUtils.addParameter(command, M3IdentityDbConstants.PARAM_DEVICE_INSTANCE_TAG, deviceTokenInfo.DeviceAppRegistrationTag);
                    M3DBUtils.addParameter(command, M3IdentityDbConstants.PARAM_DEVICE_TOKEN, deviceTokenInfo.RefreshToken);

                    // open the connection
                    conn.Open();

                    // execute the proc
                    int numberOfRows = command.ExecuteNonQuery();

                    // check the retval
                    if (numberOfRows == 0)
                    {
                        // something wrong
                        logger.error("M3OAuthDataIntegrationService.CreateOAuthRegistration", "No records where created.");
                        throw new M3Exception("M3OAuthDataIntegrationService:0003", "No records where created.", M3ServiceResultFailCodes.M3FailParameterInvalid);
                    }
                }
            }
            catch(Exception ex)
            {
                logger.error("M3OAuthDataIntegrationService.CreateOAuthRegistration", string.Format("An error occurred creating OAuth Registration.  Message: {0}.", ex.GetBaseException().Message));
                throw new M3Exception("M3OAuthDataIntegrationService:0004", "An error occurred.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }
        }

        /// <summary>
        /// Deletes all registrations associated with a given user and device tag.
        /// </summary>
        /// <param name="deviceAppInstanceTag"></param>
        /// <returns></returns>
        public static void DeleteDeviceRegistrationByIdAndTag(string deviceAppInstanceTag)
        {
            // get the userid from the currently logged in user
            int userIdint = 0;

            try
            {
                userIdint = Convert.ToInt32(M3User.GetM3UserFromCurrentContext().Id);
            }
            catch (FormatException ex)
            {
                logger.error("M3OAuthDataIntegrationService.DeleteDeviceRegistrationByIdAndTag", "Invalid User Id Format.");
                throw new M3Exception("M3OAuthDataIntegrationService:0005", "Invalid User Id.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }
            catch (OverflowException ex)
            {
                logger.error("M3OAuthDataIntegrationService.DeleteDeviceRegistrationByIdAndTag", "Invalid User Id.");
                throw new M3Exception("M3OAuthDataIntegrationService:0006", "Invalid User Id.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }

            try
            {
                // declare the dataset
                DataSet dataSet = new DataSet();

                // get a connection
                using (IDbConnection conn = M3DBUtils.getDbConnection(M3IdentityDbConstants.DB_CONNECTION_NAME_M3IDENTITY))
                {
                    // create the command object
                    IDbCommand command = M3DBUtils.createCommand(conn, M3IdentityDbConstants.SP_DELETE_DEVICE_APP_REGISTRATION, CommandType.StoredProcedure);

                    // configure the parameters
                    M3DBUtils.addParameter(command, M3IdentityDbConstants.PARAM_SYSTEM_USER_ID, userIdint);
                    M3DBUtils.addParameter(command, M3IdentityDbConstants.PARAM_DEVICE_INSTANCE_TAG, deviceAppInstanceTag);

                    // open the connection
                    conn.Open();

                    // execute the proc
                    int numberOfRows = command.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                logger.error("M3OAuthDataIntegrationService.DeleteDeviceRegistrationByIdAndTag", string.Format("An error occurred deleting the device registration.  Message: {0}.", ex.GetBaseException().Message));
                throw new M3Exception("M3OAuthDataIntegrationService:0007", "An error occurred.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }
            
        }

        /// <summary>
        /// Performs the retrieval from the database of the full token info instance corresponding to the supplied token.
        /// </summary>
        /// <param name="userId">The internal unique identifier for this user.</param>
        /// <returns></returns>
        public static DeviceTokenInfo FindDeviceTokenInfoByToken(string targetToken)
        {
            // declare the token info object
            DeviceTokenInfo deviceTokenInfo = null;

            try
            {

                // declare the dataset
                DataSet dataSet = new DataSet();

                // get a connection
                using (IDbConnection conn = M3DBUtils.getDbConnection(M3IdentityDbConstants.DB_CONNECTION_NAME_M3IDENTITY))
                {
                    // create the command object
                    IDbCommand command = M3DBUtils.createCommand(conn, M3IdentityDbConstants.SP_GET_DEVICE_APP_REGISTRATION_BY_TOKEN, CommandType.StoredProcedure);

                    // configure the parameters
                    M3DBUtils.addParameter(command, M3IdentityDbConstants.PARAM_DEVICE_TOKEN, targetToken);

                    // open the connection
                    conn.Open();

                    // Create the data adapter and execute the query
                    IDbDataAdapter dataAdapter = M3DBUtils.getDbDataAdapter(M3IdentityDbConstants.DB_CONNECTION_NAME_M3IDENTITY);
                    dataAdapter.SelectCommand = command;
                    dataAdapter.Fill(dataSet);

                }

                // Marshal the dataset to the domain object
                if (dataSet.Tables[0].Rows.Count > 0)
                {
                    deviceTokenInfo = (from dataRow in dataSet.Tables[0].AsEnumerable()
                                       select new DeviceTokenInfo
                                       {
                                           DeviceAppRegistrationTag = dataRow.Field<string>(M3IdentityDbConstants.FIELD_DEVICE_INSTANCE_TAG).Trim(),
                                           RefreshToken = dataRow.Field<string>(M3IdentityDbConstants.FIELD_DEVICE_REGISTRATION_TOKEN).Trim()
                                       }).FirstOrDefault<DeviceTokenInfo>();

                }
                else
                {
                    // no token found so set the fail code
                    logger.error("M3OAuthDataIntegrationService.FindDeviceTokenInfoByToken", "Device Token not found.");
                    throw new M3Exception("M3OAuthDataIntegrationService:0009", "Device Token not found.", M3ServiceResultFailCodes.M3FailRequestedDataNotFound);
                }
            }
            catch (Exception ex)
            {
                logger.error("M3OAuthDataIntegrationService.FindDeviceTokenInfoByToken", string.Format("An error occurred getting the device token info.  Message: {0}.", ex.GetBaseException().Message));
                throw new M3Exception("M3OAuthDataIntegrationService:0009", "An error occurred.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }

            // return the result
            return deviceTokenInfo;

        }

    }
}
