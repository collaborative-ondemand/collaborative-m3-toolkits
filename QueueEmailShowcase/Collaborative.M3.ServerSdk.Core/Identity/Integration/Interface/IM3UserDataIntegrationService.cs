﻿using Collaborative.M3.ServerSdk.Core.Identity.Domain.EF;
using Collaborative.M3.ServerSdk.Identity.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Identity.Integration.Interface
{
    public interface IM3UserDataIntegrationService
    {
        bool CreateUserCredential(UserRegistrationInfo userRegistrationInfo, string hashedPassword);
        LocalUserClaimsInfo FindUserBasicInfoById(string userId);
        string FindUserRolesById(string userId);
        UserCredential FindUserCredentialByUserName(string userName);
        bool UpdateUserCredential(string userId, string userCredentialHash);
        bool UpdateUserCredentialByName(string userName, string userCredentialHash);

        IList<UserMasterInfo> GetUserMastersByName(string name);
        UserMasterInfo GetUserMasterInfoByName(string name);
        UserMasterInfo GetUserMasterInfoById(int id);
        bool SetUserRoles(int userId, int roleId, bool autoCommit = false);
        bool SetUserRoles(string userName, int roleId, bool autoCommit = false);
        RoleInfo[] GetAllRoles();
    }
}
