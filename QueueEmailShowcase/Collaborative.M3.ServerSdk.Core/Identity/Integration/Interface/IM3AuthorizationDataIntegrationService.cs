﻿using Collaborative.M3.ServerSdk.Identity.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Identity.Integration.Interface
{
    public interface IM3AuthorizationDataIntegrationService
    {
        RoleEntitlementItem[] GetAllEntitlementsAllRoles();
        RoleEntitlementItem[] LoadAllEntitlementsAllRoles();
        RoleInfoBasic[] GetRolesForUserById(string userId);
    }
}
