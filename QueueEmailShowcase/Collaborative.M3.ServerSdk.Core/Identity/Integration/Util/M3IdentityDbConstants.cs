﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Identity.Integration.Util
{
    public class M3IdentityDbConstants
    {
        public const string FLAG_ENABLED = "Y";
        public const string FLAG_DISABLED = "N";

        /// <summary>
        /// Constants defining the name of the DB connections to be configured in web.config or other configuration sources.
        /// </summary>
        public const string DB_CONNECTION_NAME_M3IDENTITY = "m3showcasedb";

        /// <summary>
        /// Stored procedure names
        /// </summary>
        public const string SP_GET_WEB_USER_BY_USERNAME = "getWebUserByUsername";
        public const string SP_GET_WEB_CREDENTIAL_USER_BY_USERNAME = "getWebCredenitalUserByUsername";
        public const string SP_GET_M3_USER_BY_ID = "getM3UserById";
        public const string SP_CREATE_WEB_USER = "createWebUser";
        public const string SP_ADD_ROLE_TO_USER = "addRoleToUser";
        public const string SP_GET_ROLE_ENTITLEMENT_MATRIX = "getRoleEntitlementMatrix";
        public const string SP_GET_ROLES_FOR_USER = "getRolesForUser";
        public const string SP_UPDATE_WEB_CREDENTIAL = "updateWebCredential";
        public const string SP_GET_DEVICE_APP_REGISTRATION_BY_TOKEN = "getDeviceAppRegistrationByToken";
        public const string SP_ADD_DEVICE_APP_REGISTRATION = "addDeviceAppRegistration";
        public const string SP_DELETE_DEVICE_APP_REGISTRATION = "deleteDeviceAppRegistrationsForUserDeviceTag";

        /// <summary>
        /// Parameter constants
        /// </summary>
        public const string PARAM_SYSTEM_USER_ID = "@userId";
        public const string PARAM_USER_NAME_IDENTIFIER = "@userNameIdentifier";
        public const string PARAM_IS_ACTIVE = "@isActive";
        public const string PARAM_WEB_CREDENTIAL_ID = "@webCredentialId";
        public const string PARAM_WEB_CREDENTIAL_HASH = "@webCredentialHash";
        public const string PARAM_DEVICE_TOKEN = "@deviceRegistrationToken";
        public const string PARAM_DEVICE_INSTANCE_TAG = "@deviceInstanceTag";
        public const string PARAM_ROLE_ID = "@roleId";

        /// <summary>
        /// field name constants
        /// </summary>
        public const string FIELD_MASTER_USER_ID = "userId";
        public const string FIELD_USER_NAME_IDENTIFIER = "userNameIdentifier";
        public const string FIELD_IS_ACTIVE_FLAG = "isActive";
        public const string FIELD_WEB_CREDENTIAL_ID = "webCredentialId";
        public const string FIELD_WEB_CREDENTIAL_HASH = "webCredentialHash";
        public const string FIELD_ROLE_ENTITLEMENT_ID = "roleEntitlementId";
        public const string FIELD_ROLE_ID = "roleId";
        public const string FIELD_ROLE_KEY = "roleKey";
        public const string FIELD_ROLE_DISPLAY_NAME = "roleDisplayName";
        public const string FIELD_ROLE_DESCRIPTION = "roleDescription";
        public const string FIELD_ENTITLEMENT_ID = "entitlementId";
        public const string FIELD_ENTITLEMENT_KEY = "entitlementKey"; 
        public const string FIELD_ENTITLEMENT_DISPLAY_NAME = "entitlementDisplayName"; 
        public const string FIELD_ENTITLEMENT_DESCRIPTION = "entitlementDescription";
        public const string FIELD_DEVICE_INSTANCE_TAG = "deviceInstanceTag";
        public const string FIELD_DEVICE_REGISTRATION_TOKEN = "deviceRegistrationToken";



        
    }
}
