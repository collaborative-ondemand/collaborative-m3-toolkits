﻿using Collaborative.M3.ServerSdk.Core.Identity.Domain;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Identity.Integration
{
    public class ADOIdentityIntegrationService<TKey> : IUserStore<User<TKey>, TKey>, IUserPasswordStore<User<TKey>, TKey>
        where TKey : IEquatable<TKey>
    {
        public async Task CreateAsync(User<TKey> user)
        {
            await Task.Run(() => null);
        }

        public Task DeleteAsync(User<TKey> user)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Task<User<TKey>> FindByIdAsync(TKey userId)
        {
            throw new NotImplementedException();
        }

        public Task<User<TKey>> FindByNameAsync(string userName)
        {   
            var user = new User<TKey>((TKey)Convert.ChangeType(1, typeof(TKey))) { UserName = "jcotillo" };
            return Task.FromResult(user);
        }

        public async Task<string> GetPasswordHashAsync(User<TKey> user)
        {
            return await Task.Run(() => { return ""; });
        }

        public async Task<bool> HasPasswordAsync(User<TKey> user)
        {
            return await Task.Run(() => { return true; });
        }

        public async Task SetPasswordHashAsync(User<TKey> user, string passwordHash)
        {
            await Task.Run(() =>  null );
        }

        public async Task UpdateAsync(User<TKey> user)
        {
            await Task.Run(() => null);
        }
    }
}
