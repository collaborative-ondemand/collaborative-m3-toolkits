﻿using Collaborative.M3.ServerSdk.Core.Business.Common.Exception;
using Collaborative.M3.ServerSdk.Core.Business.Common.Messaging;
using Collaborative.M3.ServerSdk.Core.Business.Integration.Database.Utilities;
using Collaborative.M3.ServerSdk.Core.Cache.Integration;
using Collaborative.M3.ServerSdk.Core.Identity.Integration.Interface;
using Collaborative.M3.ServerSdk.Core.Integration;
using Collaborative.M3.ServerSdk.Core.Logging;
using Collaborative.M3.ServerSdk.Identity.Domain;
using Collaborative.M3.ServerSdk.Identity.Integration.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Identity.Integration.Implementation.ADO
{
    public class M3AuthorizationDataIntegrationService : IM3IntegrationService, IM3AuthorizationDataIntegrationService
    {
        private static readonly M3Logger logger = M3Logger.getLogger(MethodBase.GetCurrentMethod().DeclaringType.Name);

        /// <summary>
        /// Loads the matrix of roles and their entitlements as a flat, tabular response.
        /// This will always check the cache prior to checking the datasource.
        /// </summary>
        /// <returns></returns>
        public RoleEntitlementItem[] GetAllEntitlementsAllRoles()
        {
            RoleEntitlementItem[] roleEntitlementItems = null;
            try
            {
                roleEntitlementItems = M3CacheIntegrationService.Get<RoleEntitlementItem[]>("Authorization", "Entitlements");
                if (roleEntitlementItems[0] == null)
                {
                    return LoadAllEntitlementsAllRoles();
                }
            }
            catch (Exception ex)
            {
                logger.error("M3AuthorizationDataIntegrationService.GetAllEntitlementsAllRoles", string.Format("An error occurred getting the roles.  Message: {0}.", ex.GetBaseException().Message));
                throw new M3Exception("M3AuthorizationDataIntegrationService:0001", "An error occurred.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }
            return roleEntitlementItems;

        }


        /// <summary>
        /// Loads the matrix of roles and their entitlements as a flat, tabular response.
        /// </summary>
        /// <returns></returns>
        public RoleEntitlementItem[] LoadAllEntitlementsAllRoles()
        {
            // declare the item object
            RoleEntitlementItem[] roleEntitlementItems = null;

            try
            {
                // declare the dataset
                DataSet dataSet = new DataSet();

                // get a connection
                using (IDbConnection conn = M3DBUtils.getDbConnection(M3IdentityDbConstants.DB_CONNECTION_NAME_M3IDENTITY))
                {
                    // create the command object
                    IDbCommand command = M3DBUtils.createCommand(conn, M3IdentityDbConstants.SP_GET_ROLE_ENTITLEMENT_MATRIX, CommandType.StoredProcedure);

                    // configure the parameters
                    // if it is oracle, we have to add a default return cursor, pass parameters as null
                    M3DBUtils.addParameter(command, parameterName: null, parameterValue: null, isSelectStatement: true);

                    // open the connection
                    conn.Open();

                    // Create the data adapter and execute the query
                    IDbDataAdapter dataAdapter = M3DBUtils.getDbDataAdapter(M3IdentityDbConstants.DB_CONNECTION_NAME_M3IDENTITY);
                    dataAdapter.SelectCommand = command;
                    dataAdapter.Fill(dataSet);

                }

                // holds the Active flag

                // Marshal the dataset to the domain object
                if (dataSet.Tables[0].Rows.Count > 0)
                {

                    roleEntitlementItems = (from dataRow in dataSet.Tables[0].AsEnumerable()
                                            select new RoleEntitlementItem
                                            {
                                                RoleEntitlementId = (string)Convert.ChangeType(dataRow[M3IdentityDbConstants.FIELD_ROLE_ENTITLEMENT_ID], typeof(string)),//Convert.ToString(dataRow.Field<long>(M3IdentityDbConstants.FIELD_ROLE_ENTITLEMENT_ID)),
                                                RoleKey = dataRow.Field<string>(M3IdentityDbConstants.FIELD_ROLE_KEY).Trim(),
                                                RoleDisplayName = dataRow.Field<string>(M3IdentityDbConstants.FIELD_ROLE_DISPLAY_NAME).Trim(),
                                                RoleDescription = dataRow.Field<string>(M3IdentityDbConstants.FIELD_ROLE_DESCRIPTION).Trim(),
                                                EntitlementId = (string)Convert.ChangeType(dataRow[M3IdentityDbConstants.FIELD_ENTITLEMENT_ID], typeof(string)),//Convert.ToString(dataRow.Field<long>(M3IdentityDbConstants.FIELD_ENTITLEMENT_ID)),
                                                EntitlementKey = dataRow.Field<string>(M3IdentityDbConstants.FIELD_ENTITLEMENT_KEY).Trim(),
                                                EntitlementDisplayName = dataRow.Field<string>(M3IdentityDbConstants.FIELD_ENTITLEMENT_DISPLAY_NAME).Trim(),
                                                EntiltementDescription = dataRow.Field<string>(M3IdentityDbConstants.FIELD_ENTITLEMENT_DESCRIPTION).Trim()

                                            }).ToArray<RoleEntitlementItem>();
                }
                else
                {
                    // no result found so fail
                    logger.error("M3AuthorizationDataIntegrationService.LoadAllEntitlementsAllRoles", "No rows returned.");
                    throw new M3Exception("M3AuthorizationDataIntegrationService:0002", "No roles returned.", M3ServiceResultFailCodes.M3FailRequestedDataNotFound);
                }
            }
            catch (Exception ex)
            {
                logger.error("M3AuthorizationDataIntegrationService.LoadAllEntitlementsAllRoles", string.Format("An error occurred loading the roles.  Message: {0}.", ex.GetBaseException().Message));
                throw new M3Exception("M3AuthorizationDataIntegrationService:0003", "An error occurred.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }

            // return the result
            return roleEntitlementItems;

        }


        public RoleInfoBasic[] GetRolesForUserById(string userId)
        {

            // declare the item object
            RoleInfoBasic[] roleInfoList = null;

            // declare the dataset
            DataSet dataSet = new DataSet();

            // convert the string to an int value
            int systemUserId = 0;

            try
            {
                // perform the conversion
                systemUserId = Convert.ToInt32(userId);
            }
            catch (FormatException ex)
            {
                logger.error("M3AuthorizationDataIntegrationService.GetRolesForUserById", "Invalid User Id Format.");
                throw new M3Exception("M3AuthorizationDataIntegrationService:0004", "Invalid User Id.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }
            catch (OverflowException ex)
            {
                logger.error("M3AuthorizationDataIntegrationService.GetRolesForUserById", "Invalid User Id.");
                throw new M3Exception("M3AuthorizationDataIntegrationService:0005", "Invalid User Id.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }

            try
            {
                // get a connection
                using (IDbConnection conn = M3DBUtils.getDbConnection(M3IdentityDbConstants.DB_CONNECTION_NAME_M3IDENTITY))
                {
                    // create the command object
                    IDbCommand command = M3DBUtils.createCommand(conn, M3IdentityDbConstants.SP_GET_ROLES_FOR_USER, CommandType.StoredProcedure);

                    // add to parameters
                    M3DBUtils.addParameter(command, M3IdentityDbConstants.PARAM_SYSTEM_USER_ID, systemUserId, isSelectStatement: true);

                    // open the connection
                    conn.Open();

                    // Create the data adapter and execute the query
                    IDbDataAdapter dataAdapter = M3DBUtils.getDbDataAdapter(M3IdentityDbConstants.DB_CONNECTION_NAME_M3IDENTITY);
                    dataAdapter.SelectCommand = command;
                    dataAdapter.Fill(dataSet);

                }

                // holds the Active flag

                // Marshal the dataset to the domain object
                if (dataSet.Tables[0].Rows.Count > 0)
                {

                    roleInfoList = (from dataRow in dataSet.Tables[0].AsEnumerable()
                                    select new RoleInfoBasic
                                    {
                                        RoleId = (string)Convert.ChangeType(dataRow[M3IdentityDbConstants.FIELD_ROLE_ID], typeof(string)),//Convert.ToString(dataRow.Field<long>(M3IdentityDbConstants.FIELD_ROLE_ID)),
                                        RoleKey = dataRow.Field<string>(M3IdentityDbConstants.FIELD_ROLE_KEY).Trim()

                                    }).ToArray<RoleInfoBasic>();
                }
                else
                {
                    // no result found so fail
                    logger.error("M3AuthorizationDataIntegrationService.GetRolesForUserById", "No rows returned.");
                    throw new M3Exception("M3AuthorizationDataIntegrationService:0006", "No rows returned.", M3ServiceResultFailCodes.M3FailParameterInvalid);
                }
            }
            catch (Exception ex)
            {
                logger.error("M3AuthorizationDataIntegrationService.GetRolesForUserById", string.Format("An error occurred loading the roles.  Message: {0}.", ex.GetBaseException().Message));
                throw new M3Exception("M3AuthorizationDataIntegrationService:0007", "An error occurred.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }

            // return the result
            return roleInfoList;

        }
    }
}
