﻿using Collaborative.M3.ServerSdk.Core.Identity.Integration.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Collaborative.M3.ServerSdk.Identity.Domain;
using Collaborative.M3.ServerSdk.Core.Integration.EF.Interface;
using Collaborative.M3.ServerSdk.Core.Integration.EF.Implementation;
using Collaborative.M3.ServerSdk.Identity.Integration.Util;
using Collaborative.M3.ServerSdk.Core.Identity.Domain.EF;
using Collaborative.M3.ServerSdk.Core.Logging;
using System.Reflection;
using Collaborative.M3.ServerSdk.Core.Business.Common.Messaging;
using Collaborative.M3.ServerSdk.Core.Business.Common.Exception;
using Collaborative.M3.ServerSdk.Identity.Common.User;
using Collaborative.M3.ServerSdk.Core.Common.IoC;
using SimpleInjector;

namespace Collaborative.M3.ServerSdk.Core.Identity.Integration.Implementation.EF
{
    public class M3UserDataIntegrationService : IM3UserDataIntegrationService
    {
        private static readonly M3Logger logger = M3Logger.getLogger("M3UserDataIntegrationService");
        public bool CreateUserCredential(UserRegistrationInfo userRegistrationInfo, string hashedPassword)
        {
            try
            {
                using (M3ContainerManager.Container.BeginLifetimeScope())
                {
                    IRepository<IdnWebCredential> idnWebCredentialRepository =
                        M3ContainerManager.Container.GetInstance<IRepository<IdnWebCredential>>();

                    IdnWebCredential idnWebCredential = new IdnWebCredential()
                    {
                        IdnUserMaster = new IdnUserMaster()
                        {
                            UserNameIdentifier = userRegistrationInfo.UserName,
                            Active = true
                        },
                        WebCredentialHash = hashedPassword
                    };

                    idnWebCredentialRepository.Insert(idnWebCredential, autoCommit: true);
                }
            }
            catch (Exception ex)
            {
                logger.error("M3UserDataIntegrationService.CreateWebCredentialUser", string.Format("An exception occurred trying to create the user. Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3UserDataIntegrationService:0001", "Unable to Create Web Credential User.", M3ServiceResultFailCodes.M3FailUnknown, ex);
            }
            return true;
        }

        public LocalUserClaimsInfo FindUserBasicInfoById(string userId)
        {
            // declare the user object
            LocalUserClaimsInfo userBasicInfo = null;

            // convert the string to an int value
            int systemUserId = 0;

            try
            {
                if (!int.TryParse(userId, out systemUserId))
                {
                    logger.error("M3UserDataIntegrationService.FindUserBasicInfoById", "Invalid User Id Format.");
                    throw new M3Exception("M3UserDataIntegrationService:0002", "Invalid User Id.");
                }

                using (M3ContainerManager.Container.BeginLifetimeScope())
                {
                    IRepository<IdnUserMaster> idnUserMasterRepository =
                        M3ContainerManager.Container.GetInstance<IRepository<IdnUserMaster>>();

                    //Using projections to avoid retrieving all the other fields that are not required.
                    var idnUserMasterAnonymous = (from idnUserMasterTable in idnUserMasterRepository.Table
                                                  where idnUserMasterTable.Id == systemUserId
                                                  select new
                                                  {
                                                      Id = idnUserMasterTable.Id,
                                                      UserName = idnUserMasterTable.UserNameIdentifier
                                                  }).FirstOrDefault();


                    if (idnUserMasterAnonymous == null)
                    {
                        logger.error("M3UserDataIntegrationService.FindUserBasicInfoById", string.Format("User not found. Message: User not found in the system with the following Id: {0}", systemUserId));
                        throw new M3Exception("M3UserDataIntegrationService:0004", string.Format("User not found. Message: User not found in the system with the following Id: {0}", systemUserId));
                    }

                    userBasicInfo = new LocalUserClaimsInfo()
                    {
                        Id = idnUserMasterAnonymous.Id.ToString(),
                        UserName = idnUserMasterAnonymous.UserName
                    };
                }
            }
            catch (Exception ex)
            {
                logger.error("M3UserDataIntegrationService.FindUserBasicInfoById", string.Format("An exception occurred trying to find the user. Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3UserDataIntegrationService:0004", "Unable to retrieve User Id.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }

            return userBasicInfo;
        }

        public UserCredential FindUserCredentialByUserName(string userName)
        {
            UserCredential userCredentialUser = null;

            try
            {
                using (M3ContainerManager.Container.BeginLifetimeScope())
                {
                    IRepository<IdnUserMaster> idnUserMasterRepository =
                        M3ContainerManager.Container.GetInstance<IRepository<IdnUserMaster>>();

                    IRepository<IdnWebCredential> idnWebCredentialRepository =
                        M3ContainerManager.Container.GetInstance<IRepository<IdnWebCredential>>();

                    var userCredential = (from webCredential in idnWebCredentialRepository.Table
                                          join userMaster in idnUserMasterRepository.Table on webCredential.UserId equals userMaster.Id
                                          where userMaster.UserNameIdentifier.ToLower() == userName.ToLower()
                                          select new
                                          {
                                              UserId = userMaster.Id,
                                              UserName = userMaster.UserNameIdentifier,
                                              Active = userMaster.Active,
                                              PasswordHash = webCredential.WebCredentialHash
                                          }).FirstOrDefault();

                    if (userCredential == null)
                    {
                        logger.error("M3UserDataIntegrationService.FindUserBasicInfoById", string.Format("User not found. Message: User not found in the system with the following Username: {0}", userName));
                        throw new M3Exception("M3UserDataIntegrationService:0004", string.Format("User not found. Message: User not found in the system with the following Username: {0}", userName));
                    }
                }
            }
            catch (Exception ex)
            {
                logger.error("M3UserDataIntegrationService.FindWebCredentialUserByUserName", string.Format("An exception occurred trying to find the credential. Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3UserDataIntegrationService:0009", "Unable to retrieve Credentials.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }

            // return the result
            return userCredentialUser;
        }

        public string FindUserRolesById(string userId)
        {
            // convert the string to an int value
            int systemUserId = 0;

            try
            {
                if (!int.TryParse(userId, out systemUserId))
                {
                    logger.error("M3UserDataIntegrationService.FindUserBasicInfoById", "Invalid User Id Format.");
                    throw new M3Exception("M3UserDataIntegrationService:0002", "Invalid User Id.");
                }

                using (M3ContainerManager.Container.BeginLifetimeScope())
                {
                    IRepository<IdnRoleDefinition> idnRoleDefinitionRepository =
                        M3ContainerManager.Container.GetInstance<IRepository<IdnRoleDefinition>>();

                    IRepository<IdnUserRole> idnUserRoleRepository =
                        M3ContainerManager.Container.GetInstance<IRepository<IdnUserRole>>();

                    List<IdnRoleDefinition> roles = (from idnRoleDefinitionTable in idnRoleDefinitionRepository.Table
                                                     join idnUserRoleTable in idnUserRoleRepository.Table on idnRoleDefinitionTable.Id equals idnUserRoleTable.UserId
                                                     where idnUserRoleTable.UserId == systemUserId
                                                     select idnRoleDefinitionTable).ToList();
                    return String.Join(",", roles);
                }
            }
            catch (Exception ex)
            {
                logger.error("M3UserDataIntegrationService.FindUserRolesById", string.Format("An exception occurred trying to find the roles. Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3UserDataIntegrationService:0007", "Unable to retrieve Roles.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }
        }

        public bool UpdateUserCredential(string userId, string userCredentialHash)
        {
            // convert the string to an int value
            int systemUserId = 0;

            try
            {
                if (!int.TryParse(userId, out systemUserId))
                {
                    logger.error("M3UserDataIntegrationService.FindUserBasicInfoById", "Invalid User Id Format.");
                    throw new M3Exception("M3UserDataIntegrationService:0002", "Invalid User Id.");
                }

                using (M3ContainerManager.Container.BeginLifetimeScope())
                {
                    IRepository<IdnWebCredential> idnWebCredentialRepository =
                        M3ContainerManager.Container.GetInstance<IRepository<IdnWebCredential>>();

                    var idnWebCredential =
                                        idnWebCredentialRepository
                                        .Table
                                        .Where(idnWebCredentialTable => idnWebCredentialTable.IdnUserMaster.Id == systemUserId)
                                        .FirstOrDefault();

                    if (idnWebCredential == null)
                    {
                        logger.error("M3UserDataIntegrationService.FindUserBasicInfoById", string.Format("User not found. Message: User not found in the system with the following Id: {0}", systemUserId));
                        throw new M3Exception("M3UserDataIntegrationService:0004", string.Format("User not found. Message: User not found in the system with the following Id: {0}", systemUserId));
                    }

                    idnWebCredential.WebCredentialHash = userCredentialHash;

                    idnWebCredentialRepository.Update(idnWebCredential, autoCommit: true);
                }
            }
            catch (Exception ex)
            {
                logger.error("M3UserDataIntegrationService.UpdateWebCredentials", string.Format("An exception occurred trying to update the credential. Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3UserDataIntegrationService:0012", "Unable to update Web Credentials.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }

            return true;
        }

        public IList<UserMasterInfo> GetUserMastersByName(string name)
        {
            try
            {
                using (M3ContainerManager.Container.BeginLifetimeScope())
                {
                    IRepository<IdnUserMaster> idnUserMasterRepository =
                        M3ContainerManager.Container.GetInstance<IRepository<IdnUserMaster>>();

                    IList<UserMasterInfo> userMasterList = (from idnUserMaster in idnUserMasterRepository.Table
                                                            where idnUserMaster.UserNameIdentifier.ToLower().Contains(name.ToLower())
                                                            select new UserMasterInfo()
                                                            {
                                                                UserId = idnUserMaster.Id,
                                                                UserName = idnUserMaster.UserNameIdentifier
                                                            }).ToList();
                    return userMasterList;
                }
            }
            catch (Exception ex)
            {
                logger.error("M3UserDataIntegrationService.GetUserMasterByName", string.Format("An exception occurred trying to create the user. Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3UserDataIntegrationService:0001", "Unable to Create Web Credential User.", M3ServiceResultFailCodes.M3FailUnknown, ex);
            }
        }

        public bool SetUserRoles(int userId, int roleId, bool autoCommit = false)
        {
            try
            {
                using (M3ContainerManager.Container.BeginLifetimeScope())
                {
                    IRepository<IdnUserRole> idnUserRoleRepository =
                        M3ContainerManager.Container.GetInstance<IRepository<IdnUserRole>>();

                    M3User m3User = M3User.GetM3UserFromCurrentContext();

                    IdnUserRole role = new IdnUserRole
                    {
                        UserId = userId,
                        RoleId = roleId,
                        CreatedBy = m3User.UserName,
                        CreatedDate = System.DateTime.Now,
                        ModifiedBy = m3User.UserName,
                        ModifiedDate = System.DateTime.Now
                    };

                    idnUserRoleRepository.Insert(role, autoCommit);
                }
            }
            catch (Exception ex)
            {
                logger.error("M3UserDataIntegrationService.SetUserRoles", string.Format("An exception occurred trying to update the credential. Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3UserDataIntegrationService:0012", "Unable to update Web Credentials.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }

            return true;
        }

        public RoleInfo[] GetAllRoles()
        {
            RoleInfo[] roleList = null;
            List<RoleInfo> roleInfoList = new List<RoleInfo>();

            try
            {
                using (M3ContainerManager.Container.BeginLifetimeScope())
                {
                    IRepository<IdnRoleDefinition> idnRoleDefinitionRepository =
                        M3ContainerManager.Container.GetInstance<IRepository<IdnRoleDefinition>>();

                    var roles = (from idnRoleDefinitionTable in idnRoleDefinitionRepository.Table
                                 select new
                                 {
                                     RoleId = idnRoleDefinitionTable.Id,
                                     RoleKey = idnRoleDefinitionTable.RoleKey,
                                     RoleDisplayName = idnRoleDefinitionTable.RoleDisplayName,
                                     RoleDescription = idnRoleDefinitionTable.RoleDescription
                                 }).ToList();

                    if (roles == null)
                    {
                        // no result found so fail
                        logger.error("M3IdentityService.GetAllRoles", "No rows returned.");
                        throw new M3Exception("M3IdentityService:0006", "No rows returned.", M3ServiceResultFailCodes.M3FailParameterInvalid);
                    }

                    roles.ForEach(roleInfo =>
                    roleInfoList.Add(new RoleInfo()
                    {
                        RoleId = roleInfo.RoleId.ToString(),
                        RoleKey = roleInfo.RoleKey,
                        RoleDisplayName = roleInfo.RoleDisplayName,
                        RoleDescription = roleInfo.RoleDescription
                    }));
                }
                roleList = roleInfoList.ToArray();
            }
            catch (Exception ex)
            {
                logger.error("M3IdentityService.GetAllRoles", string.Format("An error occurred loading the roles.  Message: {0}.", ex.GetBaseException().Message));
                throw new M3Exception("M3IdentityService:0007", "An error occurred.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }

            return roleList;
        }
        
        public bool RemoveUserRole(int userId, int roleId, bool? active = true, bool autoCommit = false)
        {
            try
            {
                using (M3ContainerManager.Container.BeginLifetimeScope())
                {
                    IRepository<IdnUserRole> idnUserRoleRepository =
                        M3ContainerManager.Container.GetInstance<IRepository<IdnUserRole>>();

                    idnUserRoleRepository.GetById(userId);

                    var idnUserRole = idnUserRoleRepository
                                        .Table
                                        .Where(idnUserRoleTable => idnUserRoleTable.UserId == userId)
                                        .FirstOrDefault();

                    idnUserRoleRepository.Delete(idnUserRole);
                }
            }
            catch (Exception ex)
            {
                logger.error("M3UserDataIntegrationService.RemoveUserRole", string.Format("An exception occurred trying to update the credential. Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3UserDataIntegrationService:0012", "Unable to update Web Credentials.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }

            return true;
        }

        public bool SetUserRoles(string userName, int roleId, bool autoCommit = false)
        {
            try
            {
                using (M3ContainerManager.Container.BeginLifetimeScope())
                {
                    IRepository<IdnUserRole> idnUserRoleRepository =
                        M3ContainerManager.Container.GetInstance<IRepository<IdnUserRole>>();

                    UserMasterInfo user = GetUserMasterInfoByName(userName);

                    if (user == null)
                    {
                        throw new Exception("User does not exist");
                    }

                    IdnUserRole role = new IdnUserRole
                    {
                        UserId = user.UserId,
                        RoleId = roleId,
                        CreatedBy = userName,
                        CreatedDate = System.DateTime.Now,
                        ModifiedBy = userName,
                        ModifiedDate = System.DateTime.Now
                    };

                    idnUserRoleRepository.Insert(role, autoCommit);
                }
            }
            catch (Exception ex)
            {
                logger.error("M3UserDataIntegrationService.SetUserRoles", string.Format("An exception occurred trying to update the credential. Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3UserDataIntegrationService:0012", "Unable to update Web Credentials.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }

            return true;
        }

        public UserMasterInfo GetUserMasterInfoByName(string name)
        {
            try
            {
                using (M3ContainerManager.Container.BeginLifetimeScope())
                {
                    IRepository<IdnUserMaster> idnUserMasterRepository =
                        M3ContainerManager.Container.GetInstance<IRepository<IdnUserMaster>>();

                    UserMasterInfo userMaster = (from idnUserMaster in idnUserMasterRepository.Table
                                                 where idnUserMaster.UserNameIdentifier.ToLower() == name.ToLower()
                                                 select new UserMasterInfo()
                                                 {
                                                     UserId = idnUserMaster.Id,
                                                     UserName = idnUserMaster.UserNameIdentifier
                                                 }).FirstOrDefault();
                    return userMaster;
                }
            }
            catch (Exception ex)
            {
                logger.error("M3UserDataIntegrationService.GetUserMasterInfoByName", string.Format("An exception occurred trying to create the user. Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3UserDataIntegrationService:0001", "Unable to Create Web Credential User.", M3ServiceResultFailCodes.M3FailUnknown, ex);
            }
        }

        public UserMasterInfo GetUserMasterInfoById(int id)
        {
            try
            {
                using (M3ContainerManager.Container.BeginLifetimeScope())
                {
                    IRepository<IdnUserMaster> idnUserMasterRepository =
                        M3ContainerManager.Container.GetInstance<IRepository<IdnUserMaster>>();

                    UserMasterInfo userMaster = (from idnUserMaster in idnUserMasterRepository.Table
                                                 where idnUserMaster.Id == id
                                                 select new UserMasterInfo()
                                                 {
                                                     UserId = idnUserMaster.Id,
                                                     UserName = idnUserMaster.UserNameIdentifier
                                                 }).FirstOrDefault();
                    return userMaster;
                }
            }
            catch (Exception ex)
            {
                logger.error("M3UserDataIntegrationService.GetUserMasterInfoById", string.Format("An exception occurred trying to retrieve the user. Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3UserDataIntegrationService:0001", "Unable to retrieve the user.", M3ServiceResultFailCodes.M3FailUnknown, ex);
            }
        }

        public bool UpdateUserCredentialByName(string userName, string userCredentialHash)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(userName))
                {
                    logger.error("M3UserDataIntegrationService.FindUserBasicInfoByName", "Invalid Username.");
                    throw new M3Exception("M3UserDataIntegrationService:0002", "Invalid Username.");
                }

                using (M3ContainerManager.Container.BeginLifetimeScope())
                {
                    IRepository<IdnWebCredential> idnWebCredentialRepository =
                        M3ContainerManager.Container.GetInstance<IRepository<IdnWebCredential>>();

                    var idnWebCredential =
                                        idnWebCredentialRepository
                                        .Table
                                        .Where(idnWebCredentialTable => idnWebCredentialTable.IdnUserMaster.UserNameIdentifier.ToLower() == userName.ToLower())
                                        .FirstOrDefault();

                    if (idnWebCredential == null)
                    {
                        logger.error("M3UserDataIntegrationService.FindUserBasicInfoById", string.Format("User not found. Message: User not found in the system with the following Name: {0}", userName));
                        throw new M3Exception("M3UserDataIntegrationService:0004", string.Format("User not found. Message: User not found in the system with the following Name: {0}", userName));
                    }

                    idnWebCredential.WebCredentialHash = userCredentialHash;

                    idnWebCredentialRepository.Update(idnWebCredential, autoCommit: true);
                }
            }
            catch (Exception ex)
            {
                logger.error("M3UserDataIntegrationService.UpdateWebCredentials", string.Format("An exception occurred trying to update the credential. Message: {0}", ex.GetBaseException().Message));
                throw new M3Exception("M3UserDataIntegrationService:0012", "Unable to update Web Credentials.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }

            return true;
        }
    }
}
