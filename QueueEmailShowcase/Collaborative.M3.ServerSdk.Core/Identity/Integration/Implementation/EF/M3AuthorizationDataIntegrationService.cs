﻿using Collaborative.M3.ServerSdk.Core.Business.Common.Exception;
using Collaborative.M3.ServerSdk.Core.Business.Common.Messaging;
using Collaborative.M3.ServerSdk.Core.Cache.Integration;
using Collaborative.M3.ServerSdk.Core.Common.IoC;
using Collaborative.M3.ServerSdk.Core.Identity.Domain.EF;
using Collaborative.M3.ServerSdk.Core.Identity.Integration.Interface;
using Collaborative.M3.ServerSdk.Core.Integration.EF.Implementation;
using Collaborative.M3.ServerSdk.Core.Integration.EF.Interface;
using Collaborative.M3.ServerSdk.Core.Logging;
using Collaborative.M3.ServerSdk.Identity.Domain;
using Collaborative.M3.ServerSdk.Identity.Integration.Util;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Identity.Integration.Implementation.EF
{
    public class M3AuthorizationDataIntegrationService : IM3AuthorizationDataIntegrationService
    {
        private static readonly M3Logger logger = M3Logger.getLogger("M3AuthorizationDataIntegrationService");

        public RoleEntitlementItem[] GetAllEntitlementsAllRoles()
        {
            RoleEntitlementItem[] roleEntitlementItems = null;
            try
            {
                roleEntitlementItems = M3CacheIntegrationService.Get<RoleEntitlementItem[]>("Authorization", "Entitlements");
                if (roleEntitlementItems[0] == null)
                {
                    return LoadAllEntitlementsAllRoles();
                }
            }
            catch (Exception ex)
            {
                logger.error("M3AuthorizationDataIntegrationService.GetAllEntitlementsAllRoles", string.Format("An error occurred getting the roles.  Message: {0}.", ex.GetBaseException().Message));
                throw new M3Exception("M3AuthorizationDataIntegrationService:0001", "An error occurred.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }
            return roleEntitlementItems;
        }

        public RoleEntitlementItem[] LoadAllEntitlementsAllRoles()
        {
            // declare the item object
            RoleEntitlementItem[] roleEntitlementItems = null;
            List<RoleEntitlementItem> roleEntitlementItemList = new List<RoleEntitlementItem>();

            try
            {
                using (M3ContainerManager.Container.BeginLifetimeScope())
                {
                    IRepository<IdnRoleDefinition> idnRoleDefinitionRepository = 
                        M3ContainerManager.Container.GetInstance<IRepository<IdnRoleDefinition>>();

                    IRepository<IdnRoleEntitlement> idnRoleEntitlementRepository = 
                        M3ContainerManager.Container.GetInstance<IRepository<IdnRoleEntitlement>>();

                    IRepository<IdnEntitlementDefinition> idnEntitlementDefinitionRepository = 
                        M3ContainerManager.Container.GetInstance<IRepository<IdnEntitlementDefinition>>();

                    var anonymousResult = (from idnRoleDefinitionTable in idnRoleDefinitionRepository.Table
                                           join idnRoleEntitlementTable in idnRoleEntitlementRepository.Table on idnRoleDefinitionTable.Id equals idnRoleEntitlementTable.RoleId
                                           join idnEntitlementDefinitionTable in idnEntitlementDefinitionRepository.Table on idnRoleEntitlementTable.EntitlementId equals idnEntitlementDefinitionTable.Id
                                           select new
                                           {
                                               RoleEntitlemenId = idnRoleEntitlementTable.Id,
                                               RoleId = idnRoleDefinitionTable.Id,
                                               RoleKey = idnRoleDefinitionTable.RoleKey,
                                               RoleDisplayName = idnRoleDefinitionTable.RoleDisplayName,
                                               RoleDescription = idnRoleDefinitionTable.RoleDescription,
                                               EntitlementId = idnEntitlementDefinitionTable.Id,
                                               EntitlementKey = idnEntitlementDefinitionTable.EntitlementKey,
                                               EntitlementDisplayName = idnEntitlementDefinitionTable.EntitlementDisplayName,
                                               EntitlementDescription = idnEntitlementDefinitionTable.EntitlementDescription
                                           }).ToList();

                    if (anonymousResult == null)
                    {
                        // no result found so fail
                        logger.error("M3AuthorizationDataIntegrationService.LoadAllEntitlementsAllRoles", "No rows returned.");
                        throw new M3Exception("M3AuthorizationDataIntegrationService:0002", "No roles returned.", M3ServiceResultFailCodes.M3FailRequestedDataNotFound);
                    }

                    anonymousResult.ForEach(result =>
                        roleEntitlementItemList.Add(new RoleEntitlementItem()
                        {
                            EntiltementDescription = result.EntitlementDescription,
                            EntitlementDisplayName = result.EntitlementDisplayName,
                            EntitlementId = result.EntitlementId.ToString(),
                            EntitlementKey = result.EntitlementKey,
                            RoleDescription = result.RoleDescription,
                            RoleDisplayName = result.RoleDisplayName,
                            RoleEntitlementId = result.RoleEntitlemenId.ToString(),
                            RoleId = result.RoleId.ToString(),
                            RoleKey = result.RoleKey
                        }));
                }

                roleEntitlementItems = roleEntitlementItemList.ToArray();

            }
            catch (Exception ex)
            {
                logger.error("M3AuthorizationDataIntegrationService.LoadAllEntitlementsAllRoles", string.Format("An error occurred loading the roles.  Message: {0}.", ex.GetBaseException().Message));
                throw new M3Exception("M3AuthorizationDataIntegrationService:0003", "An error occurred.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }

            // return the result
            return roleEntitlementItems;
        }

        public RoleInfoBasic[] GetRolesForUserById(string userId)
        {
            // declare the item object
            RoleInfoBasic[] roleInfoList = null;

            // convert the string to an int value
            int systemUserId = 0;

            List<RoleInfoBasic> roleInfoBasicList = new List<RoleInfoBasic>();

            try
            {
                if (!int.TryParse(userId, out systemUserId))
                {
                    logger.error("M3UserDataIntegrationService.FindUserBasicInfoById", "Invalid User Id Format.");
                    throw new M3Exception("M3UserDataIntegrationService:0002", "Invalid User Id.");
                }

                using (M3ContainerManager.Container.BeginLifetimeScope())
                {
                    IRepository<IdnRoleDefinition> idnRoleDefinitionRepository =
                        M3ContainerManager.Container.GetInstance<IRepository<IdnRoleDefinition>>();

                    IRepository<IdnUserRole> idnUserRoleRepository =
                        M3ContainerManager.Container.GetInstance<IRepository<IdnUserRole>>();

                    var userRoles = (from idnRoleDefinitionTable in idnRoleDefinitionRepository.Table
                                     join idnUserRoleTable in idnUserRoleRepository.Table on idnRoleDefinitionTable.Id equals idnUserRoleTable.RoleId
                                     where idnUserRoleTable.UserId == systemUserId
                                     select new
                                     {
                                         RoleId = idnRoleDefinitionTable.Id,
                                         RoleKey = idnRoleDefinitionTable.RoleKey
                                     }).ToList();

                    if (userRoles == null)
                    {
                        // no result found so fail
                        logger.error("M3AuthorizationDataIntegrationService.GetRolesForUserById", "No rows returned.");
                        throw new M3Exception("M3AuthorizationDataIntegrationService:0006", "No rows returned.", M3ServiceResultFailCodes.M3FailParameterInvalid);
                    }

                    userRoles.ForEach(roleInfoBasic =>
                        roleInfoBasicList.Add(new RoleInfoBasic()
                        {
                            RoleId = roleInfoBasic.RoleId.ToString(),
                            RoleKey = roleInfoBasic.RoleKey
                        }));
                }

                roleInfoList = roleInfoBasicList.ToArray();
            }
            catch (Exception ex)
            {
                logger.error("M3AuthorizationDataIntegrationService.GetRolesForUserById", string.Format("An error occurred loading the roles.  Message: {0}.", ex.GetBaseException().Message));
                throw new M3Exception("M3AuthorizationDataIntegrationService:0007", "An error occurred.", M3ServiceResultFailCodes.M3FailParameterInvalid, ex);
            }

            // return the result
            return roleInfoList;
        }
    }
}
