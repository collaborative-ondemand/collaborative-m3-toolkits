﻿using Collaborative.M3.ServerSdk.Identity.Integration.Util;
//using Oracle.ManagedDataAccess.Client;
using System;
using System.Configuration;
using System.Data;
using System.Data.Common;

namespace Collaborative.M3.ServerSdk.Core.Business.Integration.Database.Utilities
{
    /// <summary>
    /// Helper class containing DB related utilities such as connection factories.
    /// </summary>
    public class M3DBUtils
    {
        /// <summary>
        /// Constant representing a successful stored proc execution
        /// </summary>
        public const int M3_PROC_SUCCESS = 0;

        /// <summary>
        /// Cached value of the db connection name.  
        /// </summary>
        private static string dbConnectionNameCache = null;

        /// <summary>
        /// Factory method to get an active connection to the Showcase database.
        /// </summary>
        /// <returns>a connection to the showcase database.</returns>
        public static IDbConnection getDbConnection(string connectionStringName) {
            
            // check for cached value
            // TODO:  build the connect string cache to save on lookups
            if (M3DBUtils.dbConnectionNameCache == null)
            {
                // not cached so look it up
                M3DBUtils.dbConnectionNameCache = M3DBUtils.lookupConnectionString(connectionStringName);

                // check if the connection was found
                if (M3DBUtils.dbConnectionNameCache == null)
                {
                    // configuration is broken...
                    //TODO: Log this issue
                    throw new ConfigurationErrorsException("Database connection string " + connectionStringName + " not configured.");
                }
            }

            // now create the conection
            DbProviderFactory factory = DbProviderFactories.GetFactory(M3DBUtils.lookupProviderName(connectionStringName));
            IDbConnection connection = factory.CreateConnection();
            connection.ConnectionString = M3DBUtils.dbConnectionNameCache;

            // return the connection
            return connection;
            
        }


        /// <summary>
        /// Factory method to get a data adapter.
        /// </summary>
        /// <returns>a data adapter for the appropriate database.</returns>
        public static IDbDataAdapter getDbDataAdapter(string connectionStringName)
        {

            // check for cached value
            // TODO:  build the connect string cache to save on lookups
            if (M3DBUtils.dbConnectionNameCache == null)
            {
                // not cached so look it up
                M3DBUtils.dbConnectionNameCache = M3DBUtils.lookupConnectionString(connectionStringName);

                // check if the connection was found
                if (M3DBUtils.dbConnectionNameCache == null)
                {
                    // configuration is broken...
                    //TODO: Log this issue
                    throw new ConfigurationErrorsException("Database connection string " + connectionStringName + " not configured.");
                }
            }

            // now create the conection
            DbProviderFactory factory = DbProviderFactories.GetFactory(M3DBUtils.lookupProviderName(connectionStringName));
            IDbDataAdapter adapter = factory.CreateDataAdapter();

            // return the adapter
            return adapter;

        }


        /// <summary>
        /// Helper function to create a command object.
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="commandText"></param>
        /// <param name="commandType"></param>
        /// <returns>The command that was created.</returns>
        public static IDbCommand createCommand(IDbConnection connection, string commandText, CommandType commandType)
        {
            // check for null command or parameterName passed in
            IDbCommand command = null;
            if (connection != null)
            {
                // create the command object
                command = connection.CreateCommand();

                // set the command text
                command.CommandText = commandText;

                // set the command type
                command.CommandType = commandType;
            }

            return command;
        }

        
        /// <summary>
        /// Helper function to add a parameter and its value to the list of parameters.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="parameterName"></param>
        /// <param name="parameterValue"></param>
        /// <returns>The command with the parameter added.</returns>
        public static IDbCommand addParameter(IDbCommand command, string parameterName, object parameterValue, bool isSelectStatement = false)
        {
            // check for null command or parameterName passed in
            if (command != null && parameterName != null) 
            {
                // configure the parameters
                IDbDataParameter parameter = command.CreateParameter();
                parameter.ParameterName = parameterName;
                parameter.Value = parameterValue;
                command.Parameters.Add(parameter);
            }

            if (isSelectStatement)
            {
                string providerName = M3DBUtils.lookupProviderName(M3IdentityDbConstants.DB_CONNECTION_NAME_M3IDENTITY);
                if (providerName.ToLower().Contains("oracle"))
                {
                    //Uncomment for Oracle
                    //OracleParameter par = new OracleParameter("cur", OracleDbType.RefCursor, System.Data.ParameterDirection.Output);
                    //command.Parameters.Add(par);
                }
            }

            return command;
        }


        /// <summary>
        /// Helper function to lookup the connection string from the active web.config file.
        /// </summary>
        /// <param name="connectionName"></param>
        /// <returns>The requested connection string or null if the string is not found.</returns>
        private static String lookupConnectionString(String connectionName)
        {
            string connectString = null;

            // perform the lookup of all connection strings
            ConnectionStringSettings connectStringConfig = ConfigurationManager.ConnectionStrings[connectionName];

            // check if found
            if (connectStringConfig != null)
            {
                // found the string
                connectString = connectStringConfig.ConnectionString;
            }

            // return the result
            return connectString;

        }

        /// <summary>
        /// Helper function to lookup the provider name from the active web.config file.
        /// </summary>
        /// <param name="connectionName"></param>
        /// <returns>The requested provider name or null if the string is not found.</returns>
        private static String lookupProviderName(String connectionName)
        {
            string providerName = null;

            // perform the lookup of all connection strings
            ConnectionStringSettings connectStringConfig = ConfigurationManager.ConnectionStrings[connectionName];

            // check if found
            if (connectStringConfig != null)
            {
                // found the string
                providerName = connectStringConfig.ProviderName;
            }

            // return the result
            return providerName;

        }
    }
}
