﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Business.Integration.Database.Utilities
{
    public class M3DatabaseConstants
    {
        public static int LIST_FIRST = 0;
        public static String MAPPED_RESULT_KEY = "result";
    }
}
