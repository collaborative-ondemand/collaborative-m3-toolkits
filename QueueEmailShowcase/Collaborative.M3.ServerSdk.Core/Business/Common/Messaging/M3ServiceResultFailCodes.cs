﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Business.Common.Messaging
{
    /// <summary>
    /// Contains the standard reserved result fail reason codes provided by the M3 Framework.
    /// </summary>
    public enum M3ServiceResultFailCodes
    {
        M3FailUnknown,
        M3FailMissingParameter,
        M3FailParameterInvalid,
        M3FailRequestedDataNotFound,
        M3FailAuthenticationRequired,
        M3FailAuthorizationRequired,
        M3FailRequestInactiveUser
    }
}
