﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Business.Common.Messaging
{

    /// <summary>
    /// Contains the result of an M3 Service API call.  The IsSuccess property indicates the overall success of the service execution.  If IsSuccess returns
    /// true the payload may or may not be populated with the PayloadType.  If IsSuccess is false no payload items will be provided.  Adding a fail reason through any of
    /// the overloaded methods will result in IsSuccess being toggled to false.
    /// </summary>
    /// <typeparam name="PayloadType"></typeparam>
    public class M3ServiceResult<PayloadType>
    {
        /// <summary>
        /// Contains the list of fail reasons associated with this response
        /// </summary>
        private List<M3ServiceResultFailItem> failReasons = new List<M3ServiceResultFailItem>();

        /// <summary>
        /// Contains the list of payload items
        /// </summary>
        private List<PayloadType> payloadItems = new List<PayloadType>();

        /// <summary>
        /// Flag indicating success or failure of the service invocation.
        /// </summary>
        public bool IsSuccess { get; private set; }
       
        /// <summary>
        /// Default Constructor. Creates an M3ServiceResult with IsSuccess defaulted to true.
        /// </summary>
        public M3ServiceResult()
        {
            IsSuccess = true;

        }

        /// <summary>
        /// Constructor builds a failed result with a code and an empty reason.
        /// </summary>
        /// <param name="failCode"></param>
        public M3ServiceResult(string failCode) : this(failCode,"")
        {
            
        }

        /// <summary>
        /// Constructor builds a failed result with a code and reason.
        /// </summary>
        /// <param name="failCode"></param>
        /// <param name="failReason"></param>
        public M3ServiceResult(string failCode, string failReason)
        {
            this.SetResultFailed();
            this.AddFailReason(failCode, failReason);
        }

        public M3ServiceResult(M3ServiceResultFailCodes failCode)
        {
            this.SetResultFailed();
            this.AddFailReason(failCode, "");
        }

        public M3ServiceResult(M3ServiceResultFailCodes failCode, string failReason)
        {
            this.SetResultFailed();
            this.AddFailReason(failCode, failReason);
        }
        /// <summary>
        /// Adds a payload item of type <T> to the payload.
        /// </summary>
        /// <param name="newPayloadItem"></param>
        public void AddPayloadItem(PayloadType newPayloadItem)
        {
            this.payloadItems.Add(newPayloadItem);
        }

        /// <summary>
        /// Adds a collection of items of type <T> to the payload.
        /// </summary>
        /// <param name="newPayloadItems"></param>
        public void AddPayloadItem(IEnumerable<PayloadType> newPayloadItems)
        {
            this.payloadItems.AddRange(newPayloadItems);
        }

        /// <summary>
        /// Returns the first item in the payload list.
        /// </summary>
        /// <returns></returns>
        public PayloadType GetPayloadItem()
        {
            return this.payloadItems.FirstOrDefault<PayloadType>();
        }

        /// <summary>
        /// Returns an IEnumerable<T> containing the payload.
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PayloadType> GetPayloadItems()
        {
            return this.payloadItems;
        }
        /// <summary>
        /// Toggles the status of the message permenantly to failed.
        /// </summary>
        public void SetResultFailed()
        {
            IsSuccess = false;
        }


        /// <summary>
        /// Toggles the status of the message permenantly to failed and adds the code and reason.
        /// </summary>
        /// <param name="failCode"></param>
        /// <param name="failReason"></param>
        public void SetResultFailed(string failCode, string failReason)
        {
            this.SetResultFailed();
            this.AddFailReason(failCode, failReason);
        }

        /// <summary>
        /// Toggles the status of the message permenantly to failed and adds the code with an empty reason.
        /// </summary>
        /// <param name="failCode"></param>
        public void SetResultFailed(string failCode)
        {
            this.SetResultFailed(failCode, "");
        }

        /// <summary>
        /// Toggles the status of the message permenantly to failed and adds the code based on M3ServiceResultFailCodes with an empty reason.
        /// </summary>
        /// <param name="m3FailCode"></param>
        public void SetResultFailed(M3ServiceResultFailCodes m3FailCode)
        {
            this.SetResultFailed(m3FailCode.ToString());
        }

        /// <summary>
        /// Toggles the status of the message permenantly to failed and adds the code based on M3ServiceResultFailCodes with the provided reason.
        /// </summary>
        /// <param name="m3FailCode"></param>
        /// <param name="failReason"></param>
        public void SetResultFailed(M3ServiceResultFailCodes m3FailCode, string failReason)
        {
            this.SetResultFailed(m3FailCode.ToString(), failReason);
        }

        /// <summary>
        /// Returns the list of fail reasons as an array.
        /// </summary>
        /// <returns></returns>
        public M3ServiceResultFailItem[] FailReason()
        {
            return failReasons.ToArray<M3ServiceResultFailItem>();
        }

        /// <summary>
        /// Adds a new M3ServiceResultFailReason to the list of fail reasons for this result and sets the result status to failed.
        /// </summary>
        /// <param name="failCode"></param>
        /// <param name="failReason"></param>
        public void AddFailReason(string failCode, string failReason)
        {
            M3ServiceResultFailItem m3FailReason = new M3ServiceResultFailItem();
            m3FailReason.FailCode = failCode;
            m3FailReason.FailReason = failReason;

            // add to the lsit
            this.AddFailReason(m3FailReason);
        }

        /// <summary>
        /// Adds a new M3ServiceResultFailReason with a failCode and emtpy reason to the list of fail reasons for this result and sets the result status to failed.
        /// </summary>
        /// <param name="failCode"></param>
        public void AddFailReason(string failCode)
        {
            this.AddFailReason(failCode, "");
        }
        /// <summary>
        /// Adds a new M3ServiceResultFailItem to the list of fail reasons for this result and sets the result status to failed.
        /// </summary>
        /// <param name="failReason"></param>
        public void AddFailReason(M3ServiceResultFailItem m3FailItem)
        {
            // Set the success flag to false
            this.SetResultFailed();

            // add the message to the list
            this.failReasons.Add(m3FailItem);
        }

        /// <summary>
        /// Adds a new M3ServiceResultFailItem to the list of fail reasons using M3ServiceResultFailCode and the provided fail reason.
        /// </summary>
        /// <param name="m3FailCode"></param>
        /// <param name="failReason"></param>
        public void AddFailReason(M3ServiceResultFailCodes m3FailCode, string failReason)
        {
            this.AddFailReason(m3FailCode.ToString(), failReason);
        }

        /// <summary>
        /// Adds a new M3ServiceResultFailItem to the list of fail reasons using M3ServiceResultFailCode and an empty fail reason.
        /// </summary>
        /// <param name="m3FailCode"></param>
        public void AddFailReason(M3ServiceResultFailCodes m3FailCode)
        {
            this.AddFailReason(m3FailCode.ToString(), "");
        }
    }
}
