﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Business.Common.Messaging
{
    public class M3ServiceResultFailItem
    {
        public string FailCode { get; set; }
        public string FailReason { get; set; }
    }
}
