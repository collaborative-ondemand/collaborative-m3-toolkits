﻿using Collaborative.M3.ServerSdk.Core.Business.Domain;
using Collaborative.M3.ServerSdk.Core.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Business.Common.Container
{
    public static class M3Container
    {
        #region Fields
        static ReaderWriterLockSlim allRegistrationsCacheLock = new ReaderWriterLockSlim();
        static IList<Tuple<string, Type, Type, object, RegistrationStrategy>> container = new List<Tuple<string, Type, Type, object, RegistrationStrategy>>();
        #endregion
        /// <summary>
        /// Method that registers all concrete classes based on an interface TType passed
        /// </summary>
        /// <typeparam name="TInterfaceType">Interface type to lookup in assemblies in order find the corresponding concrete implementations</typeparam>
        /// <param name="registrationStrategyType">Two options PerCall (default if not passed) or Singleton</param>
        public static void RegisterAllConcreteImplementations<TInterfaceType>(RegistrationStrategy registrationStrategyType = RegistrationStrategy.PerCall)
        {
            //Using IList as good practice, since the calling method could return a sorted list, or a paged list or a general list

            IList<Type> typesToRegister = GetAllAssembliesFromCallingProject<TInterfaceType>();
            typesToRegister = typesToRegister.Concat(GetAllAssembliesWithinCurrentProject<TInterfaceType>(typesToRegister)).ToList();

            foreach (Type type in typesToRegister)
            {
                switch (registrationStrategyType)
                {
                    case RegistrationStrategy.Singleton:
                        RegisterInstance(registrationType: type, instanceType: type, createInstance: true, registrationStrategy: registrationStrategyType);
                        break;
                    case RegistrationStrategy.PerCall:
                        RegisterInstance(registrationType: type, instanceType: type, createInstance: false, registrationStrategy: registrationStrategyType);
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// Method used to register and create a new instance at the same time. The type used is TType and the instance to be created is implementationType
        /// </summary>
        /// <typeparam name="TType">Type used to locate the concrete class</typeparam>
        /// <param name="implementationType">Type that will be used to create a new instance</param>
        /// <param name="registrationStrategyType">Optional parameter if not specified it will use PerCall strategy which means a new instance of implementationType will be created everytime TType is requested</param>
        public static void Register<TType>(Type implementationType, RegistrationStrategy registrationStrategyType = RegistrationStrategy.PerCall)
            where TType : class
        {
            Type type = typeof(TType);

            switch (registrationStrategyType)
            {
                case RegistrationStrategy.Singleton:
                    RegisterInstance(registrationType: type, instanceType: implementationType, createInstance: true, registrationStrategy: registrationStrategyType);
                    break;
                case RegistrationStrategy.PerCall:
                    RegisterInstance(registrationType: type, instanceType: implementationType, createInstance: false, registrationStrategy: registrationStrategyType);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Method used to register and create a new instance at the same time. TInterface will be used to locate an instance of TClass
        /// </summary>
        /// <typeparam name="TInterface">Type used to locate the concrete class</typeparam>
        /// <typeparam name="TClass">Type that will be used to create a new instance</typeparam>
        /// <param name="registrationStrategyType">Optional parameter if not specified it will use PerCall strategy which means a new instance of implementationType will be created everytime TType is requested</param>
        public static void Register<TInterface, TClass>(RegistrationStrategy registrationStrategyType = RegistrationStrategy.PerCall)
            where TClass : class
        {
            Type type = typeof(TInterface);

            switch (registrationStrategyType)
            {
                case RegistrationStrategy.Singleton:
                    RegisterInstance(registrationType: type, instanceType: typeof(TClass), createInstance: true, registrationStrategy: registrationStrategyType);
                    break;
                case RegistrationStrategy.PerCall:
                    RegisterInstance(registrationType: type, instanceType: typeof(TClass), createInstance: false, registrationStrategy: registrationStrategyType);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Method used to register a concrete instance already created (different than other methods that when a type is registered it will create a new instance at the same time). 
        /// This method is used when a concrete instance wants to be registered.
        /// Uses Singleton strategy meaning it will return the same instance that was passed, everytime the type is retrieved.
        /// </summary>
        /// <param name="concreteType">Instance of a class that will be registered as Singleton</param>
        public static void RegisterSingle(object concreteType)
        {
            Type type = concreteType.GetType();

            allRegistrationsCacheLock.EnterReadLock();
            try
            {
                if (container.Any(i => i.Item1 == type.FullName))
                {
                    //Do nothing, is already registered
                    return;
                }
            }
            finally
            {
                allRegistrationsCacheLock.ExitReadLock();
            }

            allRegistrationsCacheLock.EnterWriteLock();
            try
            {
                if (container.Any(i => i.Item1 == type.FullName))
                {
                    //Validate it twice because maybe other thread already registered it, if so then do nothing otherwise register the object
                    return;
                }
                container.Add(new Tuple<string, Type, Type, object, RegistrationStrategy>(type.FullName, type, type, concreteType, RegistrationStrategy.Singleton));
            }
            finally
            {
                allRegistrationsCacheLock.ExitWriteLock();
            }
        }

        /// <summary>
        /// Method used to register and create a new instance at the same time. This method is used to register and create a new instance of implementationType. 
        /// implementationType will be used to locate and create a new instance of a class
        /// </summary>
        /// <param name="implementationType">Type that will be used to locate and create a new instance of a class</param>
        /// <param name="registrationStrategyType">Optional parameter if not specified it will use PerCall strategy which means a new instance of implementationType will be created everytime TType is requested</param>
        public static void Register(Type implementationType, RegistrationStrategy registrationStrategyType = RegistrationStrategy.PerCall)
        {
            switch (registrationStrategyType)
            {
                case RegistrationStrategy.Singleton:
                    RegisterInstance(registrationType: implementationType, instanceType: implementationType, createInstance: true, registrationStrategy: registrationStrategyType);
                    break;
                case RegistrationStrategy.PerCall:
                    RegisterInstance(registrationType: implementationType, instanceType: implementationType, createInstance: false, registrationStrategy: registrationStrategyType);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Method used to retrieve an instance of a class. Pass TType in order to locate the intance of a class
        /// </summary>
        /// <typeparam name="TType">Type used to locate an instance of a class</typeparam>
        /// <returns>An instance of TTYpe using the strategy specified when the type was registered</returns>
        public static TType GetInstance<TType>()
            where TType : class
        {
            var tuple = container.Where(i => i.Item2 == typeof(TType) || i.Item3 == typeof(TType)).FirstOrDefault();
            if (tuple != null)
            {
                switch (tuple.Item5)
                {
                    case RegistrationStrategy.Singleton:
                        return tuple.Item4 as TType;
                    case RegistrationStrategy.PerCall:
                        return Activator.CreateInstance(tuple.Item3) as TType;
                    default:
                        return null;
                }
            }
            return null;
        }

        /// <summary>
        /// Method used to retrieve all instances of a class. Passing TType, if it is an interface, it will retrieve all classes that implemented the interface
        /// </summary>
        /// <typeparam name="TType">Type used to locate an instance of a class</typeparam>
        /// <returns>A List containing all instances of TType</returns>
        public static IList<TType> GetAllInstances<TType>()
            where TType : class
        {
            var tuples = container.Where(i => i.Item2 == typeof(TType) || i.Item3 == typeof(TType)).ToList();
            List<TType> allInstances = new List<TType>();
            if (tuples != null && tuples.Count > 0)
            {
                foreach (var tuple in tuples)
                {
                    allInstances.Add(GetInstance<TType>());
                }
                return allInstances;
            }
            return null;
        }

        /// <summary>
        /// Method used to retrieve an instance of a class.
        /// </summary>
        /// <param name="type">Type used to locate an instance of a class</param>
        /// <returns>An instance of an object using the strategy specified when the type was registered</returns>
        public static object GetInstance(Type type)
        {
            var tuple = container.Where(i => i.Item2 == type || i.Item3 == type).FirstOrDefault();
            if (tuple != null)
            {
                switch (tuple.Item5)
                {
                    case RegistrationStrategy.Singleton:
                        return tuple.Item4;
                    case RegistrationStrategy.PerCall:
                        return Activator.CreateInstance(tuple.Item3);
                    default:
                        return null;
                }
            }
            return null;
        }

        /// <summary>
        /// Method used to retrieve all instances of a class.
        /// </summary>
        /// <param name="type">Type used to locate all instances of a class</param>
        /// <returns>A List containing all instances of object</returns>
        public static IList<object> GetAllInstances(Type type)
        {
            var tuples = container.Where(i => i.Item2 == type || i.Item3 == type).ToList();
            List<object> allInstances = new List<object>();
            if (tuples != null && tuples.Count > 0)
            {
                foreach (var tuple in tuples)
                {
                    allInstances.Add(GetInstance(type));
                }
                return allInstances;
            }
            return null;
        }

        /// <summary>
        /// Method used to evaluate if a given TType is already registered
        /// </summary>
        /// <typeparam name="TType">Type to evaluate if it was already registered</typeparam>
        /// <returns>Boolean indicating if the type is or is not registered</returns>
        public static bool HasRegisteredType<TType>()
        {
            return container.Any(tuple => tuple.Item2 == typeof(TType) || tuple.Item3 == typeof(TType));
        }

        /// <summary>
        /// Method used to retrieved all the classes that implements TInterfaceType from the caller method. Currently is set as Private, 
        /// because of this, there won't be any benefit since this method evaluates if a caller's assemblies are implementing TInterfaceType.
        /// i.e. If Project X calls this method, then this method will evaluate all referenced assemblies within Project X
        /// </summary>
        /// <typeparam name="TInterfaceType">Interface used to locate all classes implementing it</typeparam>
        /// <returns>List of all Types - Classes - implementing TInterfaceType</returns>
        private static IList<Type> GetAllAssembliesFromCallingProject<TInterfaceType>()
        {
            Assembly thisAssembly = Assembly.GetExecutingAssembly();

            StackTrace stackTrace = new StackTrace();
            StackFrame[] frames = stackTrace.GetFrames();

            List<Assembly> tmpAssemblyList = new List<Assembly>();
            List<Type> typesToRegisterList = new List<Type>();

            foreach (var stackFrame in frames)
            {
                var declaringType = stackFrame.GetMethod().DeclaringType;
                if (declaringType != null)
                {
                    var ownerAssembly = stackFrame.GetMethod().DeclaringType.Assembly;
                    if (!tmpAssemblyList.Any(assembly => assembly == ownerAssembly))
                    {
                        //Temp list required because the stack could contain duplicate references to the same assembly so we don't want to look up again something
                        //that was already evaluated.

                        tmpAssemblyList.Add(ownerAssembly);

                        //There will be only one calling project that contains all the types to register.
                        //i.e. Project A contains implementations of interface of IType1, the method calling register 
                        var typesToRegister =
                            ownerAssembly
                            .GetTypes()
                            .Where(type => !String.IsNullOrEmpty(type.Namespace))
                            .Where(type => type.GetInterfaces().Contains(typeof(TInterfaceType)));

                        if (typesToRegister.Count() > 0)
                            typesToRegisterList.AddRange(typesToRegister.ToList());
                    }
                }
            }
            return typesToRegisterList;
        }

        /// <summary>
        /// Method used to retrieved all the classes that implements TInterfaceType within the current project
        /// </summary>
        /// <typeparam name="TInterfaceType">Interface used to locate all classes implementing it</typeparam>
        /// <param name="typesFound">List containing Types that got already registered in order to prevent duplicated types to be Added to the return List</param>
        /// <returns>List of all Types - Classes - implementing TInterfaceType</returns>
        private static IList<Type> GetAllAssembliesWithinCurrentProject<TInterfaceType>(IList<Type> typesFound)
        {
            var typesToRegister = Assembly.GetExecutingAssembly().GetTypes()
            .Where(type => !String.IsNullOrEmpty(type.Namespace))
            .Where(type => type.GetInterfaces().Contains(typeof(TInterfaceType)))
            .Where(type => !typesFound.Any(i => i.FullName == type.FullName));

            return typesToRegister.ToList();
        }

        /// <summary>
        /// Method used to Register a searchable Type and to create the proper instance of the type. Both could be different in the case that registrationType is an Interface and instanceType is the class implementing the interface. 
        /// </summary>
        /// <param name="registrationType">Type used to locate the instance of a class</param>
        /// <param name="instanceType">Type used to create a new instance of a class that will be located by searching for registrationType</param>
        /// <param name="createInstance">Boolean indicating if a new instance of instanceType is required or not</param>
        /// <param name="registrationStrategy">This could be PerCall or Singleton, used when an instance is retrieved</param>
        private static void RegisterInstance(Type registrationType, Type instanceType, bool createInstance, RegistrationStrategy registrationStrategy)
        {
            allRegistrationsCacheLock.EnterReadLock();
            object instance = null;
            try
            {
                if (container.Any(i => i.Item2 == registrationType || i.Item3 == registrationType))
                {
                    //Do nothing, is already registered
                    return;
                }
            }
            finally
            {
                allRegistrationsCacheLock.ExitReadLock();
            }

            allRegistrationsCacheLock.EnterWriteLock();
            try
            {
                if (container.Any(i => i.Item2 == registrationType || i.Item3 == registrationType))
                {
                    //Validate it twice because maybe other thread already registered it, if so then do nothing otherwise register the object
                    return;
                }

                if (createInstance)
                    instance = Activator.CreateInstance(instanceType);

                container.Add(new Tuple<string, Type, Type, object, RegistrationStrategy>(registrationType.FullName, registrationType, instanceType, instance, registrationStrategy));
            }
            finally
            {
                allRegistrationsCacheLock.ExitWriteLock();
            }
        }
    }
}
