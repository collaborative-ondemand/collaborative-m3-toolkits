﻿using Collaborative.M3.ServerSdk.Core.Business.Common.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Business.Common.Exception
{
    public class M3Exception : System.Exception
    {
        public const string idFormat = "M3_{0}";

        private string id;
        private M3ServiceResultFailCodes type;
        private List<KeyValuePair<string, object>> details = new List<KeyValuePair<string, object>>();

        public M3Exception(string id, string message)
            : this(id, message, M3ServiceResultFailCodes.M3FailUnknown, null)
        { }

        public M3Exception(string id, string message, System.Exception inner)
            : this(id, message, M3ServiceResultFailCodes.M3FailUnknown, inner)
        { }

        public M3Exception(string id, string message, M3ServiceResultFailCodes type)
            : this(id, message, type, null)
        { }

        public M3Exception(string id, string message, M3ServiceResultFailCodes type, System.Exception inner)
            : base(message, inner)
        {
            this.id = string.Format(M3Exception.idFormat, id);
            this.type = type;
        }

        public void addDetail(string key, object value)
        {
            KeyValuePair<string, object> pair = new KeyValuePair<string, object>(key, value);
            details.Add(pair);
        }

        public M3ServiceResultFailCodes Type()
        {
            return this.type;
        }

        public IEnumerable<KeyValuePair<string, object>> Details()
        {
            return this.details;
        }
    }
}
