﻿﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Business.Common.Utils
{
    public static class Conversion<TInput, TOutput>
    {
        private static readonly Func<TInput, TOutput> Converter;

        static Conversion()
        {
            Converter = CreateConverter();
        }

        /// <summary>
        /// Creates the converter.
        /// </summary>
        /// <returns></returns>
        private static Func<TInput, TOutput> CreateConverter()
        {
            var input = Expression.Parameter(typeof(TInput), "input");

            // For each property that exists in the destination object, is there a property with the same name in the source object?
            var destinationProperties = typeof(TOutput)
                .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(prop => prop.CanWrite);

            var sourceProperties = typeof(TInput)
                .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(prop => prop.CanRead);

            var memberBindings = sourceProperties.Join(destinationProperties,
                sourceProperty => sourceProperty.Name,
                destinationProperty => destinationProperty.Name,
                (sourceProperty, destinationProperty) => (MemberBinding)Expression.Bind(destinationProperty, Expression.Property(input, sourceProperty)));

            var body = Expression.MemberInit(Expression.New(typeof(TOutput)), memberBindings);
            var lambda = Expression.Lambda<Func<TInput, TOutput>>(body, input);

            return lambda.Compile();
        }

        public static TOutput From(TInput input)
        {
            if (input != null)
            {
                return Converter(input);
            }

            return default(TOutput);
        }

        /// <summary>
        /// Froms the list.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        public static List<TOutput> FromList(List<TInput> input)
        {
            List<TOutput> output = (from x in input
                                    where x != null
                                    select Conversion<TInput, TOutput>.From(x)).ToList();
            return output;
        }

        /// <summary>
        /// Inflates a new object type list from a data table.
        /// </summary>
        /// <param name="dt">The dt.</param>
        /// <returns></returns>
        public static List<TOutput> FromDataTable(DataTable dt)
        {
            return Mapper.Map<IDataReader, List<TOutput>>(dt.CreateDataReader());
        }
    }
}