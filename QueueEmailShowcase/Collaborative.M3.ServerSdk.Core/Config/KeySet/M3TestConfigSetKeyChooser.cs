﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Config.KeySet
{
    /// <summary>
    /// Implementation of IM3ConfigSetKeyChooser that always returns the string "testConfigKey".
    /// </summary>
    public class M3TestConfigSetKeyChooser : IM3ConfigSetKeyChooser
    {
        private const string TEST_CONFIG_KEY = "testConfigKey";
        /// <summary>
        /// Always returns "testConfigKey".
        /// </summary>
        /// <returns></returns>
        public string GetActiveM3ConfigKey()
        {
            return TEST_CONFIG_KEY;
        }
    }
}
