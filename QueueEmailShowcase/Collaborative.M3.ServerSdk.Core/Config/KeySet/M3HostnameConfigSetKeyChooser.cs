﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Collaborative.M3.ServerSdk.Core.Config.KeySet
{
    /// <summary>
    /// Default Config Set Key Chooser is used when no othe Key Chooser is configured to be used.
    /// </summary>
    public class M3HostnameConfigSetKeyChooser : IM3ConfigSetKeyChooser
    {
        /// <summary>
        /// Determines the URL by detecting the host name of the deployed server and request.
        /// </summary>
        /// <returns></returns>
        public string GetActiveM3ConfigKey()
        {
            string deployedHost = System.Web.HttpContext.Current.Request.Url.Host;
            Console.WriteLine("M3DefaultConfigSetKeyChooser detected the config set key: " + deployedHost);

            return deployedHost;
        }
    }
}
