﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Config.KeySet
{
    public interface IM3ConfigSetKeyChooser
    {
        string GetActiveM3ConfigKey();
 
    }
}
