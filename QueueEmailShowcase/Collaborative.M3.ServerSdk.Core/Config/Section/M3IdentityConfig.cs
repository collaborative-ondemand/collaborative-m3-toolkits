﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Config.Section
{
    public class M3IdentityConfig : ConfigurationElement
    {
        [ConfigurationProperty("dataAccessType", IsRequired = true)]
        public String DataAccessType
        {
            get
            {
                return (String)this["dataAccessType"];
            }
            set
            {
                this["dataAccessType"] = value;
            }
        }
    }
}
