﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Config.Section
{
    public class M3CacheConfigsCollection : ConfigurationElementCollection
    {
        /// <summary>
        /// <see cref="System.Configuration.ConfigurationElement.CreateNewElement()"/>
        /// </summary>
        /// <returns></returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new M3CacheConfig();
        }

        /// <summary>
        /// <see cref="System.Configuration.ConfigurationElement.GetElementKey()"/>
        /// </summary>
        /// <returns></returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((M3CacheConfig)element).Name;
        }

        /// <summary>
        /// Index property to get an M3ConfigSet at the specified location.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public M3CacheConfig this[int index]
        {

            get
            {
                return (M3CacheConfig)BaseGet(index);
            }

        }

        public new M3CacheConfig this[string key]
        {

            get
            {
                return (M3CacheConfig)BaseGet(key);
            }

        }
    }
}
