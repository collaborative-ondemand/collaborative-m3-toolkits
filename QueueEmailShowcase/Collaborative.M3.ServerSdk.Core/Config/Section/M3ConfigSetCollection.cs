﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Config.Section
{
    /// <summary>
    /// Collection of all m3ConfigSet instances configured in Web.config.
    /// </summary>
    public class M3ConfigSetCollection : ConfigurationElementCollection
    {
        /// <summary>
        /// Public constructor.
        /// </summary>
        public M3ConfigSetCollection()
        {

        }

        /// <summary>
        /// <see cref="System.Configuration.ConfigurationElement.CreateNewElement()"/>
        /// </summary>
        /// <returns></returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new M3ConfigSet();
        }

        /// <summary>
        /// <see cref="System.Configuration.ConfigurationElement.GetElementKey()"/>
        /// </summary>
        /// <returns></returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((M3ConfigSet)element).Name;
        }

        /// <summary>
        /// Index property to get an M3ConfigSet at the specified location.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public M3ConfigSet this[int index]
        {

            get 
            { 
                return (M3ConfigSet) BaseGet(index); 
            }

            set
            {

                if (BaseGet(index) != null)
                {

                    BaseRemoveAt(index);

                }

                BaseAdd(index, value);
            }

        }
    }
}
