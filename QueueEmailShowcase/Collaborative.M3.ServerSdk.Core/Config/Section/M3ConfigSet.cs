﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Config.Section
{
    public class M3ConfigSet : ConfigurationElement
    {

        /// <summary>
        /// The logical name of this configuration set.
        /// </summary>
        [ConfigurationProperty("name", IsRequired = true)]
        public String Name
        {
            get
            {
                return (String)this["name"];
            }
            set
            {
                this["name"] = value;
            }
        }

        /// <summary>
        /// Indicates the key that was used to select this config set.
        /// </summary>
        [ConfigurationProperty("m3ConfigKey", IsRequired = true)]
        public String M3ConfigKey
        {
            get
            {
                return (String)this["m3ConfigKey"];
            }
            set
            {
                this["m3ConfigKey"] = value;
            }
        }

        /// <summary>
        /// Configuration related to the M3 Logging component.
        /// </summary>
        [ConfigurationProperty("m3LoggingConfig", IsRequired = true)]
        public M3LoggingConfig M3LoggingConfig
        {
            get
            {
                return (M3LoggingConfig)this["m3LoggingConfig"];
            }

            set
            {
                this["m3LoggingConfig"] = value;
            }
        }

        /// <summary>
        /// Application specific configuration information.
        /// </summary>
        [ConfigurationProperty("m3AppConfigs", IsRequired = false)]
        [ConfigurationCollection(typeof(M3AppConfigsCollection), AddItemName = "m3AppConfig")]
        public M3AppConfigsCollection M3AppConfigs
        {
            get
            {
                return base["m3AppConfigs"] as M3AppConfigsCollection;
            }

        }

        /// <summary>
        /// Cache specific configuration information.
        /// </summary>
        [ConfigurationProperty("m3CacheConfigs", IsRequired = false)]
        [ConfigurationCollection(typeof(M3CacheConfigsCollection), AddItemName = "m3CacheConfig")]
        public M3CacheConfigsCollection M3CacheConfigs
        {
            get
            {
                return base["m3CacheConfigs"] as M3CacheConfigsCollection;
            }

        }

    }
}
