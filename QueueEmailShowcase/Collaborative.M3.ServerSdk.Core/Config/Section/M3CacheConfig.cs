﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Config.Section
{
    public class M3CacheConfig : ConfigurationElement
    {
        [ConfigurationProperty("name", IsKey = true, IsRequired = true)]
        public String Name
        {
            get
            {
                return (String)this["name"];
            }
            set
            {
                this["name"] = value;
            }
        }

        [ConfigurationProperty("timeToLive", IsRequired = true)]
        public String TimeToLive
        {
            get
            {
                return (String)this["timeToLive"];
            }
            set
            {
                this["timeToLive"] = value;
            }
        }

        [ConfigurationProperty("type", IsRequired = true)]
        public String Type
        {
            get
            {
                return (String)this["type"];
            }
            set
            {
                this["type"] = value;
            }
        }

        [ConfigurationProperty("loader", IsRequired = false)]
        public String Loader
        {
            get
            {
                return (String)this["loader"];
            }
            set
            {
                this["loader"] = value;
            }
        }
    }
}
