﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Config.Section
{

    /// <summary>
    /// Root configuration handler for the m3Config section of Web.config.  This class reflects the top-level of the config hierarchy and
    /// is not typically used by applications.  This class is typically only used by the framework to load the various config sets and 
    /// determine thea active config set.
    /// </summary>
    public class M3ConfigSection : ConfigurationSection
    {
       
        /// <summary>
        /// Property containing an M3ConfigSetCollection of all configured M3ConfigSets.
        /// </summary>
        [ConfigurationProperty("m3ConfigSets")]
        [ConfigurationCollection(typeof(M3ConfigSetCollection), AddItemName = "m3ConfigSet")]
        public M3ConfigSetCollection M3ConfigSets
        {
            get 
            { 
                return base["m3ConfigSets"] as M3ConfigSetCollection; 
            }
            
        }

        /// <summary>
        /// Indicates the implementation classname of the ConfigSet Chooser provider used to select the active config set.
        /// </summary>
        [ConfigurationProperty("m3ConfigSetKeyChooserProvider", IsRequired = true)]
        public String ConfigSetChooserProvider
        {
            get
            {
                return (String)this["m3ConfigSetKeyChooserProvider"];
            }
        }
    }
}
