﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Config.Section
{
    public class M3AppConfigsCollection : ConfigurationElementCollection
    {
        /// <summary>
        /// <see cref="System.Configuration.ConfigurationElement.CreateNewElement()"/>
        /// </summary>
        /// <returns></returns>
        protected override ConfigurationElement CreateNewElement()
        {
            return new M3AppConfig();
        }

        /// <summary>
        /// <see cref="System.Configuration.ConfigurationElement.GetElementKey()"/>
        /// </summary>
        /// <returns></returns>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((M3AppConfig)element).Name;
        }

        /// <summary>
        /// Index property to get an M3ConfigSet at the specified location.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public M3AppConfig this[int index]
        {

            get
            {
                return (M3AppConfig)BaseGet(index);
            }

            /*
            set
            {

                if (BaseGet(index) != null)
                {

                    BaseRemoveAt(index);

                }

                BaseAdd(index, value);
            }
            */
        }

        public new M3AppConfig this[string key]
        {

            get
            {
                return (M3AppConfig)BaseGet(key);
            }

        }
    }
}
