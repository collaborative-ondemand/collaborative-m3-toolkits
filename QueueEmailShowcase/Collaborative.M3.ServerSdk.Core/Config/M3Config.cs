﻿using Collaborative.M3.ServerSdk.Core.Config.KeySet;
using Collaborative.M3.ServerSdk.Core.Config.Section;
using Collaborative.M3.ServerSdk.Core.Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Config
{
    /// <summary>
    /// Provides access to various configuration-based functionality.
    /// </summary>
    public class M3Config
    {
        // contains the active configs set
        private static M3ConfigSet m3ActiveConfigSet = null;

        // initialized flag
        private static bool isInitialized = false;

        // lock semaphor
        private static Object initLock = new Object();


        /// <summary>
        /// Returns the active configuration set based on the configured config set chooser provider.
        /// </summary>
        public static M3ConfigSet ActiveConfig
        {
            get
            {
                return M3Config.GetActiveM3ConfigSet();
            }
        }

        /// <summary>
        /// Overrides the automatic config key detection and uses the config set indicated by the provided config key.
        /// This method must be called first before any other access to this class and should typically only be used to support testing.
        /// </summary>
        /// <param name="configKey"></param>
        public static void InitConfigSetWithKey(string configKey)
        {
            M3Config.InitConfig(configKey);
        }

        /// <summary>
        /// Returns the active config set based on the config set key appropriate for this running instance.
        /// </summary>
        /// <returns></returns>
        private static M3ConfigSet GetActiveM3ConfigSet()
        {
            if (M3Config.isInitialized == false)
            {
                M3Config.InitConfig();
            }

            return M3Config.m3ActiveConfigSet;
        }

        /// <summary>
        /// Performs initialization and controls for stampede effect.  Optional config key is passed in.  If a config key
        /// is passed in, then use the config key to intiailize.  This is often done for unit tests.
        /// </summary>
        private static void InitConfig(string configKey="")
        {
            // first confirm the init flag
            if (M3Config.isInitialized == false)
            {
                // now check again but block for stampede managemetn
                lock (M3Config.initLock) 
                {
                    if (M3Config.isInitialized == true) 
                    {
                        // someone beat us to this request so just return...otherwise let it go forward
                        return;
                    }

                }
            }

            // If we got here then we are responsible for initializing....so do it....
            M3Config.PerformInitialization(configKey);

        }

        
        /// <summary>
        /// Should only be called from InitConfig()
        /// </summary>
        private static void PerformInitialization(string configKey)
        {
            // declare the config section instance
            M3ConfigSection m3ConfigSection = null;

            try
            {
                // get the base config section
                m3ConfigSection = (M3ConfigSection)System.Configuration.ConfigurationManager.GetSection("m3Config");
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
                throw new ConfigurationErrorsException("Error loading the M3ConfigurationSection: m3Config.", ex);
            }
            

            // check for success
            if (m3ConfigSection == null)
            {
                // configuration error...log and throw
                string message = "M3Config:  Configuration Error - <m3Config> section missing from config file.";
                Console.WriteLine(message);
                throw new ConfigurationErrorsException(message);
            }

            if (configKey.IsNullOrEmpty())
            {
                // no config key provided so look it up
                // first make sure there is a provider configred
                if (m3ConfigSection.ConfigSetChooserProvider.IsNullOrEmpty())
                {
                    throw new ConfigurationErrorsException("Element m3Config must contain a 'configSetChooserProvider' attribute.");
                }

                // provider config exists so pass to the "determine" method
                configKey = M3Config.DetermineActiveConfigKey(m3ConfigSection.ConfigSetChooserProvider);
            }

            // find the correct config set based on the key
            // TODO:  clean this up by adding a string index property to the collection
            foreach (M3ConfigSet m3ConfigSet in m3ConfigSection.M3ConfigSets)
            {
                if (m3ConfigSet.M3ConfigKey.Equals(configKey, StringComparison.OrdinalIgnoreCase))
                {
                    // found a match to set the active
                    M3Config.m3ActiveConfigSet = m3ConfigSet;
                    Console.WriteLine("M3Config: Found m3ConfigSet for config key: " + configKey);

                    // set the flag
                    M3Config.isInitialized = true;

                    // kick out of this function.
                    return;
                }

            }

            // if we get to here then not found....write out the issue
            string setNotFoundMessage = "M3Config: ERROR ************  Unable to find a config set for config key: " + configKey;
            Console.WriteLine(setNotFoundMessage);
            throw new Exception(setNotFoundMessage);

        }

        /// <summary>
        /// Selects the active config key based on the algorithm provider.
        /// </summary>
        /// <returns></returns>
        private static string DetermineActiveConfigKey(string configSetKeyChooserProvider)
        {
            IM3ConfigSetKeyChooser keySetChooser = null;

            try
            {
                // use reflection to grab an instance of the indicated provider
                // first get the type
                Type providerType = Type.GetType(configSetKeyChooserProvider);
                
                // second create the object
                Object newObject = Activator.CreateInstance(providerType);

                // last, cast to the interface
                keySetChooser = (IM3ConfigSetKeyChooser)newObject;

            } catch (Exception ex)
            {
                string message = "Error instantiating the ConfigSetKeyChooserProvider specified in the m3Config: " + configSetKeyChooserProvider + ".  Error Message:  " + ex.Message;
                Console.WriteLine(message);
                throw new ConfigurationErrorsException(message, ex);

            }

            // get the active config key
            string activeConfigKey = null;

            try
            {
                activeConfigKey = keySetChooser.GetActiveM3ConfigKey();
            }
            catch (Exception ex)
            {
                string message = "Key Chooser Provider '" + configSetKeyChooserProvider + "' threw an excpetion while determining the active config key: " + ex.Message;
                Console.WriteLine(message);
                throw new ConfigurationErrorsException(message, ex);
            }

            return activeConfigKey;

        }
    }
}
