﻿using Collaborative.M3.ServerSdk.Core.Business.Domain;
using Collaborative.M3.ServerSdk.Core.Config;
using Collaborative.M3.ServerSdk.Core.Config.Section;
using Collaborative.M3.ServerSdk.Core.Logging;
using Collaborative.M3.ServerSdk.Identity.Common.Utils;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Compilation;

namespace Collaborative.M3.ServerSdk.Core.Cache.Domain
{
    public class M3Cache : IM3Domain
    {

        private static readonly M3Logger logger = M3Logger.getLogger(MethodBase.GetCurrentMethod().DeclaringType.Name);

        public string name { get; set; }
        public int timeToLive { get; set; }
        public string type { get; set; }  // [e.g. - INIT, EAGER, LAZY]
        public string loader { get; set; }

        private Dictionary<string, M3CacheEntry> cacheEntries = new Dictionary<string, M3CacheEntry>();
        private long lastUpdatedTicks = System.DateTime.Now.Ticks;
        private long expirationTicks = 0;

        /// <summary>
        /// Flag indicating if the manager is initialized. 
        /// </summary>
        private M3ThreadSafeBoolean isInitialized = new M3ThreadSafeBoolean();

        /// <summary>
        /// Flag indicating if a refresh is occuring.
        /// </summary>
        private M3ThreadSafeBoolean refreshInProgress = new M3ThreadSafeBoolean();

        /// <summary>
        /// Used to manage thread safety while a refresh is in progress
        /// </summary>
        private static ReaderWriterLockSlim refreshInProgressLock = new ReaderWriterLockSlim();

        /// <summary>
        /// Semaphore used to manage the initialization of the Authorization manager.
        /// </summary>
        private readonly object initLocker = new object();

        /**
         * Constructor
         *
         */
        public M3Cache(string name)
        {
            M3CacheConfigsCollection configs = M3Config.ActiveConfig.M3CacheConfigs;
            foreach (M3CacheConfig config in configs)
            {
                if (config.Name.Equals(name))
                {
                    this.name = config.Name;
                    this.timeToLive = Convert.ToInt32(config.TimeToLive);
                    this.type = config.Type;
                    this.loader = config.Loader;
                    break;
                }
            }
        }


        /// <summary>
        /// Initialization method gets called with every invocation but only executes fully once.  It performs the initialization of 
        /// this cache in a thread-safe manner and handles the stampede effect for the initial execution. Additional requestors will block until the
        /// first initialization completes.
        /// </summary>
        public void Init()
        {
            // perform a non-thread safe check first.  if it is true then someone else got there first
            // if it is true then attempt a thread safe check
            if (isInitialized.Value == false)
            {
                // only one thread can execute an initiation so this needs to be thread safe with a lock
                // this will cause all other threads to wait to get in and once the first thread completes, the initialization flag will be true
                // causing them to move on without trying to re-initialize
                lock (initLocker)
                {
                    // perform a thread-safe check attempting to set the init flag to true and conduct init if it was previously false
                    if (isInitialized.Value == false)
                    {
                        if (IsInitLoaded())
                        {
                            Load();
                        }
                        else
                        {
                            logger.debug("M3Cache.Init", "Cache is initialized, but not loaded.");
                        }

                        // once all config is complete, flip the init flag so no other inits occur
                        isInitialized.ToggleTrue();

                    }
                    else
                    {
                        logger.debug("M3Cache.Init", string.Format("Initialization in progress. key: {0}", this.name));
                    }
                } // end of lock block
            }
        }


        /// <summary>
        /// Loads the cache and parses into the appropriate structure replacing the existing information in this component in 
        /// a thread-safe manner.  This refresh handles the stampede effect so that only one execution occurs per refresh cycle.  Additional requestors will 
        /// access stale data until refresh completes.
        /// </summary>
        private void Load()
        {

            // try to set the in progress flag in a thread safe manner
            if (refreshInProgress.ToggleTrue() == false)
            {
                // the original refresh value was false and we successfully set it to true so 
                // this invocation is responsible to perform the refresh
                // Use reflection to invoke cache loader
                // Determine which class is responsible for loading.
                Type service = BuildManager.GetType(this.loader, true, false);
                IM3CacheLoader m3CacheLoader = (IM3CacheLoader)Activator.CreateInstance(service);

                // invoke the cache loader
                M3Cache refreshedCache = m3CacheLoader.Load();

                // save the new entry
                // grab a WriteLock
                refreshInProgressLock.EnterWriteLock();

                // replace the cache entry
                this.cacheEntries = refreshedCache.cacheEntries;

                // calculate expiration
                UpdateExpiration(refreshedCache.timeToLive);

                // update the last updated timestamp
                SetLastUpdatedTicks(DateTime.Now.Ticks);

                // exit the lock
                refreshInProgressLock.ExitWriteLock();

                // update the in progress flag
                refreshInProgress.ToggleFalse();
            }
        }


        /**
         * Retrieves the specified cache entry
         *
         * @param key
         * @return
         */
        public Object Get(string key)
        {
            if (cacheEntries.ContainsKey(key))
            {
                return cacheEntries[key].GetValue();
            }
            return null;
        }


        public void Put(string key, M3CacheEntry entry)
        {
            cacheEntries[key] = entry;
            SetLastUpdatedTicks(DateTime.Now.Ticks);
        }


        public bool IsInitLoaded()
        {
            bool isInit = false;

            // Check the cache type
            if (this.type == "INIT")
            {
                // the cache should be loaded when initialized.
                isInit = true;
            }

            // return the result
            return isInit;
        }

        public bool IsLoaded()
        {
            bool isLoaded = false;

            // Check if the cache entry is not null and contains a record.
            if (this.cacheEntries != null && this.cacheEntries.Count > 0)
            {
                // the cache is loaded
                isLoaded = true;
            }

            // return the result
            return isLoaded;
        }

        public long GetLastUpdatedTicks()
        {
            return lastUpdatedTicks;
        }

        public void SetLastUpdatedTicks(long lastUpdatedTicks)
        {
            this.lastUpdatedTicks = lastUpdatedTicks;
        }

        /**
         * Updates the expiration time of the current cache object
         *
         * @param secondsToLive how many seconds will the item live in the cache before expiring
         */
        public void UpdateExpiration(int secondsToLive)
        {

            // Calculate the Time To Live
            // Any object without a seconds to live will not be assigned an explicit expiration date
            if (secondsToLive != 0)
            {

                DateTime currentDate = DateTime.Now;
                DateTime expiration = currentDate.AddSeconds(secondsToLive);

                this.expirationTicks = expiration.Ticks;
            }
        }

        public bool IsExpired()
        {

            bool isExpired = false;

            // Check to see if there is an expiration time on this object
            if (this.expirationTicks > 0)
            {
                // Convert the Expiration time to a date
                DateTime expirationDate = new DateTime(this.expirationTicks);

                // Compare the expiration date to now
                if (expirationDate.CompareTo(DateTime.Now) < 0)
                {
                    // item has expired
                    isExpired = true;
                    logger.debug("M3Cache.IsExpired", string.Format("Cache has expired. name: {0}", this.name));
                }
                else
                {
                    //item is not expired
                    logger.debug("M3Cache.IsExpired", string.Format("Cache has not expired. name: {0}", this.name));
                }
            }

            //return the result
            return isExpired;
        }
    }
}
