﻿using Collaborative.M3.ServerSdk.Core.Business.Domain;
using Collaborative.M3.ServerSdk.Core.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Cache.Domain
{
    public class M3CacheEntry
    {
        // define the class logger
        private static readonly M3Logger logger = M3Logger.getLogger(MethodBase.GetCurrentMethod().DeclaringType.Name);

        private String key = null;
        private Object value = null;

        /**
         * Constructor
         * Initializes a cache object
         *
         * @param key
         * @param value
         */
        public M3CacheEntry(string key, Object value)
        {
            //Set the Key and the Value for the Cached Object
            this.key = key;
            this.value = value;
        }

        public String GetKey()
        {
            return key;
        }

        public Object GetValue()
        {
            return value;
        }

        public void SetValue(Object aValue)
        {
            this.value = aValue;
        }

    }
}
