﻿using Collaborative.M3.ServerSdk.Core.Cache.Domain;
using Collaborative.M3.ServerSdk.Core.Config;
using Collaborative.M3.ServerSdk.Core.Config.Section;
using Collaborative.M3.ServerSdk.Core.Logging;
using Collaborative.M3.ServerSdk.Core.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;
using System.Web.Compilation;

namespace Collaborative.M3.ServerSdk.Core.Cache
{
    public class M3CacheManager
    {
        private static readonly M3Logger logger = M3Logger.getLogger(MethodBase.GetCurrentMethod().DeclaringType.Name);

        /// <summary>
        /// Flag indicating if the manager is initialized. 
        /// </summary>
        private static M3ThreadSafeBoolean isInitialized = new M3ThreadSafeBoolean();

        /// <summary>
        /// Flag indicating if a refresh is occuring.
        /// </summary>
        private static M3ThreadSafeBoolean refreshInProgress = new M3ThreadSafeBoolean();

        /// <summary>
        /// Used to manage thread safety while a refresh is in progress
        /// </summary>
        private static ReaderWriterLockSlim refreshInProgressLock = new ReaderWriterLockSlim();

        /// <summary>
        /// Semaphore used to manage the initialization of the Authorization manager.
        /// </summary>
        private static readonly object initLocker = new object();

        /// <summary>
        /// Holds the time that the role map was last refreshed.
        /// </summary>
        private static DateTime lastRefresh = DateTime.Now;

        private static Dictionary<String, M3Cache> caches = new Dictionary<String, M3Cache>();

        /// <summary>
        /// Initialization method gets called with every invocation but only executes fully once.  It performs the initialization of 
        /// this manager in a thread-safe manner and handles the stampede effect for the initial execution. Additional requestors will block until the
        /// first initialization completes.
        /// </summary>
        private static void Init()
        {
            // perform a non-thread safe check first.  if it is true then someone else got there first
            // if it is true then attempt a thread safe check
            if (M3CacheManager.isInitialized.Value == false)
            {
                // only one thread can execute an initiation so this needs to be thread safe with a lock
                // this will cause all other threads to wait to get in and once the first thread completes, the initialization flag will be true
                // causing them to move on without trying to re-initialize
                lock (M3CacheManager.initLocker)
                {

                    // perform a thread-safe check attempting to set the init flag to true and conduct init if it was previously false
                    if (M3CacheManager.isInitialized.Value == false)
                    {
                        M3CacheConfigsCollection configs = M3Config.ActiveConfig.M3CacheConfigs;
                        foreach (M3CacheConfig config in configs)
                        {
                            M3Cache cache = new M3Cache(config.Name);
                            cache.Init();

                            M3CacheManager.caches.Add(config.Name, cache);
                        }

                        // once all config is complete, flip the init flag so no other inits occur
                        M3CacheManager.isInitialized.ToggleTrue();

                    }
                    else
                    {
                        logger.debug("M3CacheManager.init", "Initialization in progress.");
                    }
                } // end of lock block
            }
        }


        /// <summary>
        /// Loads the cache and parses into the appropriate structure replacing the existing information in this component in 
        /// a thread-safe manner.  This refresh handles the stampede effect so that only one execution occurs per refresh cycle.  Additional requestors will 
        /// access stale data until refresh completes.
        /// </summary>
        private static void Refresh(string cacheName)
        {
            M3Cache cache = caches[cacheName];
            if (cache != null && cache.loader != null)
            {
                // try to set the in progress flag in a thread safe manner
                if (refreshInProgress.ToggleTrue() == false)
                {
                    // the original refresh value was false and we successfully set it to true so 
                    // this invocation is responsible to perform the refresh
                    // Use reflection to invoke cache loader
                    // Determine which class is responsible for loading.
                    Type service = BuildManager.GetType(cache.loader, true, false);
                    IM3CacheLoader m3CacheLoader = (IM3CacheLoader)Activator.CreateInstance(service);

                    // invoke the cache loader
                    M3Cache refreshedCache = m3CacheLoader.Load();

                    // save the new entry
                    // grab a WriteLock
                    refreshInProgressLock.EnterWriteLock();

                    // calculate expiration
                    refreshedCache.UpdateExpiration(refreshedCache.timeToLive);

                    // update the last updated timestamp
                    refreshedCache.SetLastUpdatedTicks(DateTime.Now.Ticks);

                    // replace the cache entry
                    caches[cacheName] = refreshedCache;

                    // exit the lock
                    refreshInProgressLock.ExitWriteLock();

                    // update the in progress flag
                    refreshInProgress.ToggleFalse();
                }
            }
        }

        /// <summary>
        /// Checks to see if a refresh is required.  If a refresh is required then the method will trigger it.
        /// Should be invoked on each access of the cache.
        /// </summary>
        /// <returns></returns>
        private static void TriggerRefreshIfRequired(string cacheName)
        {
            M3Cache cache = caches[cacheName];
            // check it
            if (cache.IsExpired())
            {
                // time exceeded so check to see if there is a refresh in progress
                if (refreshInProgress.Value == false)
                {
                    // refresh required and no one is currently refreshing so do a refresh
                    Refresh(cacheName);
                }
            }
        }


        public static void TriggerRefresh(string cacheName)
        {
            if (cacheName != null)
            {
                if (caches.ContainsKey(cacheName))
                {
                    Refresh(cacheName);
                }
            }
        }


        /// <summary>
        /// Clears the cache in a thread-safe manner.
        /// </summary>
        private static void Clear(string cacheName)
        {
            M3Cache cache = caches[cacheName];
            if (cache != null)
            {
                // try to set the in progress flag in a thread safe manner
                if (refreshInProgress.ToggleTrue() == false)
                {
                    // the original refresh value was false and we successfully set it to true so 
                    // this invocation is responsible to perform the refresh

                    // invoke the cache loader
                    M3Cache clearedCache = new M3Cache(cacheName);

                    // save the new entry
                    // grab a WriteLock
                    refreshInProgressLock.EnterWriteLock();

                    // replace the cache entry
                    caches[cacheName] = clearedCache;

                    // exit the lock
                    refreshInProgressLock.ExitWriteLock();

                    // update the in progress flag
                    refreshInProgress.ToggleFalse();
                }
            }
        }


        public static void Flush(string cacheName)
        {
            if (cacheName != null)
            {
                if (caches.ContainsKey(cacheName))
                {
                    Clear(cacheName);
                }
            }
        }


        /**
         * Retrieves the specified cache
         *
         * @param key
         * @return
         */
        public static Object Get(string cacheName, String key)
        {
            Init();
            if (cacheName != null)
            {
                if (caches.ContainsKey(cacheName))
                {
                    M3Cache cache = caches[cacheName];
                    if (cache.IsLoaded())
                    {
                        TriggerRefreshIfRequired(cacheName);
                    }
                    else
                    {
                        Refresh(cacheName);
                    }
                    cache = caches[cacheName];
                    return cache.Get(key);
                }
            }
            return null;
        }
    }
}
