﻿using Collaborative.M3.ServerSdk.Core.Business.Common.Messaging;
using Collaborative.M3.ServerSdk.Core.Business.Integration.Database.Utilities;
using Collaborative.M3.ServerSdk.Core.Cache;
using Collaborative.M3.ServerSdk.Core.Integration;
using Collaborative.M3.ServerSdk.Core.Logging;
using Collaborative.M3.ServerSdk.Identity.Integration.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Cache.Integration
{
    public class M3CacheIntegrationService
    {
        private static readonly M3Logger logger = M3Logger.getLogger(MethodBase.GetCurrentMethod().DeclaringType.Name);

        public static T Get<T>(string cacheName, string cacheKey)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            logger.info("M3CacheIntegrationService.Get", string.Format("Getting {0} List from {1} cache", cacheKey, cacheName));

            // holds the success flag
            T cacheEntry = (T)M3CacheManager.Get(cacheName, cacheKey);

            sw.Stop();
            logger.info("M3CacheIntegrationService.Get", sw, string.Format("Got {0} List from {1} cache", cacheKey, cacheName));

            return cacheEntry;
        }

    }
}
