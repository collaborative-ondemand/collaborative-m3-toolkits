﻿using Collaborative.M3.ServerSdk.Core.Cache.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Cache
{
    public interface IM3CacheLoader
    {
        /// <summary>
        ///     Interface method for Cache Loaders.  This method is invoked to load the cache when the cache is of type INIT or EAGER.
        /// </summary>
        /// <returns></returns>
        M3Cache Load();
    }
}
