﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Cryptography.Interface
{
    public interface IM3EncryptDecryptProvider
    {
        string Decrypt(string cipherText, string encryptionPrivateKey = "");
        string Encrypt(string plainText, string encryptionPrivateKey = "");
    }
}
