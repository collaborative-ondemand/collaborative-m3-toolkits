﻿using Collaborative.M3.ServerSdk.Core.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Cryptography
{
    public class HashUtil
    {
        /// <summary>
        /// Performs a SHA256 hash on the cleartext string and returns a Base64 encoded value of the hash.
        /// </summary>
        /// <param name="cleartext"></param>
        /// <returns>Base64 encoded string representing the hash.</returns>
        public static string HashSHA256(string cleartext)
        {
            // initialize the algorithm
            HashAlgorithm sha256 = new SHA256CryptoServiceProvider();

            // convert cleartext to bytes
            byte[] clearBytes = cleartext.ToByteArray();

            // perform the hash
            byte[] hashBytes = sha256.ComputeHash(clearBytes);

            // encode
            string hashString = hashBytes.EncodeBase64();

            // return it
            return hashString;
        }
    }
}
