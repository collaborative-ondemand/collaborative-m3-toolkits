﻿using Collaborative.M3.ServerSdk.Core.Cryptography.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Cryptography.Implementation
{
    public class M3EncryptDecryptDefaultProvider : IM3EncryptDecryptProvider
    {
        private const string ENCRYPTION_KEY = "834acd5b12ee341c901ebc7a11bb987d";
        public string Decrypt(string cipherText, string encryptionPrivateKey = "")
        {
            try
            {
                if (String.IsNullOrEmpty(cipherText))
                    return cipherText;

                if (String.IsNullOrEmpty(encryptionPrivateKey))
                    encryptionPrivateKey = ENCRYPTION_KEY;

                var tDESalg = new TripleDESCryptoServiceProvider();
                tDESalg.Key = new ASCIIEncoding().GetBytes(encryptionPrivateKey.Substring(0, 24));
                tDESalg.IV = new ASCIIEncoding().GetBytes(encryptionPrivateKey.Substring(24, 8));

                byte[] buffer = Convert.FromBase64String(cipherText);
                return DecryptTextFromMemory(buffer, tDESalg.Key, tDESalg.IV);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string Encrypt(string plainText, string encryptionPrivateKey = "")
        {
            try
            {
                if (string.IsNullOrEmpty(plainText))
                    return plainText;

                if (String.IsNullOrEmpty(encryptionPrivateKey))
                    encryptionPrivateKey = ENCRYPTION_KEY;

                var tDESalg = new TripleDESCryptoServiceProvider();
                tDESalg.Key = new ASCIIEncoding().GetBytes(encryptionPrivateKey.Substring(0, 24));
                tDESalg.IV = new ASCIIEncoding().GetBytes(encryptionPrivateKey.Substring(24, 8));

                byte[] encryptedBinary = EncryptTextToMemory(plainText, tDESalg.Key, tDESalg.IV);
                return Convert.ToBase64String(encryptedBinary);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region Encryption Utilities

        private static byte[] EncryptTextToMemory(string data, byte[] key, byte[] iv)
        {
            using (var ms = new MemoryStream())
            {
                using (var cs = new CryptoStream(ms, new TripleDESCryptoServiceProvider().CreateEncryptor(key, iv), CryptoStreamMode.Write))
                {
                    byte[] toEncrypt = new UnicodeEncoding().GetBytes(data);
                    cs.Write(toEncrypt, 0, toEncrypt.Length);
                    cs.FlushFinalBlock();
                }

                return ms.ToArray();
            }
        }

        private static string DecryptTextFromMemory(byte[] data, byte[] key, byte[] iv)
        {
            using (var ms = new MemoryStream(data))
            {
                using (var cs = new CryptoStream(ms, new TripleDESCryptoServiceProvider().CreateDecryptor(key, iv), CryptoStreamMode.Read))
                {
                    var sr = new StreamReader(cs, new UnicodeEncoding());
                    return sr.ReadLine();
                }
            }
        }

        #endregion
    }
}
