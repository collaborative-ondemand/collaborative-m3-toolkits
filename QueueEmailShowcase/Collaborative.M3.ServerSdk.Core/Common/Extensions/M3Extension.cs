﻿using System;
using System.Web;

namespace Collaborative.M3.ServerSdk.Core.Common.Extensions
{
    public static class M3Extension
    {
        public static TType AsType<TType>(this string typeToConvert)
            where TType : class
        {
            return Convert.ChangeType(typeToConvert, typeof(TType)) as TType;
        }
        public static string ToStringEx(this object obj)
        {
            if (obj == null || obj == DBNull.Value)
                return string.Empty;
            else
                return obj.ToString();
        }

        /// <summary>
        /// This extension is added to String objects only, will verify if the string is null empty or white space, if true then will set the default value
        /// othewise will return the string representation
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToStringEx(this string obj, object defaultValue)
        {
            if (String.IsNullOrWhiteSpace(obj))
            {
                return defaultValue.ToStringEx();
            }
            else
            {
                return obj;
            }
        }

        public static int ToIntEx(this object obj)
        {
            int outNumber = 0;
            if (obj == null || obj == DBNull.Value)
                return 0;
            else
            {
                if (!int.TryParse(obj.ToString(), out outNumber))
                {
                    return 0;
                }
                else
                {
                    return outNumber;
                }
            }
        }

        /// <summary>
        /// This extension is added to floats only, and is meant to prevent displaying a float value as scientific notation.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToInt64StrEx(this float obj)
        {
            try
            {
                return Convert.ToInt64(obj).ToString();
            }
            catch
            {

                return string.Empty;
            }
        }

        /// <summary>
        /// Method used to validate if a DateTime struct is equal to DateTime.MinValue if ye
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string ToShortDateStringEx(this DateTime dt, object defaultValue)
        {
            if (dt == DateTime.MinValue)
            {
                return defaultValue.ToStringEx();
            }
            else
            {
                return dt.ToShortDateString();
            }
        }

        /// <summary>
        /// Converts the provided app-relative path into an absolute Url containing the 
        /// full host name
        /// </summary>
        /// <param name="relativeUrl">App-Relative path</param>
        /// <returns>Provided relativeUrl parameter as fully qualified Url</returns>
        /// <example>~/path/to/foo to http://www.web.com/path/to/foo</example>
        public static string ToAbsoluteUrl(this string relativeUrl)
        {
            if (string.IsNullOrEmpty(relativeUrl))
                return relativeUrl;

            if (HttpContext.Current == null)
                return relativeUrl;

            if (relativeUrl.StartsWith("/"))
                relativeUrl = relativeUrl.Insert(0, "~");
            if (!relativeUrl.StartsWith("~/"))
                relativeUrl = relativeUrl.Insert(0, "~/");

            var url = HttpContext.Current.Request.Url;
            var port = url.Port != 80 ? (":" + url.Port) : String.Empty;

            return String.Format("{0}://{1}{2}{3}",
                url.Scheme, url.Host, port, VirtualPathUtility.ToAbsolute(relativeUrl));
        }
    }
}