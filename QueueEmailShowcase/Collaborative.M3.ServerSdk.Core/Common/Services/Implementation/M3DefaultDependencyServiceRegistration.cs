﻿using Collaborative.M3.ServerSdk.Core.Business.Common.Container;
using Collaborative.M3.ServerSdk.Core.Common.Interface;
using Collaborative.M3.ServerSdk.Core.Common.IoC;
using Collaborative.M3.ServerSdk.Core.Identity.Integration.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Common.Implementation
{
    public class M3DefaultDependencyServiceRegistration : 
        IM3DependencyServiceResolver, IM3DependencyServiceRegistration
    {
        #region Constructor
        public M3DefaultDependencyServiceRegistration()
        {
            RegisterAuthenticationDataIntegrationService();
            RegisterAuthorizationDataIntegrationService();
        }
        #endregion

        #region IM3ServiceDependencyResolver
        public virtual object GetService(Type serviceType)
        {
            return M3ContainerManager.Container.GetInstance(serviceType);
        }

        public virtual TService GetService<TService>() 
            where TService : class
        {
            return M3ContainerManager.Container.GetInstance<TService>();
        }

        public virtual IEnumerable<object> GetServices(Type serviceType)
        {
            return M3ContainerManager.Container.GetAllInstances(serviceType);
        }

        public virtual IEnumerable<TService> GetServices<TService>() 
            where TService : class
        {
            return M3ContainerManager.Container.GetAllInstances<TService>();
        }
        #endregion

        #region IM3ServiceDependencyRegistration
        public virtual void RegisterAuthenticationDataIntegrationService()
        {
            M3ContainerManager.Container.Register<IM3UserDataIntegrationService, Identity.Integration.Implementation.EF.M3UserDataIntegrationService>();
        }

        public virtual void RegisterAuthorizationDataIntegrationService()
        {
            M3ContainerManager.Container.Register<IM3AuthorizationDataIntegrationService, Identity.Integration.Implementation.EF.M3AuthorizationDataIntegrationService>();
        }
        #endregion
    }
}
