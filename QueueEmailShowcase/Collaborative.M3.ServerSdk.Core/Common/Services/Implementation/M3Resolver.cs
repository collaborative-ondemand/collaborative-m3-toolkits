﻿using Collaborative.M3.ServerSdk.Core.Common.Interface;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Common.Implementation
{
    public class M3Resolver
    {
        private static M3Resolver instance = new M3Resolver();
        private const string NOT_IMPLEMENT_SERVICE_LOCATOR = "Service locator not implemented, service: {0}";

        private IM3DependencyServiceResolver _current;
        
        //
        // Summary:
        //     Initializes a new instance of the Collaborative.M3.ServerSdk.Core.Common.M3ServiceDependencyResolver class.
        public M3Resolver()
        {
            InnerSetResolver(new M3DefaultDependencyServiceRegistration());
        }

        //
        // Summary:
        //     Gets the implementation of the dependency resolver.
        //
        // Returns:
        //     The implementation of the dependency resolver.
        public static IM3DependencyServiceResolver Current
        {
            get { return instance.InnerCurrent; }
        }

        //
        // Summary:
        //     This API is not intended to be used
        //     directly from your code.
        //
        // Returns:
        //     The implementation of the dependency resolver.
        private IM3DependencyServiceResolver InnerCurrent
        {
            get { return _current; }
        }

        //
        // Summary:
        //     Provides a registration point for dependency resolvers, using the specified dependency
        //     resolver interface.
        //
        // Parameters:
        //   resolver:
        //     The dependency resolver.
        public static void SetResolver(IM3DependencyServiceResolver resolver)
        {
            instance.InnerSetResolver(resolver);
        }
        
        //
        // Summary:
        //     This API is not intended to be used directly from your code.
        //
        // Parameters:
        //   resolver:
        //     The object that implements the dependency resolver.
        private void InnerSetResolver(IM3DependencyServiceResolver resolver)
        {
            if (resolver == null)
            {
                throw new ArgumentNullException("resolver");
            }

            _current = resolver;
        }
    }
}
