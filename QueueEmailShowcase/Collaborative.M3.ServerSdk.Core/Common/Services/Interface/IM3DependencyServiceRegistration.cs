﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Common.Interface
{
    public interface IM3DependencyServiceRegistration
    {
        void RegisterAuthenticationDataIntegrationService();
        void RegisterAuthorizationDataIntegrationService();
    }
}
