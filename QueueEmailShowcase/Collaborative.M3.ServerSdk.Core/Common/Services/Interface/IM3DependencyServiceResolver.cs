﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Common.Interface
{
    public interface IM3DependencyServiceResolver
    {
        //
        // Summary:
        //     Resolves singly registered services that support arbitrary object creation.
        //
        // Parameters:
        //   serviceType:
        //     The type of the requested service or object.
        //
        // Returns:
        //     The requested service or object.
        object GetService(Type serviceType);

        TService GetService<TService>()
            where TService : class;
        //
        // Summary:
        //     Resolves multiply registered services.
        //
        // Parameters:
        //   serviceType:
        //     The type of the requested services.
        //
        // Returns:
        //     The requested services.
        IEnumerable<object> GetServices(Type serviceType);

        IEnumerable<TService> GetServices<TService>()
            where TService : class;
    }
}
