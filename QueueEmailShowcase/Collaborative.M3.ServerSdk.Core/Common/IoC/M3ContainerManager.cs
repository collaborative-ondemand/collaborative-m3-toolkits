﻿using Collaborative.M3.ServerSdk.Core.Business.Common.Container;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Common.IoC
{
    /// <summary>
    /// Class used to initialize (if it is not initialized) a Simple Injector Container so the underlying methods can be accessed (i.e. GetInstance method)
    /// </summary>
    public static class M3ContainerManager
    {
        [MethodImpl(MethodImplOptions.Synchronized)]
        private static void Initialize()
        {
            Container container = M3ContainerRegistration.RegisterEFScoping();
            M3Container.RegisterSingle(container);
        }

        public static Container Container
        {
            get
            {
                if (!M3Container.HasRegisteredType<Container>())
                {
                    Initialize();
                }
                return M3Container.GetInstance<Container>();
            }
        }
    }
}
