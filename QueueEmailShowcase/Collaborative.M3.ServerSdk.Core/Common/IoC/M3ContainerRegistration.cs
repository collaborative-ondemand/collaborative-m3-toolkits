﻿using Collaborative.M3.ServerSdk.Core.Integration.EF.Implementation;
using Collaborative.M3.ServerSdk.Core.Integration.EF.Interface;
using Collaborative.M3.ServerSdk.Identity.Integration.Util;
using SimpleInjector;
using SimpleInjector.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Common.IoC
{
    public class M3ContainerRegistration
    {
        public static Container RegisterEFScoping()
        {
            Container container = new Container();

            container.RegisterLifetimeScope<IDbContext>(() => 
            new M3EFObjectContext(M3IdentityDbConstants.DB_CONNECTION_NAME_M3IDENTITY), disposeWhenLifetimeScopeEnds: false);

            container.Register(typeof(IRepository<>), typeof(EFRepository<>));

            return container;
        }
    }
}
