﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Utils
{
    /// <summary>
    /// Set of extension methods providing clean syntax for common tasks.
    /// </summary>
    public static class M3ExtensionMethods
    {
        /// <summary>
        /// Extension method to check a string for null or empty.  
        /// </summary>
        /// <param name="theString"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(this string theString)
        {
            if (theString == null)
            {
                return true;
            }

            if (theString.Length == 0)
            {
                return true;
            }

            // if we got here all is well
            return false;
        }

        /// <summary>
        /// M3 Extension method that converts a string to a byte[].
        /// </summary>
        /// <param name="startString"></param>
        /// <returns></returns>
        public static byte[] ToByteArray(this string startString)
        {
            byte[] bytes = new byte[startString.Length * sizeof(char)];
            System.Buffer.BlockCopy(startString.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        /// <summary>
        /// M3 Extension method that converts a byte[] to a string.
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static string ToString(this byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

        /// <summary>
        /// M3 Extension method to encode a byte[] as a Base64 string.  Returns an empty string if there are any errors.
        /// </summary>
        /// <param name="startBytes"></param>
        /// <returns></returns>
        public static string EncodeBase64(this byte[] startBytes)
        {
            string base64String;

            try
            {
                base64String = System.Convert.ToBase64String(startBytes, 0, startBytes.Length);
            }
            catch (System.ArgumentNullException)
            {
                base64String = "";
            }

            return base64String;
        }
    }
}
