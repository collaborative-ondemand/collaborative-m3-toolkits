﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Net;
using System.Net.Http;
using System.Text;

namespace Collaborative.M3.ServerSdk.Core.Security.Filters
{
    /// <summary>
    /// Attribute class to enforce https
    /// Update WebApiConfig to set for all api requests
    /// ex config.Filters.Add(new RequireHttpsAttribute());
    /// </summary>
    public class RequireHttpsAttribute : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            var request = actionContext.Request;

            if (request.RequestUri.Scheme != Uri.UriSchemeHttps)
            {
                HttpResponseMessage response;

                //Update Response Header to point to a new location
                var uriBuilder = new UriBuilder(request.RequestUri);
                uriBuilder.Scheme = Uri.UriSchemeHttps;
                uriBuilder.Port = 443;

                //Set the Response Body Message
                string body = "<p>The requested resource can be found at <a href=\" {0}\">{0}</a>.</p>";

                //If the user is making a GET then set the location to force HTTPS
                if (request.Method.Method == "GET")
                {
                    response = request.CreateResponse(HttpStatusCode.Found, new StringContent(body, Encoding.UTF8, "text/html"));
                    response.Headers.Location = uriBuilder.Uri;
                }
                else
                {
                    response = request.CreateResponse(HttpStatusCode.NotFound, new StringContent(body, Encoding.UTF8, "text/html"));
                }

                //Set the Response
                actionContext.Response = response;
            }
        }
    }
}
