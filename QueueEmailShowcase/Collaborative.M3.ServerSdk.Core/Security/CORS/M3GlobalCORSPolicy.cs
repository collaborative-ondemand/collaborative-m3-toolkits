﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Cors;
using System.Web.Http.Cors;
using System.Net.Http;
using System.Threading;

namespace Collaborative.M3.ServerSdk.Core.Security.CORS
{
    public class M3GlobalCORSPolicy : Attribute, ICorsPolicyProvider
    {
        private CorsPolicy _policy;

        public M3GlobalCORSPolicy()
        {
            //Create a new CORS policy
            _policy = new CorsPolicy
            {
                AllowAnyMethod = true,
                AllowAnyHeader = true,
                AllowAnyOrigin = true
            };
        }

        public Task<CorsPolicy> GetCorsPolicyAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_policy);
        }
    }
}
