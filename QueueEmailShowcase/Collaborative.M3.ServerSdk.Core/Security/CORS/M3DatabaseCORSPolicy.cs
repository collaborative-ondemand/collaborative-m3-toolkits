﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Cors;
using System.Web.Http.Cors;
using System.Net.Http;
using System.Threading;

namespace Collaborative.M3.ServerSdk.Core.Security.CORS
{
    internal class M3DatabaseCORSPolicy: Attribute, ICorsPolicyProvider
    {
        private CorsPolicy _policy;

        public M3DatabaseCORSPolicy()
        {
            //Create a new CORS policy
            _policy = new CorsPolicy
            {
                AllowAnyMethod = true,
                AllowAnyHeader = true
            };

            //TODO Engage database service to retrieve a list of allowed origins

        }

        public Task<CorsPolicy> GetCorsPolicyAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            return Task.FromResult(_policy);
        }
    }
}
