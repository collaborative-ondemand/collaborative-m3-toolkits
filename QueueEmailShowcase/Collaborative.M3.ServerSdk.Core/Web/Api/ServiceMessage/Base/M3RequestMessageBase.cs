﻿using Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage.MessageElements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage.Base
{
    public abstract class M3RequestMessageBase<DomainBody> where DomainBody : IM3MessageBody
    {
        /**
         * The m3header property is implemented and managed by the framework and cannot be overridden.
         */
        public RequestHeader m3RequestHeader { get; set; }

        /**
         * The body property must be overriden by all subclasses.
         */
        public abstract DomainBody body { get; set; }
    }

}
