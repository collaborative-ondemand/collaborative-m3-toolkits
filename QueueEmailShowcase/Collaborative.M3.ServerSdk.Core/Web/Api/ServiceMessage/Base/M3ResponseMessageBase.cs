﻿using Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage.MessageElements;

namespace Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage.Base
{
    public abstract class M3ResponseMessageBase<DomainBody> where DomainBody: IM3MessageBody
    {
        /**
         * The m3header property is implemented and managed by the framework and cannot be overridden.
         */
        public MessageHeader m3header { get; set; }

        /**
         * The body property must be overriden by all subclasses.
         */
        public abstract DomainBody body { get; set; }
        
        /**
         * Default constructor
         */

        public M3ResponseMessageBase() 
        {
            m3header = new MessageHeader();
        }
    }
}