﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage.Base
{
    /**
     * This interface is a marker interface for use in all DTO classes that serve as the 'body' element of an M3ResponseMessage
     */
    public interface IM3MessageBody
    {

    }
}
