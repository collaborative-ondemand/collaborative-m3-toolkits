﻿using Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage.Base;

namespace Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage
{
    public class M3DoubleMessageResponse : M3ResponseMessageTypeBase<double>
    {
        /// <summary>
        /// M3 Message Body, returns a double
        /// </summary>
        public override double body { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="bodyContent"></param>
        public M3DoubleMessageResponse(double bodyContent)
        {
            body = bodyContent;
        }
    }
}
