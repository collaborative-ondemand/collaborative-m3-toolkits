﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage
{
    public class M3MessageConstants
    {
        // Status messages
        public const string STATUS_SUCCESS = "OK";
        public const string STATUS_FAIL_UNKNOWN = "FAIL";
        public const string STATUS_FAIL_VERSION = "FAIL_VERSION";
        public const string STATUS_FAIL_AUTHN = "FAIL_AUTHN";
        public const string STATUS_FAIL_AUTHZ = "FAIL_AUTHZ";
        public const string STATUS_FAIL_INVALID_PARAMETER = "FAIL_INVALID_PARAMETER";

        // Response messages
        public const string MSG_NOOP = "NOOP";
        public const string MSG_EMPTY = "";

        // Version constants
        public const string VER_CURR_VER = "00.00.01";
        public const string VER_MIN_VER = "00.00.01";

    }
}