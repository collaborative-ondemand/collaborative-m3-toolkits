﻿using Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage.Base;

namespace Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage
{
    public class M3LongMessageResponse : M3ResponseMessageTypeBase<long>
    {
        /// <summary>
        /// Body Element for a M3 Message, returns a long value
        /// </summary>
        public override long body { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="bodyContent"></param>
        M3LongMessageResponse(long bodyContent)
        {
            body = bodyContent;
        }
    }
}
