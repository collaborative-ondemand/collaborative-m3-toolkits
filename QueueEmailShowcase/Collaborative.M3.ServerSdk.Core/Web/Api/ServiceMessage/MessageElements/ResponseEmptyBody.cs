﻿using Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage.Base;
using System;

namespace Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage.MessageElements
{
    public class ResponseEmptyBody : IM3MessageBody { }
}
