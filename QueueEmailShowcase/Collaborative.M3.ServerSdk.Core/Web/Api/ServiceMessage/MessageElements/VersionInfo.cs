﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage.MessageElements
{
    public class VersionInfo
    {
        public string curVersion { get; set; }
        public string minVersion { get; set; }

        public VersionInfo()
        {
            curVersion = M3MessageConstants.VER_CURR_VER;
            minVersion = M3MessageConstants.VER_MIN_VER;

        }
    }
}