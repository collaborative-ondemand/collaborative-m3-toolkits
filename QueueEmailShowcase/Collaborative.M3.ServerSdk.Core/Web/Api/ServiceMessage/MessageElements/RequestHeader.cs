﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage.MessageElements
{
    public class RequestHeader
    {
        public string requestId { get; set; }
        // TODO:  remove after fully testing the device reg flow
        //public DeviceRegistration deviceReg { get; set;}
    }
}
