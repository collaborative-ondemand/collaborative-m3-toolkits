﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage.MessageElements
{
    public class DeviceRegistration
    {
        public String regToken { get; set; }
    }
}
