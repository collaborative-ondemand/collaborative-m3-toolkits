﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage.MessageElements
{
    public class MessageHeader
    {
        public string msgId { get; set; }
        public MessageStatus status { get; set; }
        public VersionInfo version { get; set; }

        public MessageHeader()
        {
            msgId = System.Guid.NewGuid().ToString();
            status = new MessageStatus();
            version = new VersionInfo();
            
        }
    }
}