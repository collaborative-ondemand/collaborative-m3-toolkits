﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage.MessageElements
{
    public class MessageStatus
    {
        public string statusCode { get; set; }
        public string statusInfo { get; set; }

        public MessageStatus()
        {
            this.statusCode = M3MessageConstants.STATUS_SUCCESS;
            this.statusInfo = M3MessageConstants.MSG_EMPTY;
        }
    }
}