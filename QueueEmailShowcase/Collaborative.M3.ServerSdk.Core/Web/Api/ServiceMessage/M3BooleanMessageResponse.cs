﻿using Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage.Base;

namespace Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage
{
    public class M3BooleanMessageResponse: M3ResponseMessageTypeBase<bool>
    {
        /// <summary>
        /// Body element for an M3 Message, returns a boolen value
        /// </summary>
        public override bool body { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="bodyContent"></param>
        M3BooleanMessageResponse(bool bodyContent)
        {
            body = bodyContent;
        }
    }
}
