﻿using Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage.Base;

namespace Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage
{
    public class M3IntegerMessageResponse : M3ResponseMessageTypeBase<int>
    {
        /// <summary>
        /// Body Element for an M3 Message, return an integer value
        /// </summary>
        public override int body { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="bodyContent"></param>
        public M3IntegerMessageResponse(int bodyContent)
        {
            body = bodyContent;
        }
    }
}
