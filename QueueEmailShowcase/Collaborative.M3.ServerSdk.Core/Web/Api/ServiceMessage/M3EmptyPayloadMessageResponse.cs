﻿using Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage.Base;
using Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage.MessageElements;


namespace Collaborative.M3.ServerSdk.Core.Web.Api.ServiceMessage
{
    public class M3EmptyPayloadMessageResponse : M3ResponseMessageBase<ResponseEmptyBody>
    {
        public override ResponseEmptyBody body { get; set;}
    }
}
