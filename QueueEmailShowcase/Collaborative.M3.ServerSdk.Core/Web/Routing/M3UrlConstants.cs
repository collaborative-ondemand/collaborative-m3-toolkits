﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.M3.ServerSdk.Core.Web.Routing
{
    public class M3UrlConstants
    {
        public const String M3_LIST = "/list";
        public const String M3_GET = "/get";
        public const String M3_CREATE = "/create";
        public const String M3_UPDATE = "/update";
        public const String M3_DELETE = "/delete";
    }
}
