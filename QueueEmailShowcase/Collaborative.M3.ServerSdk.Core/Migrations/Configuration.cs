namespace Collaborative.M3.ServerSdk.Core.Migrations
{
    using Collaborative.M3.ServerSdk.Core.Identity.Domain.EF;
    using Collaborative.M3.ServerSdk.Core.Integration.EF.Implementation;
    using Collaborative.M3.ServerSdk.Core.Integration.EF.Interface;
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Collaborative.M3.ServerSdk.Core.Integration.EF.Implementation.M3EFObjectContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Collaborative.M3.ServerSdk.Core.Integration.EF.Implementation.M3EFObjectContext context)
        {
            bool runSeedData = false;
            if (bool.TryParse(ConfigurationManager.AppSettings["RunSeedData"], out runSeedData) && runSeedData)
            {

                IRepository<IdnRoleDefinition> roleRepository = new EFRepository<IdnRoleDefinition>(context);

                List<IdnRoleDefinition> roles = new List<IdnRoleDefinition>();
                roles.Add(new IdnRoleDefinition()
                {
                    RoleKey = "ADMIN",
                    RoleDisplayName = "ADMIN",
                    RoleDescription = "Admin Role",
                    //IdnUserRoles,
                    //IdnRoleEntitlements
                    CreatedBy = "SEED_DATA",
                    CreatedDate = DateTime.Now,
                    ModifiedBy = "SEED_DATA",
                    ModifiedDate = DateTime.Now
                });

                roles.ForEach(role => roleRepository.Insert(role));
                roleRepository.Commit();
            }
        }
    }
}
