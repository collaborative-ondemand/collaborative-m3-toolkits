namespace Collaborative.M3.ServerSdk.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialLoad_20160306 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "M3.IDN_ENTITLEMENTS",
                c => new
                    {
                        ENTITLEMENT_ID = c.Int(nullable: false, identity: true),
                        KEY = c.String(nullable: false, maxLength: 48),
                        NAME = c.String(maxLength: 64),
                        DESCRIPTION = c.String(maxLength: 128),
                        MODIFIED_BY = c.String(nullable: false, maxLength: 38),
                        MODIFIED_DT = c.DateTime(nullable: false),
                        CREATED_BY = c.String(nullable: false, maxLength: 38),
                        CREATED_DT = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ENTITLEMENT_ID);
            
            CreateTable(
                "M3.IDN_ROLE_ENTITLEMENTS",
                c => new
                    {
                        ROLE_ENTITLEMENT_ID = c.Int(nullable: false, identity: true),
                        ROLE_ID = c.Int(nullable: false),
                        ENTITLEMENT_ID = c.Int(nullable: false),
                        MODIFIED_BY = c.String(nullable: false, maxLength: 38),
                        MODIFIED_DT = c.DateTime(nullable: false),
                        CREATED_BY = c.String(nullable: false, maxLength: 38),
                        CREATED_DT = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ROLE_ENTITLEMENT_ID)
                .ForeignKey("M3.IDN_ENTITLEMENTS", t => t.ENTITLEMENT_ID)
                .ForeignKey("M3.IDN_ROLES", t => t.ROLE_ID)
                .Index(t => t.ROLE_ID)
                .Index(t => t.ENTITLEMENT_ID);
            
            CreateTable(
                "M3.IDN_ROLES",
                c => new
                    {
                        ROLE_ID = c.Int(nullable: false, identity: true),
                        KEY = c.String(nullable: false, maxLength: 32),
                        NAME = c.String(maxLength: 48),
                        DESCRIPTION = c.String(maxLength: 128),
                        MODIFIED_BY = c.String(nullable: false, maxLength: 38),
                        MODIFIED_DT = c.DateTime(nullable: false),
                        CREATED_BY = c.String(nullable: false, maxLength: 38),
                        CREATED_DT = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ROLE_ID);
            
            CreateTable(
                "M3.IDN_USER_ROLES",
                c => new
                    {
                        USER_ROLE_ID = c.Int(nullable: false, identity: true),
                        USER_ID = c.Int(nullable: false),
                        ROLE_ID = c.Int(nullable: false),
                        MODIFIED_BY = c.String(nullable: false, maxLength: 38),
                        MODIFIED_DT = c.DateTime(nullable: false),
                        CREATED_BY = c.String(nullable: false, maxLength: 38),
                        CREATED_DT = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.USER_ROLE_ID)
                .ForeignKey("M3.IDN_ROLES", t => t.ROLE_ID)
                .ForeignKey("M3.IDN_USERS", t => t.USER_ID)
                .Index(t => t.USER_ID)
                .Index(t => t.ROLE_ID);
            
            CreateTable(
                "M3.IDN_USERS",
                c => new
                    {
                        USER_ID = c.Int(nullable: false, identity: true),
                        USER_NAME = c.String(nullable: false, maxLength: 20),
                        ACTIVE = c.Boolean(nullable: false),
                        MODIFIED_BY = c.String(nullable: false, maxLength: 38),
                        MODIFIED_DT = c.DateTime(nullable: false),
                        CREATED_BY = c.String(nullable: false, maxLength: 38),
                        CREATED_DT = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.USER_ID);
            
            CreateTable(
                "M3.IDN_WEB_CREDENTIALS",
                c => new
                    {
                        WEB_CREDENTIAL_ID = c.Int(nullable: false, identity: true),
                        USER_ID = c.Int(nullable: false),
                        PASSWORD_HASH = c.String(nullable: false, maxLength: 256),
                        MODIFIED_BY = c.String(nullable: false, maxLength: 38),
                        MODIFIED_DT = c.DateTime(nullable: false),
                        CREATED_BY = c.String(nullable: false, maxLength: 38),
                        CREATED_DT = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.WEB_CREDENTIAL_ID)
                .ForeignKey("M3.IDN_USERS", t => t.USER_ID)
                .Index(t => t.USER_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("M3.IDN_WEB_CREDENTIALS", "USER_ID", "M3.IDN_USERS");
            DropForeignKey("M3.IDN_ROLE_ENTITLEMENTS", "ROLE_ID", "M3.IDN_ROLES");
            DropForeignKey("M3.IDN_USER_ROLES", "USER_ID", "M3.IDN_USERS");
            DropForeignKey("M3.IDN_USER_ROLES", "ROLE_ID", "M3.IDN_ROLES");
            DropForeignKey("M3.IDN_ROLE_ENTITLEMENTS", "ENTITLEMENT_ID", "M3.IDN_ENTITLEMENTS");
            DropIndex("M3.IDN_WEB_CREDENTIALS", new[] { "USER_ID" });
            DropIndex("M3.IDN_USER_ROLES", new[] { "ROLE_ID" });
            DropIndex("M3.IDN_USER_ROLES", new[] { "USER_ID" });
            DropIndex("M3.IDN_ROLE_ENTITLEMENTS", new[] { "ENTITLEMENT_ID" });
            DropIndex("M3.IDN_ROLE_ENTITLEMENTS", new[] { "ROLE_ID" });
            DropTable("M3.IDN_WEB_CREDENTIALS");
            DropTable("M3.IDN_USERS");
            DropTable("M3.IDN_USER_ROLES");
            DropTable("M3.IDN_ROLES");
            DropTable("M3.IDN_ROLE_ENTITLEMENTS");
            DropTable("M3.IDN_ENTITLEMENTS");
        }
    }
}
