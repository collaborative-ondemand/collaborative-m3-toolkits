﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Collaborative.C2.Toolkit.MVC.Showcase.Startup))]
namespace Collaborative.C2.Toolkit.MVC.Showcase
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
