﻿using System.Web;
using System.Web.Mvc;

namespace Collaborative.C2.Toolkit.MVC.Showcase
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
