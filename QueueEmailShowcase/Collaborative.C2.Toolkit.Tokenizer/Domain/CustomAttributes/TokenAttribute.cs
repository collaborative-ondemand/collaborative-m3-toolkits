﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.C2.Toolkit.Tokenizer.Domain.CustomAttributes
{
    [System.AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = true)]
    public sealed class TokenAttribute : Attribute
    {
        public string TokenKey { get; set; }
    }
}
