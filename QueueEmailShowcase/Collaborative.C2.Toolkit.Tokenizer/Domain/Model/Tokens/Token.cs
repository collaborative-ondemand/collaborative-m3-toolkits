﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collaborative.C2.Toolkit.Tokenizer.Domain.Model.Tokens
{
    public class BaseToken
    {
        /// <summary>
        /// Token key
        /// </summary>
        public string Key { get; set; }
        /// <summary>
        /// Token value
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// Indicates whether this token should not be HTML encoded
        /// </summary>
        public bool NeverHtmlEncoded { get; set; }

        public override string ToString()
        {
            return string.Format("{0}: {1}", Key, Value);
        }
    }
}
