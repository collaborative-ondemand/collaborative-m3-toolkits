﻿
namespace Collaborative.C2.Toolkit.EventPublisher.Business.Interface
{
    public interface IConsumer<T>
    {
        void HandleEvent(T eventMessage);
    }
}
