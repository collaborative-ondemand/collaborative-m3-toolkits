﻿using System.Collections.Generic;

namespace Collaborative.C2.Toolkit.EventPublisher.Business.Interface
{
    /// <summary>
    /// Event subscrption service
    /// </summary>
    public interface ISubscriptionService
    {
        /// <summary>
        /// Get subscriptions
        /// </summary>
        /// <typeparam name="T">Type</typeparam>
        /// <returns>Event consumers</returns>
        IEnumerable<IConsumer<T>> GetSubscriptions<T>();
    }
}
